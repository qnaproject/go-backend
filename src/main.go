package main

import (
	"crypto/tls"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"golang.org/x/net/context"
	cron2 "gopkg.in/robfig/cron.v2"
	"io"
	"io/ioutil"
	"log"
	"models"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"
	"utils"
)

var ApiLogger *os.File

type myServer struct {
	http.Server
	reqCount     uint32
	shutDownChan chan bool
}

var s *myServer

func init() {
	var err error
	utils.GlobalLogger, err = os.OpenFile("log.log", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		fmt.Println("fatal\nERROR OPENING GLOBAL LOGGER FILE")
		os.Exit(1)
	}
	ApiLogger, err = os.OpenFile("api.log", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		fmt.Println("fatal\nERROR OPENING API LOGGER FILE")
		os.Exit(1)
	}
}

func loggerMiddleware(next http.Handler) http.Handler {
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Level: "error", Msg: "Panic Recovered in loggerMiddleware", Panic: a})
		}
	}()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Close = true
		w.Header().Set("Connection", "close")
		w.Header().Set("Content-Type", "text/html;")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Cache-Control, cache-override-nounce, Content-Type, session_id, access-control-allow-origin, access-control-allow-headers")
		w.Header().Set("Access-Control-Max-Age", "86400")
		w.Header().Set("Cache-Control", "s-maxage=0, max-age=0")

		s := fmt.Sprintf("%v\t%v\t%v\n%+v\n", time.Now().String(), r.Method, r.URL, r.RemoteAddr)
		fmt.Println(s)
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			io.WriteString(w, "OK")
			return
		}
		io.WriteString(ApiLogger, s)
		next.ServeHTTP(w, r)
	})
}

func main() {
	defer func() {
		if a := recover(); a != nil {
			fmt.Println(a)
			utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "Panic Recovered in Main", Panic: a, ErrorCode: 1})
			time.Sleep(2 * time.Second)
		} else {
			utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "Main Exiting", ErrorCode: 1})
			time.Sleep(2 * time.Second)
		}
	}()

	defer func() {
		models.DB.Close()
		utils.Logger(utils.MsgPrint{Msg: "DB Closed", Level: "error"})
	}()
	defer func() {
		utils.GlobalLogger.Close()
		utils.Logger(utils.MsgPrint{Msg: "GlobalLogger Closed", Level: "error"})
	}()
	defer func() {
		err := models.UDB.Close()
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: "UDB Close Error", Level: "error"})
		}
		utils.Logger(utils.MsgPrint{Msg: "UDB Closed", Level: "error"})
	}()
	err := models.UDB.RunValueLogGC(0.1)
	if err != nil {
		fmt.Println("error during UDB GC->", err)
	}

	c := cron2.New()
	c.AddFunc("TZ=Asia/Kolkata 0 0 1 * * *", models.CRON_deleteOldVerifyEmailEntries)
	c.AddFunc("TZ=Asia/Kolkata 0 0 */6 * * *", models.CRON_deleteUnverifiedUserAccounts)
	c.AddFunc("TZ=Asia/Kolkata 0 5 */6 * * *", models.CRON_runUDBGarbageCollection)
	c.AddFunc("TZ=Asia/Kolkata 0 */8 * * * *", models.CRON_setTrendingPosts)
	c.AddFunc("TZ=Asia/Kolkata 0 */6 * * * *", models.CRON_setLatestPosts)
	c.AddFunc("TZ=Asia/Kolkata 0 */7 * * * *", models.CRON_setLatestArticles)
	c.AddFunc("TZ=Asia/Kolkata 0 0 2 * * *", models.CRON_deleteOldMessages)
	c.AddFunc("TZ=Asia/Kolkata 0 0 6 * * *", models.CRON_deleteOldNotifications)
	c.AddFunc("TZ=Asia/Kolkata 0 0 3 * * *", models.CRON_deleteUnlinkedImages)
	c.AddFunc("TZ=Asia/Kolkata 0 */10 * * * *", models.CRON_setLeaders)
	c.AddFunc("TZ=Asia/Kolkata 0 0 4 * * *", models.CRON_deleteOldEmailSessionIdLinkage)
	c.AddFunc("TZ=Asia/Kolkata 0 0 5 * * *", models.CRON_deleteOldClosedReportEntries)

	//0 0 1 * * * is 1am
	c.Start()
	fmt.Printf("\nCRON Entries:\n%+v\n", c.Entries())

	//models.UDB.DropAll()
	models.CRON_setTrendingPosts()
	models.CRON_setLatestPosts()
	models.CRON_deleteOldMessages()
	models.CRON_setLatestArticles()
	models.CRON_deleteUnlinkedImages()
	models.CRON_setLeaders()
	models.CRON_deleteOldEmailSessionIdLinkage()
	models.CRON_deleteOldClosedReportEntries()
	models.CRON_deleteOldNotifications()
	/*
		Actual Working Logic Starts Below
	*/
	/*Badger stats*/
	lsm, vlog := models.UDB.Size()
	fmt.Println("BadgerDB size of lsm->", lsm)
	fmt.Println("BadgerDB size of vlog->", vlog)

	http.DefaultClient.Timeout = time.Minute * 1
	cer, err := tls.LoadX509KeyPair("/home/ubuntu/go/clevereply.com.pem", "/home/ubuntu/go/clevereply.com.key")
	tlsConfig := &tls.Config{Certificates: []tls.Certificate{cer}}
	if err != nil {
		log.Fatal("\n\n\nCOULD NOT LOAD ORIGIN CERTS\n\n\n", err)
	}
	s := &myServer{
		Server: http.Server{
			Addr:           ":443",
			ReadTimeout:    25 * time.Second,
			WriteTimeout:   25 * time.Second,
			IdleTimeout: 60 * time.Second,
			MaxHeaderBytes: 1 << 20,
			TLSConfig: tlsConfig,
		},
	}
	// s := &myServer{
	// 	Server: http.Server{
	// 		Addr:           ":8000",
	// 		ReadTimeout:    25 * time.Second,
	// 		WriteTimeout:   25 * time.Second,
	// 		IdleTimeout: 60 * time.Second,
	// 		MaxHeaderBytes: 1 << 20,
	// 	},
	// }
	r := mux.NewRouter().StrictSlash(true)
	s.Handler = r
	mainRouter := r.PathPrefix("/").Subrouter().StrictSlash(true)
	mainRouter.Use(loggerMiddleware)
	/**********/
	go func() {
		s.waitShutDown()
	}()
	mainRouter.HandleFunc("/shutdown141592653", s.ShutdownHandler)
	/**********/
	/*User*/
	mainRouter.HandleFunc("/createUser", callCreateUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/updateUser", callUpdateUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getUserDetails", callGetUserDetails).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/login", callLoginUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/logout", callLogoutUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/changePassword", callChangePassword).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/resendVerificationMail", callResendVerficationMail).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/verifyUser", callVerifyUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/suspendUser", callSuspendUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteUser", callDeleteUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/sendPasswordResetEmail", callSendPasswordResetEmail).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/verifyPasswordResetKey", callVerifyPasswordResetKey).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/resetPassword", callResetPassword).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getPublicUserDetails", callGetPublicUserDetails).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getOtherPublicUserDetails", callGetOtherPublicUserDetails).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/uploadAvatar", callUploadAvatar).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getUserDpUrlAndPoints", callGetUserDpUrlAndPoints).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/continueWithGoogle", callContinueWithGoogle).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/continueWithFacebook", callContinueWithFacebook).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/updateUserBackground", callUpdateUserBackground).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getLeaders", callGetLeaders).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/follow", callFollow).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/unFollow", callUnFollow).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getUserFeed", callGetUserFeed).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getFollowingCategories", callGetFollowingCategories).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getFollowingUsers", callGetFollowingUsers).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllUserFollowers", callGetAllUserFollowers).Methods(http.MethodPost, http.MethodOptions)

	/*Question*/
	mainRouter.HandleFunc("/addQuestion", callAddQuestion).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteQuestion", callDeleteQuestion).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/closeQuestion", callCloseQuestion).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/openQuestion", callOpenQuestion).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/changeQuestionCategory", callChangeQuestionCategory).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllUserQuestions", callGetAllUserQuestions).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getPost", callGetPost).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getTrendingPosts", callGetTrendingPosts).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getLatestPosts", callGetLatestPosts).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getOtherQuestions", callGetOtherQuestions).Methods(http.MethodPost, http.MethodOptions)

	/*Answer*/
	mainRouter.HandleFunc("/addAnswer", callAddAnswer).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllUserAnswers", callGetAllUserAnswers).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteAnswer", callDeleteAnswer).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/voteAnswer", callVoteAnswer).Methods(http.MethodPost, http.MethodOptions)

	/*Article*/
	mainRouter.HandleFunc("/addArticle", callAddArticle).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteArticle", callDeleteArticle).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getArticle", callGetArticle).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllUserArticles", callGetAllUserArticles).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getLatestArticles", callGetLatestArticles).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/upvoteArticle", callUpvoteArticle).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getOtherArticles", callGetOtherArticles).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getOtherUserArticles", callGetOtherUserArticles).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/addArticleComment", callAddArticleComment).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteArticleComment", callDeleteArticleComment).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getOtherArticlesForQuestion", callGetOtherArticlesForQuestion).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/uploadArticleImage", callUploadArticleImageToS3).Methods(http.MethodPost, http.MethodOptions)

	/*Comment*/
	mainRouter.HandleFunc("/addComment", callAddComment).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteComment", callDeleteComment).Methods(http.MethodPost, http.MethodOptions)

	/*Post*/

	mainRouter.HandleFunc("/getImageUrlFromTrackIds", callGetImageUrlFromTrackIds).Methods(http.MethodPost, http.MethodOptions)

	/*Report*/
	mainRouter.HandleFunc("/reportPost", callReportPost).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getReportsForModeration", callGetReportsForModeration).Methods(http.MethodPost, http.MethodOptions)

	/*Message*/
	mainRouter.HandleFunc("/addMessage", callAddMessage).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getConversation", callGetConversation).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllMessages", callGetAllMessages).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/blockMessagesFromUser", callBlockMessagesFromUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/unblockUser", callUnblockUser).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/markAllMessagesAsRead", callMarkAllMessagesAsRead).Methods(http.MethodPost, http.MethodOptions)

	/*Notifications*/
	mainRouter.HandleFunc("/getNotifications", callGetNotifications).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/setNotificationAsRead", callSetNotificationAsRead).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/markAllNotificationsAsRead", callMarkAllNotificationsAsRead).Methods(http.MethodPost, http.MethodOptions)

	/*Bio*/
	mainRouter.HandleFunc("/getUserBio", callGetUserBio).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/callAddOrUpdateUserBio", callAddOrUpdateUserBio).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllBackgroundUrls", callGetAllBackgroundUrls).Methods(http.MethodPost, http.MethodOptions)

	/*Bookmark*/
	mainRouter.HandleFunc("/addBookmark", callAddBookmark).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/deleteBookmark", callDeleteBookmark).Methods(http.MethodPost, http.MethodOptions)
	mainRouter.HandleFunc("/getAllUserBookmarks", callGetAllUserBookmarks).Methods(http.MethodPost, http.MethodOptions)

	/*General*/
	mainRouter.HandleFunc("/uploadImage", callUploadImageToS3).Methods(http.MethodPost, http.MethodOptions)

	//remove

	utils.Logger(utils.MsgPrint{Level: "info", Msg: "SERVER STARTING NOW"})
	err = s.ListenAndServeTLS("/home/ubuntu/go/clevereply.com.pem","/home/ubuntu/go/clevereply.com.key")
	// err = s.ListenAndServe()
	if err != nil {
		log.Printf("Listen and serve: %v", err)
	}
}

/*User*/
func callCreateUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callCreateUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.CreateUser(xUserData)
	fmt.Println(apiResponse)
	io.WriteString(w, apiResponse)
}

func callUpdateUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUpdateUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UpdateUser(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetUserDetails(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetUserDetails(sessionId)
	io.WriteString(w, apiResponse)
}

func callResendVerficationMail(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callResendVerficationMail"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.ResendVerificationEmail(xUserData)
	io.WriteString(w, apiResponse)
}

func callVerifyUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callVerifyUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.VerifyUser(xUserData)
	io.WriteString(w, apiResponse)
}

func callLoginUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callLoginUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.LoginUser(xUserData)
	io.WriteString(w, apiResponse)
}

func callLogoutUser(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.LogoutUser(sessionId)
	io.WriteString(w, apiResponse)
}

func callChangePassword(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callChangePassword"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.ChangePassword(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callSuspendUser(w http.ResponseWriter, r *http.Request) {
	xSuspensionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callSuspendUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.SuspendUser(xSuspensionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callDeleteUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteUser(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callSendPasswordResetEmail(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callSendPasswordResetEmail"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.SendPasswordResetMail(xUserData)
	io.WriteString(w, apiResponse)
}

func callVerifyPasswordResetKey(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callVerifyPasswordResetKey"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.VerifyPasswordResetKey(xUserData)
	io.WriteString(w, apiResponse)
}

func callResetPassword(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callResetPassword"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.ResetPasswordWithResetKey(xUserData)
	io.WriteString(w, apiResponse)
}

func callGetPublicUserDetails(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetPublicUserDetails"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetPublicUserDetails(xUserData, sessionId)
	w.Header().Set("Cache-Control", "s-maxage=600, max-age=2")
	io.WriteString(w, apiResponse)
}

func callGetOtherPublicUserDetails(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetOtherPublicUserDetails"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetOtherPublicUserDetails(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=600, max-age=200")
	io.WriteString(w, apiResponse)
}

func callUploadAvatar(w http.ResponseWriter, r *http.Request) {
	xImageData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUploadAvatar"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UploadAvatar(xImageData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetUserDpUrlAndPoints(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetUserDpUrlAndPoints"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetUserDpUrlAndPoints(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=600, max-age=400")
	io.WriteString(w, apiResponse)
}

func callContinueWithGoogle(w http.ResponseWriter, r *http.Request) {
	xGData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callContinueWithGoogle"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.ContinueWithGoogle(xGData)
	io.WriteString(w, apiResponse)
}

func callContinueWithFacebook(w http.ResponseWriter, r *http.Request) {
	xFData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callContinueWithFacebook"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.ContinueWithFacebook(xFData)
	io.WriteString(w, apiResponse)
}

func callUpdateUserBackground(w http.ResponseWriter, r *http.Request) {
	xBgData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUpdateUserBackground"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UpdateUserBackground(xBgData, sessionId)
	io.WriteString(w, apiResponse)
}

func callFollow(w http.ResponseWriter, r *http.Request) {
	xFollowData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callFollow"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.NewFollow(xFollowData, sessionId)
	io.WriteString(w, apiResponse)
}

func callUnFollow(w http.ResponseWriter, r *http.Request) {
	xFollowData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUnFollow"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.Unfollow(xFollowData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetUserFeed(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetUserFeed(sessionId)
	w.Header().Set("Cache-Control", "s-maxage=10, max-age=0")
	io.WriteString(w, apiResponse)
}

func callGetFollowingCategories(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetFollowingCategories(sessionId)
	io.WriteString(w, apiResponse)
}

func callGetFollowingUsers(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetFollowingUsers(sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllUserFollowers(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetAllUserFollowers(sessionId)
	io.WriteString(w, apiResponse)
}

func callGetLeaders(w http.ResponseWriter, r *http.Request) {
	apiResponse := models.GetLeaders()
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=400")
	io.WriteString(w, apiResponse)
}

/*Question*/
func callAddQuestion(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddQuestion"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddQuestion(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callDeleteQuestion(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteQuestion"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteQuestion(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callCloseQuestion(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callCloseQuestion"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.CloseQuestion(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callOpenQuestion(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callOpenQuestion"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.OpenQuestion(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callChangeQuestionCategory(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callChangeQuestionCategory"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.ChangeQuestionCategory(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllUserQuestions(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetAllUserQuestions"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetAllUserQuestions(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=2")
	io.WriteString(w, apiResponse)
}

func callGetPost(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetPost"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetPost(xQuestionData)
	w.Header().Set("Cache-Control", "s-maxage=600, max-age=10")
	io.WriteString(w, apiResponse)
}

func callGetTrendingPosts(w http.ResponseWriter, r *http.Request) {
	xCategoryData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetTrendingPosts"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetTrendingPosts(xCategoryData)
	w.Header().Set("Cache-Control", "s-maxage=300, max-age=100")
	io.WriteString(w, apiResponse)
}

func callGetLatestPosts(w http.ResponseWriter, r *http.Request) {
	xCategoryData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in calcallGetLatestPostslGetTrendingPosts"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetLatestPosts(xCategoryData)
	w.Header().Set("Cache-Control", "s-maxage=300, max-age=100")
	io.WriteString(w, apiResponse)
}

func callGetOtherQuestions(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in calcallGetLatestPostslGetTrendingPosts"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetOtherQuestions(xQuestionData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=400")
	io.WriteString(w, apiResponse)
}

/*Answer*/
func callAddAnswer(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddAnswer"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddAnswer(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllUserAnswers(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddAnswer"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetAllUserAnswers(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=2")
	io.WriteString(w, apiResponse)
}

func callDeleteAnswer(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteAnswer"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteAnswer(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

func callVoteAnswer(w http.ResponseWriter, r *http.Request) {
	xQuestionData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callVoteAnswer"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.VoteAnswer(xQuestionData, sessionId)
	io.WriteString(w, apiResponse)
}

/*Article*/
func callAddArticle(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddArticle"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddArticle(xArticleData, sessionId)
	io.WriteString(w, apiResponse)
}

func callDeleteArticle(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteArticle"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteArticle(xArticleData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetArticle(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetArticle"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetArticle(xArticleData)
	w.Header().Set("Cache-Control", "s-maxage=600, max-age=200")
	io.WriteString(w, apiResponse)
}

func callGetAllUserArticles(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetAllUserArticles"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetAllUserArticles(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=2")
	io.WriteString(w, apiResponse)
}

func callGetLatestArticles(w http.ResponseWriter, r *http.Request) {
	xCategoryData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetLatestArticles"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetLatestArticles(xCategoryData)
	w.Header().Set("Cache-Control", "s-maxage=300, max-age=100")
	io.WriteString(w, apiResponse)
}

func callUpvoteArticle(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUpvoteArticle"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UpvoteArticle(xArticleData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetOtherArticles(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetOtherArticles"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetOtherArticles(xArticleData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=40")
	io.WriteString(w, apiResponse)
}

func callGetOtherUserArticles(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetOtherUserArticles"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetOtherUserArticles(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=400")
	io.WriteString(w, apiResponse)
}

func callAddArticleComment(w http.ResponseWriter, r *http.Request) {
	xCommentData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddArticleComment"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddArticleComment(xCommentData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetOtherArticlesForQuestion(w http.ResponseWriter, r *http.Request) {
	xArticleData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetOtherArticlesForQuestion"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetOtherArticlesForQuestion(xArticleData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=400")
	io.WriteString(w, apiResponse)
}

func callDeleteArticleComment(w http.ResponseWriter, r *http.Request) {
	xCommentData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteArticleComment"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteArticleComment(xCommentData, sessionId)
	io.WriteString(w, apiResponse)
}

func callUploadArticleImageToS3(w http.ResponseWriter, r *http.Request) {
	xImageData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in uploadImageToS3"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UploadArticleImage(xImageData, sessionId)
	io.WriteString(w, apiResponse)
}

/*Comment*/
func callAddComment(w http.ResponseWriter, r *http.Request) {
	xCommentData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddComment"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddComment(xCommentData, sessionId)
	io.WriteString(w, apiResponse)
}

func callDeleteComment(w http.ResponseWriter, r *http.Request) {
	xCommentData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteComment"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteComment(xCommentData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetImageUrlFromTrackIds(w http.ResponseWriter, r *http.Request) {
	xTrackData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetImageUrlFromTrackIds"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetImageUrlFromTrackIds(xTrackData)
	io.WriteString(w, apiResponse)
}

/*Report*/
func callReportPost(w http.ResponseWriter, r *http.Request) {
	xReportData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callReportPost"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.ReportPost(xReportData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetReportsForModeration(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetReportsForModeration(sessionId)
	io.WriteString(w, apiResponse)
}

/*Message*/
func callAddMessage(w http.ResponseWriter, r *http.Request) {
	xMessageData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddMessage"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddMessage(xMessageData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetConversation(w http.ResponseWriter, r *http.Request) {
	xMessageData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetMessage"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetConversation(xMessageData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllMessages(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetAllMessages(sessionId)
	io.WriteString(w, apiResponse)
}

func callBlockMessagesFromUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callBlockMessagesFromUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.BlockMessagesFromUser(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callUnblockUser(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callUnblockUser"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UnblockUser(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callMarkAllMessagesAsRead(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.MarkAllMessagesAsRead(sessionId)
	io.WriteString(w, apiResponse)
}

/*Notification*/
func callGetNotifications(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetNotifications(sessionId)
	io.WriteString(w, apiResponse)
}

func callSetNotificationAsRead(w http.ResponseWriter, r *http.Request) {
	xNotifData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in SetNotificationAsRead"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.SetNotificationAsRead(xNotifData, sessionId)
	io.WriteString(w, apiResponse)
}

func callMarkAllNotificationsAsRead(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.MarkAllNotificationsAsRead(sessionId)
	io.WriteString(w, apiResponse)
}

/*Bookmark*/

func callAddBookmark(w http.ResponseWriter, r *http.Request) {
	xBookmarkData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddBookmark"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddBookmark(xBookmarkData, sessionId)
	io.WriteString(w, apiResponse)
}

func callDeleteBookmark(w http.ResponseWriter, r *http.Request) {
	xBookmarkData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callDeleteBookmark"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.DeleteBookmark(xBookmarkData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllUserBookmarks(w http.ResponseWriter, r *http.Request) {
	sessionId := r.Header.Get("session_id")
	apiResponse := models.GetAllUserBookmarks(sessionId)
	w.Header().Set("Cache-Control", "s-maxage=10, max-age=5")
	io.WriteString(w, apiResponse)
}

/*General*/
func callUploadImageToS3(w http.ResponseWriter, r *http.Request) {
	xImageData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in uploadImageToS3"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.UploadImage(xImageData, sessionId)
	io.WriteString(w, apiResponse)
}

/*Bio*/
func callGetUserBio(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callGetUserBio"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	apiResponse := models.GetUserBio(xUserData)
	w.Header().Set("Cache-Control", "s-maxage=1200, max-age=1")
	io.WriteString(w, apiResponse)
}

func callAddOrUpdateUserBio(w http.ResponseWriter, r *http.Request) {
	xUserData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error(), Msg: "Error reading request body in callAddOrUpdateUserBio"})
		io.WriteString(w, `{"success": false, "msg":"Something Went Wrong."}`)
		return
	}
	sessionId := r.Header.Get("session_id")
	apiResponse := models.AddOrUpdateUserBio(xUserData, sessionId)
	io.WriteString(w, apiResponse)
}

func callGetAllBackgroundUrls(w http.ResponseWriter, r *http.Request) {
	apiResponse := models.GetAllBackgroundUrls()
	w.Header().Set("Cache-Control", "s-maxage=900, max-age=300")
	io.WriteString(w, apiResponse)
}

/******/
func (s *myServer) ShutdownHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, "Shutting Down the Server")

	//Do nothing if shutdown request already issued
	//if s.reqCount == 0 then set to 1, return true otherwise false
	if !atomic.CompareAndSwapUint32(&s.reqCount, 0, 1) {
		log.Printf("Shutdown through API call in progress...")
		return
	}

	//shutdown the server
	utils.Logger(utils.MsgPrint{Level: "error", Msg: "(MANUAL) SHUTTING DOWN SERVER"})
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := s.Shutdown(ctx)
	if err != nil {
		log.Printf("Shutdown request error: %v", err)
	}

}

func (s *myServer) waitShutDown() {
	sigShut := make(chan os.Signal, 1)
	signal.Notify(sigShut, syscall.SIGTERM, syscall.SIGINT)
	servExit := func() {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		err := s.Shutdown(ctx)
		if err != nil {
			log.Printf("Shutdown request error: %v", err)
		}
	}
	select {
	case <-sigShut:
		utils.Logger(utils.MsgPrint{Level: "error", Msg: "(SIGNAL/INTERRUPT) SHUTTING DOWN SERVER"})
		servExit()
	}
}

/******/

package utils

import (
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"github.com/aaaton/golem"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/dchest/uniuri"
	"github.com/getsentry/raven-go"
	"github.com/jmoiron/sqlx"
	"github.com/logrusorgru/aurora"
	"gopkg.in/redis.v5"
	"io"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)
var GlobalLogger *os.File
var S3Session *session.Session
var DB *sqlx.DB
var RDB *redis.Client
var mailConf = struct{
	Host string
	Port int
	Username string
	Password string
	FromAddr string
}{
	Host:"email-smtp.us-east-1.amazonaws.com",
	Port:2587,
	Username:"AKIAS27QNSWSS7B5JIM3",
	Password:os.Getenv("SES_SMTP_PWD"),
	FromAddr:"contact@clevereply.com",
}

const (
	ErrGeneric = "Something went wrong. Please try again later."
	ErrSuspension = "Your account is under suspension."
	ErrTerminated = "Your account has been terminated."
	ErrAccountNonExistant = "This account doesn't exist."
	ErrNotLoggedIn = "You need to login to continue."
	ErrIncorrectPassword = "The password you entered is incorrect."
	ErrQuestionTooShort = "Your question is too short. Please provide a longer question."
	ErrQuestionTooLong = "Your question is too long. Please provide a shorter question."
	ErrInvalidCharsInQuestion = "Your question has invalid characters."
	ErrDescriptionTooShort = "Your description is too short. Please provide a longer description (min 20 chars)."
	ErrDescriptionTooLong = "Your description is too long. Please provide a shorter description (max 1500 chars)."
	ErrInvalidCategory = "Invalid category."
	ErrNoPermissioToDeleteQuestion = "You do not have permission to delete this question."
	ErrNoPermissioToCloseQuestion = "You do not have permission to close this question."
	ErrQuestionDoesntExist = "This question doesn't exist."
	ErrQuestionAlreadyClosedOrDeleted = "This question has already been closed or doesn't exist."
	ErrQuestionAlreadyOpenedOrDeleted = "This question is already open or doesn't exist."
	ErrNoPermissioToOpenQuestion = "You do not have permission to open this question."
	ErrInvalidReportCategory = "Invalid Report Category."
	ErrQuestionAlreadyOfCategory = "The question is already of the selected category."
	ErrNoPermissionToChangeCategory = "You do not have permission to change the category of this question."
	ErrEmptyAnswer = "The answer cannot be empty."
	ErrCannotAnswerOwnQuestion = "You cannot answer your own question."
	ErrThisQuestionIsClosed = "This question is closed."
	ErrInvalidCharactersInQualification = "Your qualification has invalid characters."
	ErrAnswerTooShort = "Your answer is too short. Please provide a longer answer."
	ErrAnswerTooLong = "Your answer is too long. Please provide a shorted answer."
	ErrYouHaveNotAskedAnyQuestions = "No questions asked yet."
	ErrCannotDeleteQuestionAfterGrace = "You cannot delete your question after 2 days of posting it."
	ErrNotAuthorized = "You are not authorized to perform this operation."
	ErrAnswerDoesntExist = "This answer doesn't exist."
	ErrCannotDeleteAnswerAfterGrace = "You cannot delete your answer after 2 days of posting it."
	ErrCannotPostEmptyComment = "You cannot post an empty comment."
	ErrCommentTooShort = "Your comment is too short. Please provide a longer comment."
	ErrCommentTooLong = "Your comment is too long. Please provide a shorter comment."
	ErrCommentDoesntExist = "This comment does not exist."
	ErrCannotDeleteCommentAfterGrace = "You cannot delete your comment after 2 days of posting it."
	ErrAlreadyDownVoted = "You have already down-voted."
	ErrAlreadyUpVoted = "You have already up-voted."
	ErrCannotVoteOwnAnswer = "You cannot vote your own answer."
	ErrInvalidPasswordResetLink = "This link is invalid."
	ErrPasswordResetLinkExpired = "This link has expired."
	ErrPasswordRequired = "Password is required."
	ErrPasswordsDoNotMatch = "The passwords do not match."
	ErrPasswordNotMatchingRequirements = "Invalid Password. Password must have Minimum 8 and Maximum 20 characters."
	ErrGettingPost = "Something went wrong while fetching the post."
	ErrCannotSendMessageToRecipient = "You cannot send messages to this user."
	ErrCannotSendEmptyMessage = "You cannot send an empty message."
	ErrRecipientForMessageDoesntExist = "The user account you are trying to send the message to does't exist."
	ErrMessageTooLong = "This message is too long."
	ErrCannotMessageYourself = "You cannot send a message to yourself."
	ErrUserAlreadyBlocked = "This user is already blocked."
	ErrUserNotBlocked = "This user was previously not blocked."

	SuccessLoggedIn = "You have successfully logged In."
	SuccessLoggedOut = "You have successfully logged Out."
	SuccessChangedPassword = "Your password has been successfully changed."
	SuccessPostedQuestion = "Your question has been successfully posted."
	SuccessDeletedQuestion = "The question has been successfully deleted."
	SuccessClosedQuestion = "The question has been successfully closed."
	SuccessOpenedQuestion = "The question has been successfully opened."
	SuccessReported = "The content has been successfully reported and will be reviewed. Thanks for reporting!"
	SuccessCategoryChanged = "The category has been successfully changed."
	SuccessPostedAnswer = "Your answer has been successfully posted."
	SuccessDeletedAnswer = "The answer has been successfully deleted."
	SuccessPostedComment = "Your comment has been successfully posted."
	SuccessDeletedComment = "The comment has been successfully deleted."
	SuccessSuspensionLifted = "The suspension on your account has been lifted. Please login again to continue."
	SuccessPasswordReset = "Your password has been successfully reset."
	SuccessMessageSent = "Your message has been successfully sent."
	SuccessBlockedMessages = "Successfully blocked messages from this user."
	SuccessUnblockedUser = "This user has been successfully unblocked."
)

func init() {
	raven.SetDSN("https://275422a8f202483b9972990d2b354bdd:3448e6a321604bec8993bd465d08c594@sentry.io/1357063")
	// if os.Getenv("SES_SMTP_PWD") == "" {
	// 	Logger(MsgPrint{Level:"fatal", Error:errors.New("SES_SMTP password not set").Error(), Msg:"SES_SMTP password not set"})
	// }
	var err error
	S3Session, err = session.NewSession(&aws.Config{Region: aws.String(os.Getenv("S3_BUCKET_REGION"))})
	if err != nil {
		Logger(MsgPrint{Level:"fatal", Error:err.Error(), Msg:"COULD NOT CREATE S3 SESSION"})
	}
	if err != nil {
		Logger(MsgPrint{Level: "fatal", Msg: "COULD NOT CONNECT TO DATABASE", Error: err.Error()})
	}
	RDB = redis.NewClient(&redis.Options{
		Addr:"localhost:6379",
		Password:os.Getenv("REDIS_PWD"),
		DB:0,
	})
}

type ResponseBodyJSON struct {
	Success bool `json:"success"`
	Msg string `json:"msg"`
	Data interface{} `json:"data"`
}

type MsgPrint struct {
	Level     string
	Msg       string
	Error     string
	Panic     interface{}
	ErrorCode int
	Data interface{}
	Api string
}

func Logger(m MsgPrint) {
	io.WriteString(GlobalLogger, fmt.Sprintf("%+v\n", m))
	var coloredMsg interface{}
	switch m.Level {
	case "info":
		coloredMsg = aurora.Gray(m).BgGreen().Bold()
	case "warn":
		coloredMsg = aurora.Gray(m).BgCyan().Bold()
	case "error":
		coloredMsg = aurora.Gray(m).BgRed().Bold()
	case "fatal":
		coloredMsg = aurora.Gray(m).BgRed().Bold()
	default:
		coloredMsg = m
	}
	log.Printf("\n%+v\n", coloredMsg)
	if m.Level == "fatal" || m.Level == "error" {
		if m.Level == "fatal" {
			raven.CaptureErrorAndWait(errors.New(m.Error), nil)
		}
		raven.CaptureError(errors.New(m.Error), nil)
	}
	if m.Level == "fatal" {
		os.Exit(m.ErrorCode)
	}
}

func AddFileToS3(content []byte, dest int, someString string) (string, error) {
	defer func(){
		if a := recover(); a != nil {
			Logger(MsgPrint{Panic:a, Level:"error", Api:"AddFileToS3"})
		}
	}()
	bucketName := ""
	fileName := ""
	switch dest {
	case 1: {
		bucketName = os.Getenv("S3_BUCKET_DP")
		if bucketName == "" || len(bucketName) <1 {
			err := errors.New("ENV NOT SET : S3_BUCKET_DP")
			raven.CaptureError(err, nil)
			Logger(MsgPrint{Level:"error", Msg:"ENV NOT SET : S3_BUCKET_DP"})
			return "", err
		}
		fileName = strconv.FormatInt(time.Now().UTC().UnixNano(), 10)+"-"+uniuri.NewLen(uniuri.StdLen)+".jpg"
		}
	case 2: {
		bucketName = os.Getenv("S3_BUCKET_AVATAR")
		if bucketName == "" || len(bucketName) <1 {
			err := errors.New("ENV NOT SET : S3_BUCKET_AVATAR")
			raven.CaptureError(err, nil)
			Logger(MsgPrint{Level:"error", Msg:"ENV NOT SET : S3_BUCKET_AVATAR"})
			return "", err
		}
		fileName =  someString+".jpg"
	}
	default: {
		return "", errors.New("INVALID BUCKET DEST")
		}
	}
	// Open the file for use
	//file, err := os.Open(fileDir)
	//if err != nil {
	//	return "", err
	//}
	//defer file.Close()

	// Get file size and read the file content into a buffer
	//fileInfo, _ := file.Stat()
	//var size int64 = fileInfo.Size()
	//buffer := make([]byte, size)
	//file.Read(buffer)

	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.

	_, err := s3.New(S3Session).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(bucketName),
		Key:                  aws.String(fileName),
		Body:                 bytes.NewReader(content),
		ContentLength:        aws.Int64(int64(len(content))),
		ContentType:          aws.String(http.DetectContentType(content)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
		CacheControl: aws.String("no-cache"),
	})
	return ("https://s3.ap-south-1.amazonaws.com/"+bucketName+"/"+fileName), err
}

func SendAMail(from string, to string, subject string, message string) (error) {
	auth := smtp.PlainAuth("", mailConf.Username , mailConf.Password, mailConf.Host)
	msg := []byte("From: Clevereply <"+from+">\r\nTo:"+to+"\r\n" + "Subject: "+subject+"\r\nMIME-Version: 1.0;\r\nContent-Type: text/html; charset=\"utf-8\"\r\n\n"+ message+"\r\n")
	err := smtp.SendMail(mailConf.Host+":"+strconv.Itoa(mailConf.Port), auth, from, []string{to}, []byte(msg))
	if err != nil {
		return err
	} else {
		return nil
	}
}

const (
	PointAnswerMinimum = 5
	PointAnswerUpvote = 1
	PointAnswerDownvote = -1
	PointFirstThreeAnswers = 10
	PointPostThreeAnswers = 5
	PointQuestionTrending  = 100
	PointModDeleteQuestion = -100
	PointModDeleteAnswer = -100
	PointModDeleteComment = -50
	PointAnswerHundredUpvotes = 100
	PointUserFirstSuspended = -500
	PointUserSecondSuspension = -1000
	PointUserOfTheYear = 10000
)

var pointMultiplier = map[int]int{
	1:1,
	100:2,
	1000:3,
	10000:4,
	100000:5,
	1000000:6,
}

func GetPointsMultiplier(points int) (int) {
	switch {
	case points < 0:
		return 0
	case points < 100:
		return pointMultiplier[1]
	case points < 1000:
		return pointMultiplier[100]
	case points < 10000:
		return pointMultiplier[1000]
	case points < 100000:
		return pointMultiplier[10000]
	case points < 1000000:
		return pointMultiplier[100000]
	case points < 10000000:
		return pointMultiplier[1000000]
	default:
		return 0
	}
}

func GzipCompress(content []byte) (string) {
	var g bytes.Buffer
	gzw, _ := gzip.NewWriterLevel(&g, 7)
	defer gzw.Close()
	gzw.Write(content)
	gzw.Flush()
	return g.String()
}

func GzipDecompress(content string) (string, error) {
	g := bytes.NewReader([]byte(content))
	var res bytes.Buffer
	gzr, err := gzip.NewReader(g)
	if err != nil {
		Logger(MsgPrint{Error:err.Error(), Level:"error", Api:"gzipDecompress"})
		return "", err
	} else {
		defer gzr.Close()
		io.Copy(&res, gzr)
		return res.String(), nil
	}
}

func GetKeywordsFromTitle(title string) (string) {
	lemmaStopWords := []string{"all","he'd","both","she'll","because","its","they've","during","herself","than","theirs","through","can","in","other","be","at","her","himself","my","while","who's","if","ourselves","they'd","under","where's","against","nor","would","each","from","itself","our","out","over","we'd","they","again","few","far","i've","into","such","this","what's","why","down","once","when's","have","his","myself","own","when","why's","about","and","i","on","she","she'd","we'll","below","me","should","that","up","your","hers","then","what","any","i'm","he's","how","he","him","you'll","yours","how's","they'll","we","until","above","before","for","it","only","same","to","she's","there","they're","you've","by","he'll","here","let's","some","where","ought","the","their","but","or","we've","who","you'd","many","that's","we're","a","i'd","themselves","which","whom","so","them","there's","too","with","i'll","it's","of","you","after","between","very","yourself","do","here's","ours","you're"}
	lemmaMap := map[string]struct{}{}
	lemm, err := golem.New("english")
	resultMap := map[string]struct{}{}
	var resultStr string
	xQString := strings.Split(title, " ")
	if err != nil {
		Logger(MsgPrint{Error:err.Error(), Level:"error", Api:"GetKeywordsFromTitle"})
		return ""
	}
	for _, v := range lemmaStopWords {
		lemmaMap[lemm.Lemma(v)] = struct{}{}
	}
	for _, v := range xQString {
		tWord := lemm.Lemma(strings.ToLower(strings.Replace(strings.Replace(strings.Replace(v, "?", "", -1), ",", "", -1), ".", "", -1)))
		match, err := regexp.MatchString( `^[a-zA-Z\\+\\-\\_\\#]{0,12}$`, tWord)
		if err != nil {
			Logger(MsgPrint{Error:err.Error(), Level:"error", Api:"GetKeywordsFromTitle"})
			return ""
		} else if match {
			if _, ok := lemmaMap[((tWord))]; !ok {
				resultMap[tWord] = struct{}{}
			}
		}
	}
	for key := range resultMap{
		if resultStr != "" {
			resultStr += ", "
		}
		resultStr+= key
	}
	return resultStr
}
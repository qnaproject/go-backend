package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/guregu/null.v3"
	"strings"
	"time"
	"utils"
)

type NotificationData struct {
	UId              int      `json:"u_id"`
	OriginUId        int      `json:"origin_u_id"`
	NotificationType int      `json:"notification_type"`
	QACId            int      `json:"qac_id"`
	Message          string   `json:"message"`
	AId              null.Int `json:"a_id"`
	Preview 		 string `json:"preview"`
}

var notifTypes = map[int]string{
	1:  "question",
	2:  "answer",
	3:  "comment",
	4:  "answer request",
	5:  "trending",
	6:  "answer up-voted",
	7:  "answer down-voted",
	8:  "question deleted",
	9:  "answer deleted",
	10: "comment deleted",
	11: "article deleted",
	12: "article up-voted",
	13: "article commented",
	14:	"following",
}

func notifyUser(notifData NotificationData) (ok bool) {
	/*
		u_id int
		origin_u_id int
		notification_type int
		qac_id int
		a_id null.Int
		message string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nnotifyUser api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "notifyUser"})
		}
	}()

	checkData := func() (error, string, interface{}) {
		if notifData.UId < 1 {
			return errors.New("Invalid user id"), "warn", notifData.UId
		} else if _, ok := notifTypes[notifData.NotificationType]; !ok {
			return errors.New("Invalid notification type"), "warn", notifData.NotificationType
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, "error", nil
			}
			switch notifData.NotificationType {
			case 2:
				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				message := strings.Title(fname) + " answered your question."
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `a_id`, `u_id`, `message`, `preview`) VALUES(?,?,?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.AId, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 3:
				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				message := strings.Title(fname) + " commented on your answer."
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `a_id`, `u_id`, `message`, `preview`) VALUES(?,?,?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.AId, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 6, 7:

				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				var message string
				if notifData.NotificationType == 6 {
					message = strings.Title(fname) + " up-voted your answer."
				} else {
					message = strings.Title(fname) + " down-voted your answer."
				}
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `a_id`, `u_id`, `message`, `preview`) VALUES(?,?,?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.AId, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 8:
				var message = "A MOD/ADMIN has deleted one of your questions."
				_, err := tx.Exec("INSERT INTO `notification` (`notification_type`, `u_id`,`message`, `preview`) VALUES (?,?,?,?)", notifData.NotificationType, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 9:
				var message = "A MOD/ADMIN has deleted one of your answers."
				_, err := tx.Exec("INSERT INTO `notification` (`notification_type`, `u_id`,`message`, `preview`) VALUES (?,?,?,?)", notifData.NotificationType, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 10:
				var message = "A MOD/ADMIN has deleted one of your comments."
				_, err := tx.Exec("INSERT INTO `notification` (`notification_type`, `u_id`,`message`, `preview`) VALUES (?,?,?,?)", notifData.NotificationType, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 11:
				var message = "A MOD/ADMIN has deleted one of your articles."
				_, err := tx.Exec("INSERT INTO `notification` (`notification_type`, `u_id`,`message`,`preview`) VALUES (?,?,?,?)", notifData.NotificationType, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 12:
				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				var message string
				message = strings.Title(fname) + " liked your article."
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `u_id`, `message`, `preview`) VALUES(?,?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 13:
				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				message := strings.Title(fname) + " commented on your article."
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `u_id`, `message`, `preview`) VALUES(?,?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.UId, message, notifData.Preview)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			case 14:
				if notifData.QACId < 1 {
					tx.Rollback()
					return errors.New("Invalid qac_id"), "warn", nil
				}
				if notifData.OriginUId < 1 {
					tx.Rollback()
					return errors.New("Invalid origin uid"), "warn", nil
				}
				var fname string
				var lname null.String
				err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", notifData.OriginUId).Scan(&fname, &lname)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "warn", nil
					}
					return err, "error", nil
				}
				if lname.Valid && lname.String != "" {
					fname = fname + " " + lname.String
				}
				var message string
				message = strings.Title(fname) + " started following you."
				_, err = tx.Exec("INSERT INTO `notification`(`notification_type`, `qac_id`, `u_id`, `message`) VALUES(?,?,?,?)", notifData.NotificationType, notifData.QACId, notifData.UId, message)
				if err != nil {
					tx.Rollback()
					return err, "error", nil
				} else {
					tx.Commit()
					return nil, "info", nil
				}
			default:
				tx.Rollback()
				return nil, "info", nil
			}
		}
	}
	err, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: level, Api: "notifyUser"})
		return false
	} else {
		utils.Logger(utils.MsgPrint{Level: level, Api: "notifyUser"})
		return true
	}
}

func GetNotifications(sessionId string) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetNotifications api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetNotifications"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid uid/role"), utils.ErrGeneric, "error", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `n_id`, `notification_type`, `qac_id`, `a_id`, `message`, `created`, `notif_read`, `preview` FROM `notification` WHERE `u_id`=? LIMIT 200", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					type notifcationObj struct {
						NId              int       `json:"n_id" db:"n_id"`
						NotificationType int       `json:"notification_type" db:"notification_type"`
						QACId            null.Int  `json:"qac_id" db:"qac_id"`
						AId              null.Int  `json:"a_id" db:"a_id"`
						Message          string    `json:"message" db:"message"`
						Created          time.Time `json:"created" db:"created"`
						NotifRead        bool      `json:"notif_read" db:"notif_read"`
						Preview 		 null.String `json:"preview" db:"preview"`
					}
					var notifications []notifcationObj
					var notification notifcationObj
					for rows.Next() {
						err := rows.StructScan(&notification)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							notifications = append(notifications, notification)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched notifications", "info", notifications
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetNotifications"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetNotifications"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func SetNotificationAsRead(xNotifData []byte, sessionId string) string {
	/*n_id*/
	start := time.Now()
	defer func() {
		fmt.Println("\nSetNotificationAsRead api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "SetNotificationAsRead"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, _, ok := getUserIdAndRole(email); !ok {
			return errors.New("User suspended or terminated"), "Not authorised", "warn", nil
		} else {
			notifData := struct {
				NId int `json:"n_id"`
			}{}
			err := json.Unmarshal(xNotifData, &notifData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if notifData.NId < 1 {
				return errors.New("Invalid n_id"), utils.ErrGeneric, "warn", nil
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_uid int
				err := tx.QueryRowx("SELECT `u_id` from `notification` WHERE `n_id`=?", notifData.NId).Scan(&t_uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else if uid != t_uid {
					tx.Rollback()
					return errors.New("Unauthorised"), "Unauthorised", "warn", nil
				} else {
					_, err := tx.Exec("UPDATE `notification` SET `notif_read`=1 WHERE `n_id`=?", notifData.NId)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						tx.Commit()
						return nil, "successfully marked notificaton as read", "info", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "SetNotificationAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "SetNotificationAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func MarkAllNotificationsAsRead(sessionId string) string {
	/*
	 */
	start := time.Now()
	defer func() {
		fmt.Println("\nMarkAllNotificationsAsRead api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "MarkAllNotificationsAsRead"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid userid or role"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				_, err := tx.Exec("UPDATE `notification` SET `notif_read`=1 WHERE `u_id`=?", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					tx.Commit()
					return nil, "Successfully marked all notifications as read", "info", nil
				}
			}
		}
	}

	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "MarkAllNotificationsAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "MarkAllNotificationsAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func CRON_deleteOldNotifications() {
	apiStart := time.Now()
	defer func() { fmt.Println("\nCRON_deleteOldNotifications api execution time-> ", time.Since(apiStart)) }()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_deleteOldNotifications started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Msg: "Unable to begin transaction in CRON_deleteOldNotifications", Error: err.Error()})
		return
	} else {
		_, err := tx.Exec("DELETE FROM `notification` WHERE `created` < ?", time.Now().UTC().Add(-15*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error()})
			return
		} else {
			tx.Commit()
			return
		}
	}
}

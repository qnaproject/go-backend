package models

import (
	"crypto/hmac"
	"database/sql"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dchest/uniuri"
	"github.com/dgraph-io/badger"
	"github.com/futurenda/google-auth-id-token-verifier"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/sha3"
	"gopkg.in/guregu/null.v3"
	"gopkg.in/redis.v5"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
	"utils"
)

var DB *sqlx.DB
var UDB *badger.DB

func init() {
	var err error
	if os.Getenv("MYSQL_PWD") == "" {
		err := errors.New("ENV NOT SET : MYSQL_PWD")
		utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "ENV NOT SET : MYSQL_PWD", Error: err.Error()})
	}
	DB, err = sqlx.Open("mysql", "root:"+os.Getenv("MYSQL_PWD")+"@/ultimategoal?parseTime=true&charset=utf8mb4&collation=utf8mb4_unicode_ci")
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "COULD NOT CONNECT TO DATABASE", Error: err.Error()})
	}
	if os.Getenv("PWD_KEY") == "" {
		err := errors.New("ENV NOT SET : PWD_KEY")
		utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "ENV NOT SET : PWD_KEY", Error: err.Error()})
	}
	opts := badger.DefaultOptions
	opts.Truncate = true
	opts.Dir = "C:\\Users\\clyde\\projects\\go-backend\\src\\sessionStore"
	opts.ValueDir = "C:\\Users\\clyde\\projects\\go-backend\\src\\sessionStore"
	UDB, err = badger.Open(opts)
	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "fatal", Msg: "COuld not open badgerdb UDB", Error: err.Error()})
	}
}

/*User model*/

type UserModel struct {
	UId         int         `db:"u_id"`
	Phone       null.String `db:"phone" json:"phone"`
	Email       string      `db:"email" json:"email"`
	FName       string      `db:"fname" json:"fname"`
	LName       null.String `db:"lname" json:"lname"`
	Gender      null.String `db:"gender" json:"gender"`
	Role        int         `db:"role" json:"role"`
	DpUrl       null.String `db:"dp_url" json:"dp_url"`
	Rp          int         `db:"rp" json:"rp"`
	Points      int         `db:"points" json:"points"`
	Pro         int         `db:"pro" json:"pro"`
	UiMode      int         `db:"ui_mode" json:"ui_mode"`
	Password    string      `db:"password" json:"password"`
	Verified    bool        `db:"verified" json:"verified"`
	Created     time.Time   `db:"created" json:"created"`
	Updated     time.Time   `db:"updated" json:"updated"`
	Deprication int         `db:"deprication" json:"deprication"`
	Experience  int         `db:"experience" json:"experience"`
}

func CreateUser(xUserData []byte) string {
	/*
		email string
		fname string
		password string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CreateUser"})
		}
	}()
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nCreateUser ->Api Response Time: ", time.Since(apiTimeStart))
	}()

	var userData UserModel
	var responseBody utils.ResponseBodyJSON

	checkData := func() (error, string, string, interface{}) {
		if len(xUserData) < 1 {
			return errors.New("Invalid Request"), "Invalid Request", "error", string(xUserData)
		}

		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", string(xUserData)
		}
		/*Phone number validation*/
		if userData.Phone.Valid {
			userData.Phone.String = strings.TrimSpace(userData.Phone.String)
			if userData.Phone.String != "" {
				match, err := regexp.MatchString(`^[+][0-9]{8,15}$`, userData.Phone.String)
				if (err != nil) || (!match) {
					var t_err error
					if err != nil {
						t_err = err
					} else {
						t_err = errors.New("Invalid Phone number")
					}
					return t_err, "Invalid Phone Number", "warn", userData.Phone.String
				}
			} else {
				userData.Phone.Valid = false
			}
		}

		/*Email validation*/
		userData.Email = strings.TrimSpace(userData.Email)
		if userData.Email == "" {
			return errors.New("Email is required"), "Email is required", "warn", nil
		} else {
			userData.Email = strings.ToLower(userData.Email)
			matched, err := regexp.MatchString(`^\S+[@]\S+$`, userData.Email)
			if (err != nil) || (!matched) {
				var t_err error
				if err != nil {
					t_err = err
				} else {
					t_err = errors.New("Invalid Email")
				}
				return t_err, "Invalid Email", "warn", userData.Email
			}
		}

		/*Fname validation*/
		userData.FName = strings.TrimSpace(userData.FName)
		if userData.FName == "" {
			return errors.New("First Name is required"), "First Name is required", "warn", nil
		} else {
			userData.FName = strings.ToLower(userData.FName)
			match, err := regexp.MatchString(`^[a-z]{1,15}$`, userData.FName)
			if (err != nil) || (!match) {
				return errors.New("Invalid First Name"), "Invalid First Name", "warn", userData.FName
			}
		}

		/*LName validation*/
		if userData.LName.Valid {
			userData.LName.String = strings.TrimSpace(userData.LName.String)
			if userData.LName.String != "" {
				userData.LName.String = strings.ToLower(userData.LName.String)
				match, err := regexp.MatchString(`^[a-z]{1,15}$`, userData.LName.String)
				if (err != nil) || (!match) {
					return errors.New("Invalid Last Name"), "Invalid Last Name", "warn", userData.LName.String
				}
			} else {
				userData.LName.Valid = false
			}
		}

		/*Gender validation*/
		if userData.Gender.Valid {
			userData.Gender.String = strings.TrimSpace(userData.Gender.String)
			if userData.Gender.String != "" {
				userData.Gender.String = strings.ToLower(userData.Gender.String)
				if !(userData.Gender.String == "male" || userData.Gender.String == "female" || userData.Gender.String == "other" || userData.Gender.String == "prefer not to say") {
					return errors.New("Invalid Gender"), "Invalid Gender", "warn", userData.Gender.String
				}
			} else {
				userData.Gender.String = "prefer not to say"
			}
		} else {
			userData.Gender.Valid = true
			userData.Gender.String = "prefer not to say"
		}

		/*Password Operations*/
		userData.Password = strings.TrimSpace(userData.Password)
		if (userData.Password == "") || (len(userData.Password) < 8) || (len(userData.Password) > 20) {
			return errors.New(utils.ErrPasswordNotMatchingRequirements), "Invalid Password. Password must have Minimum 8 and Maximum 20 characters.", "warn", nil
		}
		if len(os.Getenv("PWD_KEY")) < 0 {
			return errors.New("password key not set!"), utils.ErrGeneric, "error", nil
		}
		userData.Password = encryptPassword(userData.Password, userData.Email)

		/*Begin DB operations*/
		tx, err := DB.Beginx()
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		}
		t_userData := UserModel{}
		err = tx.QueryRowx("SELECT * from `user` WHERE `email`=?", userData.Email).StructScan(&t_userData)
		if err != nil && err != sql.ErrNoRows {
			tx.Rollback()
			return err, utils.ErrGeneric, "error", nil
		} else if err != nil && err == sql.ErrNoRows {
			row, err := tx.NamedExec("INSERT into `user` (`phone`, `fname`, `lname`, `email`, `gender`, `password`) VALUES (:phone, :fname, :lname, :email, :gender, :password)", &userData)
			if err != nil {
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			}
			uId, _ := row.LastInsertId()
			randomString := uniuri.NewLen(16)
			_, err = tx.Exec("INSERT into `email_verify` (`verify_key`, `email`) VALUES (?, ?)", randomString, userData.Email)
			if err != nil {
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			}
			if uId > 0 {
				_, err := tx.Exec("INSERT INTO `message`(`from_user_id`, `to_user_id`, `message`) VALUES(?,?,?)", 1, uId, "Hi "+userData.FName+"! Wishing you a great experience! 🙂👍")
				fmt.Println(err)
			}
			tx.Commit()
			go func() {
				err := utils.SendAMail("no-reply@clevereply.com", userData.Email, "Verify Account", "<h3>Click on the following link to verify your account <p/><a rel='nofollow' href='http://clevereply.com/verify/"+randomString+"'>Verify Account</a></h3><p/><p/>You can also copy the following link if the above link isn't working</p><p/> https://clevereply.com/verify/"+randomString)
				if err != nil {
					utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: "Cannot send mail - in CreateUser", Level: "error"})
				}
			}()
			return nil, "Successfully created account. Please verify your email by clicking the link sent to your email.", "info", nil
		} else {
			tx.Rollback()
			return errors.New("An account already exists with this email address. Please log in."), "An account already exists with this email address. Please log in.", "warn", nil
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "CreateUser", Error: err.Error(), Level: level, Msg: msg + " in Create User", Data: data})
		responseBody.Success = false
		responseBody.Msg = msg
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "CreateUser", Level: level, Msg: msg, Data: data})
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func UpdateUser(xUserData []byte, sessionId string) string {
	/*
		fname string
	*/
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nUpdateUser ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UpdateUser"})
		}
	}()
	var userData UserModel
	var responseBody utils.ResponseBodyJSON

	checkData := func() (error, string, string, interface{}) {

		if len(xUserData) < 1 {
			return errors.New("Invalid Request"), "Invalid Request.", "warn", string(xUserData)
		}
		err := json.Unmarshal(xUserData, &userData)
		fmt.Printf("%+v", userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		}
		/*Phone number validation*/
		if userData.Phone.Valid {
			userData.Phone.String = strings.TrimSpace(userData.Phone.String)
			if userData.Phone.String != "" {
				match, err := regexp.MatchString(`^[+][0-9]{8,15}$`, userData.Phone.String)
				if (err != nil) || (!match) {
					var t_err error
					if err != nil {
						t_err = err
					} else {
						t_err = errors.New("Invalid Phone number")
					}
					return t_err, "Invalid Phone number", "warn", userData.Phone.String
				}
			} else {
				userData.Phone.Valid = false
			}
		}

		/*Email validation*/
		if t_email, ok := getLoggedInUserEmail(sessionId); ok {
			userData.Email = t_email
		} else {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		}

		if userData.Email == "" {
			return errors.New("Email is required"), "Email is required", "warn", nil
		}
		fnameUpdateRequired := "`fname`=:fname,"
		/*Fname validation*/
		userData.FName = strings.TrimSpace(userData.FName)
		if userData.FName != "" {
			userData.FName = strings.ToLower(userData.FName)
			match, err := regexp.MatchString(`^[a-z]{1,50}$`, userData.FName)
			if (err != nil) || (!match) {
				return errors.New("Invalid First Name"), "Invalid First Name", "warn", userData.FName
			}
		} else {
			fnameUpdateRequired = ""
		}

		/*LName validation*/
		if userData.LName.Valid {
			userData.LName.String = strings.TrimSpace(userData.LName.String)
			if userData.LName.String != "" {
				userData.LName.String = strings.ToLower(userData.LName.String)
				match, err := regexp.MatchString(`^[a-z]{1,50}$`, userData.LName.String)
				if (err != nil) || (!match) {
					return errors.New("Invalid Last Name"), "Invalid Last Name", "warn", userData.LName.String
				}
			} else {
				userData.LName.Valid = false
			}
		}

		/*Gender validation*/
		if userData.Gender.Valid {
			userData.Gender.String = strings.TrimSpace(userData.Gender.String)
			if userData.Gender.String != "" {
				userData.Gender.String = strings.ToLower(userData.Gender.String)
				if !(userData.Gender.String == "male" || userData.Gender.String == "female" || userData.Gender.String == "other" || userData.Gender.String == "prefer not to say") {
					return errors.New("Invalid Gender"), "Invalid Gender", "warn", userData.Gender.String
				}
			} else {
				userData.Gender.Valid = true
				userData.Gender.String = "prefer not to say"
			}
		} else {
			userData.Gender.Valid = true
			userData.Gender.String = "prefer not to say"
		}

		/*dp url operations*/
		if userData.DpUrl.Valid && len(userData.DpUrl.String) > 0 {
			if len(userData.DpUrl.String) < 36 {
				return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
			}
			if userData.DpUrl.String[:36] != "https://s3.ap-south-1.amazonaws.com/" {
				return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
			} else {
				t_dpLastPath := strings.Split(userData.DpUrl.String[36:], "/")
				if len(t_dpLastPath) < 2 {
					return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
				}
				if t_dpLastPath[0] != os.Getenv("S3_BUCKET_AVATAR") {
					return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
				}
				if len(t_dpLastPath[1]) < 5 {
					return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
				}
				if (t_dpLastPath[1])[len(t_dpLastPath[1])-4:] != ".jpg" {
					return errors.New("Invalid profile photo url"), "Invalid profile photo", "warn", nil
				}
			}
		} else {
			userData.DpUrl.Valid, userData.DpUrl.String = false, ""
		}

		/*Begin DB operations*/
		tx, err := DB.Beginx()
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		}
		t_userData := UserModel{}
		err = tx.QueryRowx("SELECT * from `user` WHERE `email`=?", userData.Email).StructScan(&t_userData)
		fmt.Println(err)
		if err != nil && err != sql.ErrNoRows {
			tx.Rollback()
			return err, utils.ErrGeneric, "error", nil
		} else if err != nil && err == sql.ErrNoRows {
			tx.Rollback()
			return errors.New("User doesn't exist"), "User doesn't exist", "error", userData.Email
		} else {
			if t_userData.Role == -1 || t_userData.Role == -2 {
				tx.Rollback()
				return errors.New("Account suspended"), "Your account is currently suspended", "", nil
			}
			_, err := tx.NamedExec("UPDATE `user` SET `phone`=:phone, "+fnameUpdateRequired+" `lname`=:lname, `gender`=:gender, `dp_url`=:dp_url WHERE `email`=:email", userData)
			if err != nil {
				responseBody.Msg = utils.ErrGeneric
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			}
			tx.Commit()
		}
		fmt.Printf("%+v", t_userData)

		return nil, "Successfully Updated User", "info", nil
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "UpdateUser", Error: err.Error(), Level: level, Msg: msg + " in UpdateUser", Data: data})
		responseBody.Success = false
		responseBody.Msg = msg
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "UpdateUser", Level: level, Msg: msg, Data: data})
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func GetUserDetails(sessionId string) string {

	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nGetUserDetails ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetUserDetails"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		email, ok := getLoggedInUserEmail(sessionId)
		if !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else {
			role, ok := hasRightToPost(email)
			if !ok {
				if role == -1 {
					return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
				} else if role == -2 {
					return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", email
				} else {
					return errors.New("invalid role"), utils.ErrGeneric, "warn", email
				}
			} else {
				userData := struct {
					Email  string      `json:"email" db:"email"`
					FName  string      `json:"fname" db:"fname"`
					LName  null.String `json:"lname" db:"lname"`
					Gender null.String `json:"gender" db:"gender"`
					DPUrl  null.String `json:"dp_url" db:"dp_url"`
					Points int         `json:"points" db:"points"`
					Pro    bool        `json:"pro" db:"pro"`
					Role   int         `json:"role" db:"role"`
				}{}

				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					err := tx.QueryRowx("SELECT `email`, `fname`, `lname`, `gender`, `dp_url`, `points`, `pro`, `role` FROM `user` WHERE `email`=?", email).StructScan(&userData)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						tx.Commit()
						return nil, "Successfully fetched profile info", "info", userData
					}
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetUserDetails"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetUserDetails"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func LoginUser(xUserData []byte) string {
	/*
		email string
		password string
		keep_logged_in bool
	*/
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nLoginUser ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "LoginUser"})
		}
	}()
	type loginData struct {
		Email        string `json:"email"`
		Password     string `json:"password"`
		KeepLoggedIn bool   `json:"keep_logged_in"`
	}
	checkData := func() (error, string, string, interface{}) {
		user := loginData{}
		err := json.Unmarshal(xUserData, &user)
		if err != nil {
			return err, utils.ErrGeneric, "error", string(xUserData)
		} else {
			user.Email = strings.TrimSpace(strings.ToLower(user.Email))
			if user.Email == "" {
				return errors.New("Email Required"), "Email Required", "warn", nil
			} else if user.Password == "" {
				return errors.New("Password Required"), "Password Required", "warn", nil
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var uPassword, fName string
				var uRole, uid int
				var verified bool
				var pro bool
				err := tx.QueryRowx("SELECT `password`, `role`, `fname`, `u_id`, `verified`, `pro` from `user` WHERE `email`=?", user.Email).Scan(&uPassword, &uRole, &fName, &uid, &verified, &pro)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return err, utils.ErrAccountNonExistant, "warn", user.Email
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if !verified {
					tx.Rollback()
					return errors.New("Email not verified."), "You have not verified your email address. Please verify to log in.", "warn", nil
				} else if uRole == -1 || uRole == -2 {
					if uRole == -1 {
						if checkForSuspensionReversal(tx, uid) {
							tx.Commit()
							return nil, utils.SuccessSuspensionLifted, "info", nil
						}
					}
					tx.Rollback()
					switch uRole {
					case -1:
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", user.Email
					case -2:
						return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", user.Email
					default:
						return errors.New("Invalid role"), utils.ErrGeneric, "error", user.Email
					}
				} else {
					shaHex := encryptPassword(user.Password, user.Email)
					if shaHex == uPassword {
						sessionId := uniuri.NewLen(20)
						err := UDB.Update(func(txn *badger.Txn) error {
							var err error
							//if user.KeepLoggedIn {
							//	err = txn.Set([]byte(sessionId), []byte(user.Email))
							//} else

							err = txn.SetWithTTL([]byte(sessionId), []byte(user.Email), 30*24*time.Hour)

							if err != nil {
								return err
							} else {
								return nil
							}
						})
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", user.Email
						} else {
							_, err := tx.Exec("INSERT INTO `user_session`(`email`, `session_id`) VALUES(?,?)", user.Email, sessionId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
							loginData := struct {
								SessionId string `json:"session_id"`
								Name      string `json:"name"`
								Pro       bool   `json:"pro"`
								UId       int    `json:u_id"`
								Role      int
							}{
								SessionId: sessionId,
								Name:      fName,
								Pro:       pro,
								UId:       uid,
								Role:      uRole,
							}
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								tx.Commit()
								return nil, utils.SuccessLoggedIn, "info", loginData
							}
						}

					} else {
						tx.Rollback()
						return errors.New("Incorrect Password"), "Incorrect Password", "warn", nil
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "LoginUser", Error: err.Error(), Msg: msg, Level: level, Data: data})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "LoginUser", Msg: msg, Level: level, Data: data})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = data
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func ContinueWithGoogle(xGoogleData []byte) (string) {
	/*
		token string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ContinueWithGoogle"})
		}
	}()
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nContinueWithGoogle ->Api Response Time: ", time.Since(apiTimeStart))
	}()

	checkData := func() (error, string, string, interface{}) {
		gData := struct{
			Token string `json:"token"`
		}{}

		err := json.Unmarshal(xGoogleData, &gData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			if gData.Token == "" || len(gData.Token) < 1 {
				return errors.New("Invalid gToken"), utils.ErrGeneric, "warn", nil
			} else {
				v := googleAuthIDTokenVerifier.Verifier{}
				aud := os.Getenv("G_AUD")
				if aud == "" {
					return errors.New("G_AUD not set"), utils.ErrGeneric, "error", nil
				}
				err := v.VerifyIDToken(gData.Token, []string{
					aud,
				})
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					claimSet, err := googleAuthIDTokenVerifier.Decode(gData.Token)
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						userData := struct{
							Email string `json:"email" db:"email"`
							FName string `json:"fname" db:"fname"`
							LName null.String `json:"lname" db:"lname"`
							DPUrl null.String `json:"dp_url" db:"dp_url"`
						}{}

						userData.Email = claimSet.Email
						if userData.Email == "" || len(userData.Email) < 1 {
							return errors.New("Invalid email"), utils.ErrGeneric, "warn", nil
						}
						if claimSet.GivenName == "" || len(claimSet.GivenName) < 3 {
							return errors.New("Invalid first name"), "Invalid First Name", "warn", nil
						} else {
							userData.FName = claimSet.GivenName
							fnames := strings.Split(userData.FName, " ")
							if len(fnames) > 1 {
								userData.FName = fnames[0]
							}
							userData.FName = strings.ToLower(userData.FName)
						}
						if claimSet.FamilyName != "" && len(claimSet.FamilyName) > 2 {
							userData.LName.Valid = true
							userData.LName.String = claimSet.FamilyName
							lnames := strings.Split(userData.LName.String, " ")
							if len(lnames) > 1 {
								userData.LName.String = lnames[0]
							}
							userData.LName.String = strings.ToLower(userData.LName.String)
						}
						//if claimSet.Picture != "" && len(claimSet.Picture) > 2 {
						//	userData.DPUrl.Valid = true
						//	userData.DPUrl.String = claimSet.Picture
						//}
						//userData.DPUrl.String = strings.Replace(userData.DPUrl.String, "s96", "s300", 3)
						tx, err := DB.Beginx()
						if err != nil {
							return err, utils.ErrGeneric, "error", nil
						} else {
							var t_email, fName string
							var t_role, uid int
							var verified bool
							var pro bool
							err := tx.QueryRowx("SELECT `email`, `role`, `fname`, `u_id`, `verified`, `pro` from `user` WHERE `email`=?", userData.Email).Scan(&t_email, &t_role, &fName, &uid, &verified, &pro)
							if err != nil {
								if err ==  sql.ErrNoRows {
									//create account
									res, err := tx.Exec("INSERT INTO `user`(`fname`, `lname`, `email`, `gender`, `verified`, `password`) VALUES(?,?,?,?,?,?)", userData.FName, userData.LName, userData.Email, "prefer not to say", 1, uniuri.NewLen(18))
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										id, err := res.LastInsertId()
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											//login
											sessionId := uniuri.NewLen(20)
											err := UDB.Update(func(txn *badger.Txn) error {
												var err error
												err = txn.Set([]byte(sessionId), []byte(userData.Email))
												if err != nil {
													return err
												} else {
													return nil
												}
											})
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", userData.Email
											} else {
												_, err := tx.Exec("INSERT INTO `user_session`(`email`, `session_id`) VALUES(?,?)", userData.Email, sessionId)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
												loginData := struct {
													SessionId string `json:"session_id"`
													Name      string `json:"name"`
													Pro       bool   `json:"pro"`
													UId       int    `json:u_id"`
													Role      int
												}{
													SessionId: sessionId,
													Name:      userData.FName,
													Pro:       false,
													UId:       int(id),
													Role:      0,
												}
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												} else {
													if id > 0 {
														_, err := tx.Exec("INSERT INTO `message`(`from_user_id`, `to_user_id`, `message`) VALUES(?,?,?)", 1, id, "Hi "+userData.FName+"! Wishing you a great experience! 🙂👍")
														fmt.Println(err)
													}
													tx.Commit()
													return nil, utils.SuccessLoggedIn, "info", loginData
												}
											}
										}
									}
								} else {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
							} else {
								if t_role == -1 {
									if checkForSuspensionReversal(tx, uid) {
										tx.Commit()
										return nil, utils.SuccessSuspensionLifted, "info", nil
									}
								}
								if t_email == "" || len(t_email) < 1{
									tx.Rollback()
									return errors.New("Inavalid email"), utils.ErrGeneric, "warn", nil
								} else {
									if t_role == -1 {
										tx.Rollback()
										return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
									} else if t_role == -2 {
										tx.Rollback()
										return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
									} else {
										//login
										sessionId := uniuri.NewLen(20)
										err := UDB.Update(func(txn *badger.Txn) error {
											var err error
											err = txn.SetWithTTL([]byte(sessionId), []byte(userData.Email), 30*24*time.Hour)
											if err != nil {
												return err
											} else {
												return nil
											}
										})
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", userData.Email
										} else {
											if(!verified) {
												_, err := tx.Exec("UPDATE USER SET `verified`=1 WHERE `email`=?", userData.Email)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
											}
											_, err := tx.Exec("INSERT INTO `user_session`(`email`, `session_id`) VALUES(?,?)", userData.Email, sessionId)
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											}
											loginData := struct {
												SessionId string `json:"session_id"`
												Name      string `json:"name"`
												Pro       bool   `json:"pro"`
												UId       int    `json:u_id"`
												Role      int
											}{
												SessionId: sessionId,
												Name:      fName,
												Pro:       pro,
												UId:       uid,
												Role:      t_role,
											}
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												tx.Commit()
												return nil, utils.SuccessLoggedIn, "info", loginData
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: level, Msg: msg, Data: data, Api:"ContinueWithGoogle"})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = false
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "ContinueWithGoogle", Level: level, Msg: msg, Data: data})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = true
		responseBody.Data = data
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func ContinueWithFacebook(xFacebookData []byte) (string) {
	/*
		token string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ContinueWithFacebook"})
		}
	}()
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nContinueWithFacebook ->Api Response Time: ", time.Since(apiTimeStart))
	}()

	checkData := func() (error, string, string, interface{}) {
		fData := struct{
			Token string `json:"token"`
		}{}

		err := json.Unmarshal(xFacebookData, &fData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			if fData.Token == "" || len(fData.Token) < 1 {
				return errors.New("Invalid fToken"), utils.ErrGeneric, "warn", nil
			} else {
				var wg sync.WaitGroup
				var asyncErr error
				fDebugData := struct {
					Data struct {
						AppId string `json:"app_id"`
						IsValid bool `json:"is_valid"`
						UserId string `json:"user_id"`
					} `json:"data"`
				}{}
				fUserData := struct {
					Id string `json:"id"`
					Email string `json:"email"`
					FirstName string `json:"first_name"`
					LastName string `json:"last_name"`
				}{}
				wg.Add(2)
				go func() {
					defer wg.Done()
					appId := os.Getenv("F_APP_ID")
					if appId == "" {
						asyncErr = errors.New("F_APP_ID not set")
						return
					}
					appSecret := os.Getenv("F_APP_SECRET")
					if appSecret == "" {
						asyncErr = errors.New("F_APP_SECRET not set")
						return
					}
					res, err := http.Get("https://graph.facebook.com/debug_token?input_token="+fData.Token+"&access_token="+appId+"|"+appSecret)
					if err != nil {
						asyncErr = err
						return
					} else {
						defer res.Body.Close()
						var resBody []byte
						resBody, err := ioutil.ReadAll(res.Body)
						if err != nil {
							asyncErr = err
							return
						} else {
							err := json.Unmarshal(resBody, &fDebugData)
							if err != nil {
								asyncErr = err
								return
							} else {
								return
							}
						}
					}
				}()

				go func() {
					defer wg.Done()
					res, err := http.Get("https://graph.facebook.com/v3.2/me?fields=id,email,first_name,last_name,verified&access_token="+fData.Token)
					if err != nil {
						asyncErr = err
						return
					} else {
						defer res.Body.Close()
						var resBody []byte
						resBody, err := ioutil.ReadAll(res.Body)
						if err != nil {
							asyncErr = err
							return
						} else {
							err := json.Unmarshal(resBody, &fUserData)
							if err != nil {
								asyncErr = err
								return
							} else {
								return
							}
						}
					}
				}()

				wg.Wait()

				if asyncErr != nil {
					return asyncErr, utils.ErrGeneric, "error", nil
				} else {
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						userData := struct{
							Email string `json:"email" db:"email"`
							FName string `json:"fname" db:"fname"`
							LName null.String `json:"lname" db:"lname"`
							DPUrl null.String `json:"dp_url" db:"dp_url"`
						}{}
						if !fDebugData.Data.IsValid {
							return errors.New("user not valid"), utils.ErrGeneric, "warn", nil
						}
						if fDebugData.Data.AppId != os.Getenv("F_APP_ID") {
							return errors.New("app mismatch"), utils.ErrGeneric, "warn", nil
						}
						if fDebugData.Data.UserId != fUserData.Id {
							return errors.New("facebook user id mismatch"), utils.ErrGeneric, "warn", nil
						}
						userData.Email = fUserData.Email
						if userData.Email == "" || len(userData.Email) < 1 {
							return errors.New("Invalid email"), utils.ErrGeneric, "warn", nil
						}
						if fUserData.FirstName == "" || len(fUserData.FirstName) < 3 {
							return errors.New("Invalid first name"), "Invalid First Name", "warn", nil
						} else {
							userData.FName = fUserData.FirstName
							fnames := strings.Split(userData.FName, " ")
							if len(fnames) > 1 {
								userData.FName = fnames[0]
							}
							userData.FName = strings.ToLower(userData.FName)
						}
						if fUserData.LastName != "" && len(fUserData.LastName) > 2 {
							userData.LName.Valid = true
							userData.LName.String = fUserData.LastName
							lnames := strings.Split(userData.LName.String, " ")
							if len(lnames) > 1 {
								userData.LName.String = lnames[0]
							}
							userData.LName.String = strings.ToLower(userData.LName.String)
						}
						//if claimSet.Picture != "" && len(claimSet.Picture) > 2 {
						//	userData.DPUrl.Valid = true
						//	userData.DPUrl.String = claimSet.Picture
						//}
						//userData.DPUrl.String = strings.Replace(userData.DPUrl.String, "s96", "s300", 3)
						tx, err := DB.Beginx()
						if err != nil {
							return err, utils.ErrGeneric, "error", nil
						} else {
							var t_email, fName string
							var t_role, uid int
							var verified bool
							var pro bool
							err := tx.QueryRowx("SELECT `email`, `role`, `fname`, `u_id`, `verified`, `pro` from `user` WHERE `email`=?", userData.Email).Scan(&t_email, &t_role, &fName, &uid, &verified, &pro)
							if err != nil {
								if err ==  sql.ErrNoRows {
									//create account
									res, err := tx.Exec("INSERT INTO `user`(`fname`, `lname`, `email`, `gender`, `verified`, `password`) VALUES(?,?,?,?,?,?)", userData.FName, userData.LName, userData.Email, "prefer not to say", 1, uniuri.NewLen(18))
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										id, err := res.LastInsertId()
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											//login
											sessionId := uniuri.NewLen(20)
											err := UDB.Update(func(txn *badger.Txn) error {
												var err error
												err = txn.Set([]byte(sessionId), []byte(userData.Email))
												if err != nil {
													return err
												} else {
													return nil
												}
											})
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", userData.Email
											} else {
												_, err := tx.Exec("INSERT INTO `user_session`(`email`, `session_id`) VALUES(?,?)", userData.Email, sessionId)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
												loginData := struct {
													SessionId string `json:"session_id"`
													Name      string `json:"name"`
													Pro       bool   `json:"pro"`
													UId       int    `json:u_id"`
													Role      int
												}{
													SessionId: sessionId,
													Name:      userData.FName,
													Pro:       false,
													UId:       int(id),
													Role:      0,
												}
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												} else {
													if id > 0 {
														_, err := tx.Exec("INSERT INTO `message`(`from_user_id`, `to_user_id`, `message`) VALUES(?,?,?)", 1, id, "Hi "+userData.FName+"! Wishing you a great experience! 🙂👍")
														fmt.Println(err)
													}
													tx.Commit()
													return nil, utils.SuccessLoggedIn, "info", loginData
												}
											}
										}
									}
								} else {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
							} else {
								if t_role == -1 {
									if checkForSuspensionReversal(tx, uid) {
										tx.Commit()
										return nil, utils.SuccessSuspensionLifted, "info", nil
									}
								}
								if t_email == "" || len(t_email) < 1{
									tx.Rollback()
									return errors.New("Inavalid email"), utils.ErrGeneric, "warn", nil
								} else {
									if t_role == -1 {
										tx.Rollback()
										return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
									} else if t_role == -2 {
										tx.Rollback()
										return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
									} else {
										//login
										sessionId := uniuri.NewLen(20)
										err := UDB.Update(func(txn *badger.Txn) error {
											var err error
											err = txn.SetWithTTL([]byte(sessionId), []byte(userData.Email), 30*24*time.Hour)
											if err != nil {
												return err
											} else {
												return nil
											}
										})
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", userData.Email
										} else {
											if(!verified) {
												_, err := tx.Exec("UPDATE USER SET `verified`=1 WHERE `email`=?", userData.Email)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
											}
											_, err := tx.Exec("INSERT INTO `user_session`(`email`, `session_id`) VALUES(?,?)", userData.Email, sessionId)
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											}
											loginData := struct {
												SessionId string `json:"session_id"`
												Name      string `json:"name"`
												Pro       bool   `json:"pro"`
												UId       int    `json:u_id"`
												Role      int
											}{
												SessionId: sessionId,
												Name:      fName,
												Pro:       pro,
												UId:       uid,
												Role:      t_role,
											}
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												tx.Commit()
												return nil, utils.SuccessLoggedIn, "info", loginData
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: level, Msg: msg, Data: data, Api:"ContinueWithFacebook"})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = false
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "ContinueWithFacebook", Level: level, Msg: msg, Data: data})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = true
		responseBody.Data = data
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func LogoutUser(sessionId string) string {
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nLogoutUser ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "LogoutUser"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New("Blank sessionId"), utils.ErrGeneric, "error", nil
		} else {
			err := UDB.Update(func(txn *badger.Txn) error {
				err := txn.Delete([]byte(sessionId))
				if err != nil {
					return err
				} else {
					return nil
				}
			})
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				return nil, utils.SuccessLoggedOut, "info", nil
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: level, Msg: msg, Data: data, Api:"LogoutUser"})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = false
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Api: "LogoutUser", Level: level, Msg: msg, Data: data})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Msg = msg
		responseBody.Success = true
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func ResendVerificationEmail(xUserData []byte) string {
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nResendVerificationEmail ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ResendVerificationEmail"})
		}
	}()
	type ResendUser struct {
		VerifyKey string `json:"verify_key" db:"verify_key"`
		Email     string `json:"email" db:"email"`
	}
	type ReturnedUser struct {
		Email    string `json:"email" db:"email"`
		Verified bool   `json:"verified" db:"verified"`
		Role     int    `json:"role" db:"role"`
	}

	checkData := func() (error, string, string, interface{}) {
		userData := ResendUser{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			/*Validate email*/
			if userData.Email == "" {
				return errors.New("Invalid Email"), "Invalid Email", "error", nil
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				returnedUser := ReturnedUser{}
				err := tx.QueryRowx("SELECT `email`, `verified`, `role` from `user` WHERE `email`=?", userData.Email).StructScan(&returnedUser)
				if err != nil {
					if err == sql.ErrNoRows {
						tx.Rollback()
						return errors.New("User doesn't exist"), utils.ErrAccountNonExistant, "warn", userData.Email
					} else {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					if returnedUser.Role == -1 || returnedUser.Role == -2 {
						tx.Rollback()
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "", nil
					} else if returnedUser.Verified == true {
						tx.Rollback()
						return nil, "Your account is already verified", "", nil
					} else {
						userData.VerifyKey = uniuri.New()
						randomString := userData.VerifyKey
						_, err := tx.NamedExec("INSERT into `email_verify` (`verify_key`, `email`) VALUES (:verify_key, :email)", &userData)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							tx.Commit()
							go func() {
								err := utils.SendAMail("no-reply@clevereplycom", userData.Email, "Verify Account", "<html><body><h3>Click on the following link to verify your account <p/><a rel='nofollow' href='http://clevereply.com/verify/"+randomString+"'>Verify Account</a></h3></body></html>")
								if err != nil {
									utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: "Cannot send mail - in CreateUser", Level: "error"})
								}
							}()
							return nil, "Mail resent successfully", "info", nil
						}
					}
				}
			}

		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		responseJson := utils.ResponseBodyJSON{}
		responseJson.Msg = msg
		responseJson.Success = false
		xResponseJson, _ := json.Marshal(responseJson)
		utils.Logger(utils.MsgPrint{Api: "ResendVerificationEmail", Level: level, Error: err.Error(), Msg: msg + " in ResendVerificationEmail", Data: data})
		return string(xResponseJson)
	} else {
		responseJson := utils.ResponseBodyJSON{}
		responseJson.Msg = msg
		responseJson.Success = true
		xResponseJson, _ := json.Marshal(responseJson)
		utils.Logger(utils.MsgPrint{Api: "ResendVerificationEmail", Level: level, Error: "", Msg: msg, Data: data})
		return string(xResponseJson)
	}
}

func VerifyUser(xVerifyData []byte) string {
	/*
		verify_key string
	*/
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nVerifyUser ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "VerifyUser"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		verifyData := struct {
			VerifyKey string `json:"verify_key"`
		}{}
		err := json.Unmarshal(xVerifyData, &verifyData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		}
		verifyString := verifyData.VerifyKey
		tx, err := DB.Beginx()
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			row := tx.QueryRowx("SELECT `email`, `created` from `email_verify` where `verify_key`=?", verifyString)
			var email string
			var created time.Time
			err := row.Scan(&email, &created)
			if err != nil {
				tx.Rollback()
				if err == sql.ErrNoRows {
					return errors.New("Invalid verification link"), "This verification link is invalid", "error", verifyString
				} else {
					return err, utils.ErrGeneric, "error", nil
				}
			} else {
				if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
					tx.Rollback()
					return errors.New("Verification link expired"), "Your verification link has expired. Please request another from the login page.", "warn", nil
				} else {
					var (
						verified bool
						role     int
					)
					row := tx.QueryRowx("SELECT `verified`, `role` from `user` where `email`=?", email)
					err := row.Scan(&verified, &role)
					if err != nil {
						if err == sql.ErrNoRows {
							tx.Rollback()
							return errors.New("Invalid Account"), "Account doesn't exist", "error", email
						} else {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", email
						}
					} else {
						if role == -1 || role == -2 {
							tx.Rollback()
							return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
						} else if verified == true {
							tx.Rollback()
							return nil, "Account already activated", "info", nil
						} else {
							_, err := tx.Exec("UPDATE `user` SET `verified`=1 WHERE `email`=?", email)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", email
							} else {
								tx.Commit()
								return nil, "Your account has been successfully verified", "info", nil
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		responseJson := utils.ResponseBodyJSON{}
		responseJson.Success = false
		responseJson.Msg = msg
		xResponseJson, _ := json.Marshal(responseJson)
		utils.Logger(utils.MsgPrint{Api: "VerifyUser", Level: level, Error: err.Error(), Msg: msg + " in VerifyUser", Data: data})
		return string(xResponseJson)
	} else {
		responseJson := utils.ResponseBodyJSON{}
		responseJson.Success = true
		responseJson.Msg = msg
		xResponseJson, _ := json.Marshal(responseJson)
		utils.Logger(utils.MsgPrint{Api: "VerifyUser", Level: level, Msg: msg + " in VerifyUser", Data: data})
		return string(xResponseJson)
	}
	return "Success?"
}

func ChangePassword(xUserData []byte, sessionId string) string {
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ChangePassword"})
		}
	}()
	type pwdData struct {
		OldPassword       string `json:"old_password"`
		RepeatNewPassword string `json:"repeat_new_password"`
		NewPassword       string `json:"new_password"`
	}

	checkData := func() (error, string, string, interface{}) {
		passwordData := pwdData{}
		err := json.Unmarshal(xUserData, &passwordData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if passwordData.OldPassword == "" {
			return errors.New("Invalid Old Password."), "Invalid Old Password.", "warn", nil
		} else if passwordData.OldPassword == passwordData.NewPassword {
			return errors.New("Old And New Passwords Cannot Be Same."), "Old And New Passwords Cannot Be Same.", "warn", nil
		} else if passwordData.NewPassword != passwordData.RepeatNewPassword {
			return errors.New("New Passwords Do Not Match"), "New Passwords Do Not Match", "warn", nil
		} else {
			passwordData.NewPassword = strings.TrimSpace(passwordData.NewPassword)
			if (passwordData.NewPassword == "") || (len(passwordData.NewPassword) < 8) || (len(passwordData.NewPassword) > 20) {
				return errors.New("Invalid Password. Password must have Minimum 8 and Maximum 20 characters."), "Invalid Password. Password must have Minimum 8 and Maximum 20 characters.", "warn", nil
			} else {
				if len(os.Getenv("PWD_KEY")) < 0 {
					return errors.New("password key not set!"), utils.ErrGeneric, "error", nil
				} else {
					email, ok := getLoggedInUserEmail(sessionId)
					if !ok {
						return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
					}
					shaHex := encryptPassword(passwordData.OldPassword, email)
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						var t_password string
						var role int
						{
							err := tx.QueryRowx("SELECT `password`,`role` from `user` WHERE email=?", email).Scan(&t_password, &role)
							if err != nil {
								tx.Rollback()
								if err == sql.ErrNoRows {
									return errors.New(utils.ErrAccountNonExistant), utils.ErrAccountNonExistant, "warn", email
								} else {
									return err, utils.ErrGeneric, "error", nil
								}
							} else {
								if role == -1 || role == -2 {
									tx.Rollback()
									return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
								} else {
									if shaHex != t_password {
										tx.Rollback()
										return errors.New(utils.ErrIncorrectPassword), utils.ErrIncorrectPassword, "warn", nil
									} else {
										shaHex := encryptPassword(passwordData.NewPassword, email)
										_, err := tx.Exec("UPDATE `user` SET `password`=? WHERE `email`=?", shaHex, email)
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											if ok := invalidateAllUserSessions(tx, email); !ok {
												tx.Rollback()
												return errors.New("Could not invalidate user sessions"), utils.ErrGeneric, "error", nil
											}
											tx.Commit()
											return nil, utils.SuccessChangedPassword, "info", nil
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "ChangePassword", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "ChangePassword", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func SuspendUser(xUserData []byte, sessionId string) string {
	/*
		u_id int
		reason string
		password string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "SuspendUser"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New("Invalid sessionId"), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/uid"), utils.ErrGeneric, "error", email
			}
		} else if role != 2 {
			return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", email
		} else {
			suspensionData := struct {
				UId      int    `json:"u_id"`
				Reason   string `json:"reason"`
				Password string `json:"password"`
			}{}

			err := json.Unmarshal(xUserData, &suspensionData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if suspensionData.UId < 1 {
				return errors.New("Invalid userid"), "Invalid UserId to suspend", "error", nil
			} else if suspensionData.Reason == "" {
				return errors.New("Reason not provided for suspension"), "Please provide a reason for the suspension.", "warn", nil
			} else if !checkPassword(email, suspensionData.Password) {
				return errors.New("Invalid password"), utils.ErrIncorrectPassword, "warn", nil
			} else {
				var dbUserRole int
				var suspendingEmail string
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					err := tx.QueryRowx("SELECT `role`,`email` FROM `user` WHERE `u_id`=?", suspensionData.UId).Scan(&dbUserRole, &suspendingEmail)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else if dbUserRole == -1 || dbUserRole == -2 {
						tx.Rollback()
						return errors.New("Account is already suspended"), "This account is already suspended.", "warn", nil
					} else {
						var suspendCount int
						err := tx.QueryRowx("SELECT COUNT(*) FROM `suspension` WHERE `u_id`=?", suspensionData.UId).Scan(&suspendCount)
						if err != nil && err != sql.ErrNoRows {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							if suspendCount > 1 {
								/*Terminate*/
								_, err := tx.Exec("UPDATE `user` SET `role`=-2 WHERE `u_id`=?", suspensionData.UId)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									invalidateAllUserSessions(tx, suspendingEmail)
									tx.Commit()
									return nil, "Account has been successfully terminated due to multiple suspensions.", "info", nil
								}
							} else {
								_, err := tx.Exec("UPDATE `user` SET `role`=-1, `deprication`=FLOOR(`deprication`/3) WHERE `u_id`=?", suspensionData.UId)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									_, err := tx.Exec("INSERT INTO `suspension`(`u_id`, `suspended_by`, `reason`) VALUES(?,?,?)", suspensionData.UId, uid, suspensionData.Reason)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										invalidateAllUserSessions(tx, suspendingEmail)
										tx.Commit()
										return nil, "Successfully suspended user.", "info", nil
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "SuspendUser", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "SuspendUser", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func DeleteUser(xUserData []byte, sessionId string) string {
	/*
		password string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteUser"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else {
			userData := struct{
				Password string `json:"password"`
			}{}
			err := json.Unmarshal(xUserData, &userData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				if userData.Password == "" {
					return errors.New("Password not provided"), "Incorrect password.", "warn", nil
				} else {
					if len(os.Getenv("PWD_KEY")) < 0 {
						return errors.New("password key not set!"), utils.ErrGeneric, "error", nil
					} else {
						shaHex := encryptPassword(userData.Password, email)
						tx, err := DB.Beginx()
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							var dbPass string
							var uid int
							err := tx.QueryRowx("SELECT `u_id`, `password` FROM `user` WHERE `email`=?", email).Scan(&uid, &dbPass)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								if dbPass == "" {
									tx.Rollback()
									return errors.New(utils.ErrGeneric), utils.ErrGeneric, "error", email
								} else {
									if dbPass != shaHex {
										tx.Rollback()
										return errors.New("Incorrect passoword"), "Incorrect password.", "warn", nil
									} else {
										if archiveUser(tx, uid) {
											if(uid > 0) {
												_, err := tx.Exec("DELETE FROM `user` WHERE `u_id`=?", uid)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", email
												} else {
													tx.Commit()
													return nil, "Successfully deleted user account", "info", email
												}
											} else {
												tx.Rollback()
												return errors.New("Invalid userid"), utils.ErrGeneric, "error", email
											}
										} else {
											tx.Rollback()
											return errors.New("Could not archive user"), utils.ErrGeneric, "error", email
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "DeleteUser", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "DeleteUser", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func SendPasswordResetMail(xUserData []byte) string {
	/*
		email string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "SendPasswordResetMail"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			Email string `json:"email"`
		}{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if userData.Email == "" {
			return errors.New("Invalid email"), "Invalid email", "warn", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var role int
				err := tx.QueryRowx("SELECT `role` FROM `user` WHERE `email`=?", userData.Email).Scan(&role)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesn't exist"), utils.ErrAccountNonExistant, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if role == -2 {
					tx.Rollback()
					return errors.New("Account has been terminated"), utils.ErrTerminated, "warn", nil
				} else {
					verifyKey := uniuri.NewLen(20)
					_, err := tx.Exec("INSERT INTO `password_reset`(`reset_key`, `email`) VALUES(?,?)", verifyKey, userData.Email)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						err = utils.SendAMail("no-reply@clevereply.com", userData.Email, "Reset Password", "<html><body><h3>Click on the following link to reset your password <p/><a rel='nofollow' href='https://clevereply.com/reset/"+verifyKey+"'>Reset Password</a></h3></body></html>")
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							tx.Commit()
							return nil, "An email has been sent with the link to reset your password.", "info", nil
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "SendPasswordResetMail", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "SendPasswordResetMail", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func VerifyPasswordResetKey(xKeyData []byte) string {
	/*
		verify_key
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "verifyPasswordResetKey"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		keyData := struct {
			VerifyKey string `json:"verify_key"`
		}{}
		err := json.Unmarshal(xKeyData, &keyData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if keyData.VerifyKey == "" {
			return errors.New("Empty password reset verify key"), "Invalid Link", "warn", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var created time.Time
				err := tx.QueryRowx("SELECT `created` FROM `password_reset` WHERE `reset_key`=?", keyData.VerifyKey).Scan(&created)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("Invalid password reset key"), utils.ErrInvalidPasswordResetLink, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
					tx.Rollback()
					return errors.New("Password verification link expired"), utils.ErrPasswordResetLinkExpired, "warn", nil
				} else {
					tx.Commit()
					return nil, "Valid Link", "info", keyData.VerifyKey
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "verifyPasswordResetKey", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "verifyPasswordResetKey", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = data
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func ResetPasswordWithResetKey(xVerifyData []byte) string {
	/*
		password string
		re_password string
		verify_key string
	*/
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "verifyPasswordResetKey"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		resetData := struct {
			Password   string `json:"password"`
			RePassword string `json:"re_password"`
			VerifyKey  string `json:"verify_key"`
		}{}

		err := json.Unmarshal(xVerifyData, &resetData)
		if err != nil {
			return err, utils.ErrGeneric, "error", "nil"
		} else if resetData.VerifyKey == "" {
			return errors.New("Invalid verify key"), utils.ErrGeneric, "warn", nil
		} else if resetData.Password == "" {
			return errors.New("Blank password"), utils.ErrPasswordRequired, "warn", nil
		} else if resetData.RePassword == "" {
			return errors.New("Re password not provided."), "Please re-enter the password", "warn", nil
		} else if resetData.Password != resetData.RePassword {
			return errors.New("The passwords do not match"), utils.ErrPasswordsDoNotMatch, "warn", nil
		} else {
			resetData.Password = strings.TrimSpace(resetData.Password)
			if len(resetData.Password) < 8 || len(resetData.Password) > 20 {
				return errors.New("Password too long or too short"), utils.ErrPasswordNotMatchingRequirements, "warn", nil
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var email string
				err := tx.QueryRowx("SELECT u.`email` FROM `password_reset` p, `user` u WHERE p.`reset_key`=? AND u.`email`=p.`email`", resetData.VerifyKey).Scan(&email)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("This account doesn't exist"), utils.ErrGeneric, "warn", nil
					}
					return err, utils.ErrGeneric, "error", nil
				} else {
					passwordHex := encryptPassword(resetData.Password, email)
					_, err := tx.Exec("UPDATE `user` SET `password`=? WHERE `email`=?", passwordHex, email)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						if ok := invalidateAllUserSessions(tx, email); !ok {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						tx.Exec("DELETE FROM `password_reset` WHERE `reset_key`=?", resetData.VerifyKey)
						tx.Commit()
						return nil, utils.SuccessPasswordReset, "info", nil
					}
				}

			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "verifyPasswordResetKey", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "verifyPasswordResetKey", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func GetPublicUserDetails(xUserData []byte, sessionId string) string {
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nGetPublicUserDetails ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetPublicUserDetails"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			UId int `json:"u_id"`
		}{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if userData.UId < 1 {
			return errors.New("Invalid userid"), utils.ErrGeneric, "error", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				userDBData := struct {
					FName   string      `json:"fname" db:"fname"`
					LName   null.String `json:"lname" db:"lname"`
					Gender  null.String `json:"gender" db:"gender"`
					DPUrl   null.String `json:"dp_url" db:"dp_url"`
					Points  int         `json:"points" db:"points"`
					Pro     bool        `json:"pro" db:"pro"`
					Created time.Time   `json:"created" db:"created"`
					Following bool `json:"following"`
				}{}
				if len(sessionId) > 0 {
					if email, ok := getLoggedInUserEmail(sessionId); ok {
						if uid, ok := getUserIdFromEmail(email); ok {
							var t_uid int
							err := tx.QueryRowx("SELECT `u_id` FROM `follow` WHERE `u_id`=? AND `fu_id`=?", uid, userData.UId).Scan(&t_uid)
							if err != nil {
								if err != sql.ErrNoRows {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
							} else {
								if t_uid == uid {
									userDBData.Following = true
								}
							}
						}
					}
				}
				err := tx.QueryRowx("SELECT `fname`, `lname`, `gender`, `dp_url`, `points`, `pro`, `created` FROM `user` WHERE `u_id`=?", userData.UId).StructScan(&userDBData)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User doesnt exist"), "This user account doesn't exist", "warn", nil
					}
					return err, utils.ErrGeneric, "error", nil
				} else {
					tx.Commit()
					return nil, "Successfuly fetched public user details", "info", userDBData
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "GetPublicUserDetails", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "GetPublicUserDetails", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = data
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func GetOtherPublicUserDetails(xUserData []byte) string {
	var apiTimeStart time.Time = time.Now()
	defer func() {
		fmt.Println("\nGetOtherPublicUserDetails ->Api Response Time: ", time.Since(apiTimeStart))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetOtherPublicUserDetails"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			UId int `json:"u_id"`
		}{}

		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if userData.UId < 0 {
			return errors.New("Invalid userid"), utils.ErrGeneric, "warn", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				resultData := struct {
					NumQuestions int `json:"num_questions"`
					NumAnswers   int `json:"num_answers"`
					NumArticles  int `json:"num_articles"`
				}{}

				err := tx.QueryRowx("SELECT COUNT(`u_id`) FROM `question` WHERE `u_id`=?", userData.UId).Scan(&resultData.NumQuestions)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("User account doesn't exist"), "User account doesn't exist", "warn", []int{}
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					err := tx.QueryRowx("SELECT COUNT(`u_id`) FROM `answer` WHERE `u_id`=?", userData.UId).Scan(&resultData.NumAnswers)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New("User account doesn't exist"), "User account doesn't exist", "warn", []int{}
						} else {
							return err, utils.ErrGeneric, "error", nil
						}
					} else {
						err := tx.QueryRowx("SELECT COUNT(`u_id`) FROM `article` WHERE `u_id`=?", userData.UId).Scan(&resultData.NumArticles)
						if err != nil {
							tx.Rollback()
							if err == sql.ErrNoRows {
								return errors.New("User account doesn't exist"), "User account doesn't exist", "warn", []int{}
							} else {
								return err, utils.ErrGeneric, "error", nil
							}
						}
					}
				}
				tx.Commit()
				return nil, "Successfully fetched other user data", "info", resultData
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "GetOtherPublicUserDetails", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "GetOtherPublicUserDetails", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = data
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func UploadAvatar(xImageData []byte, sessionId string) (response string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nUploadAvatar api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UploadAvatar"})
		}
	}()
	var email string
	var ok bool
	var uid int
	var role int
	if email, ok = getLoggedInUserEmail(sessionId); !ok {
		utils.Logger(utils.MsgPrint{Error: utils.ErrNotLoggedIn, Level: "warn", Api: "UploadAvatar"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = utils.ErrNotLoggedIn
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if uid, role, ok = getUserIdAndRole(email); !ok {
		switch role {
		case -1:
			utils.Logger(utils.MsgPrint{Error: utils.ErrSuspension, Level: "warn", Api: "UploadAvatar"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrSuspension
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		case -2:
			utils.Logger(utils.MsgPrint{Error: utils.ErrTerminated, Level: "warn", Api: "UploadAvatar"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrTerminated
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		default:
			utils.Logger(utils.MsgPrint{Error: ("invalid role/userid"), Level: "warn", Api: "UploadAvatar"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = "Something went wrong while uploading image."
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		}
	}
	// fmt.Println(string(xImageData))
	//fp, err := os.OpenFile("test.jpg", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0777)
	//if err != nil {
	//	return ""
	//} else
	//dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(string(xImageData)))

	res, err := base64.StdEncoding.DecodeString(string(xImageData))

	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadAvatar"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if (len(res) / 1024) > 300 {
		utils.Logger(utils.MsgPrint{Error: ("Image too large."), Level: "error", Api: "UploadAvatar"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Image too large."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	url, err := utils.AddFileToS3(res, 2, strconv.Itoa(uid))
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadAvatar"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Level: "info", Api: "UploadAvatar"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = true
		responseBody.Msg = "Successfully uploaded image."
		responseBody.Data = url
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}
}

func GetUserDpUrlAndPoints(xUserData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetUserDpUrlAndPoints api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetUserDpUrlAndPoints"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			UIds []int `json:"u_ids"`
		}{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			if len(userData.UIds) < 1 {
				return nil, "Successfully fetched user dps and points", "info", []int{}
			} else if len(userData.UIds) > 200 {
				return errors.New("Too many user ids"), "Too many requests", "warn", nil
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				}
				stmt, vars, err := sqlx.In("SELECT `u_id`, `dp_url`, `points` FROM `user` WHERE `u_id` IN(?)", userData.UIds)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					rows, err := tx.Queryx(stmt, vars...)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						type resultStruct struct {
							UId    int         `json:"u_id" db:"u_id"`
							DpUrl  null.String `json:"dp_url" db:"dp_url"`
							Points int         `json:"points" db:"points"`
						}
						var userInfo resultStruct
						var userInfos []resultStruct
						for rows.Next() {
							err := rows.StructScan(&userInfo)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								userInfos = append(userInfos, userInfo)
							}
						}
						tx.Commit()
						return nil, "Successfully fetched user ids and points", "info", userInfos
					}
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "GetUserDpUrlAndPoints", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "GetUserDpUrlAndPoints", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		responseBody.Data = data
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func UpdateUserBackground(xBgData []byte, sessionId string) (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nUpdateUserBackground api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UpdateUserBackground"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, ok := getUserIdFromEmail(email); !ok {
			return errors.New("Could not get userid from email"), utils.ErrGeneric, "warn", nil
		} else {
			bgData := struct{
				BgId int `json:"bg_id"`
			}{}
			err := json.Unmarshal(xBgData, &bgData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if bgData.BgId < 1 {
				return errors.New("Invalid bgId"), utils.ErrGeneric, "warn", nil
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					_, err = tx.Exec("INSERT INTO `bio`(`u_id`, `bg_id`) VALUES(?,?) ON DUPLICATE KEY UPDATE `bg_id`=?", uid, bgData.BgId, bgData.BgId)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						tx.Commit()
						return nil, "Successfully updated background", "info", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Api: "UpdateUserBackground", Error: err.Error(), Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = false
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	} else {
		utils.Logger(utils.MsgPrint{Api: "UpdateUserBackground", Level: level, Msg: msg})
		responseBody := utils.ResponseBodyJSON{}
		responseBody.Success = true
		responseBody.Msg = msg
		xResponseBody, _ := json.Marshal(responseBody)
		return string(xResponseBody)
	}
}

func GetLeaders() (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetLeaders api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetLeaders"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		result, err := utils.RDB.HGet("leaders", "1").Result()
		if err != nil {
			if err == redis.Nil || len(result) < 1 {
				go CRON_setLeaders()
				return errors.New("empty leaders set"), "No leaders.", "error", ""
			} else {
				return err, utils.ErrGeneric, "error", ""
			}
		}

		resultString := `{"success":"true", "msg":"Successfully fetched leaders", "data":[`


		resultString = resultString + result
		resultString += "]}"
		return nil, "Successfully fetched leaders", "info", resultString
	}
	err, msg, level, data1 := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetLeaders"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetLeaders"})
		return data1.(string)
	}
}

func NewFollow(xFollowData []byte, sessionId string) (string) {
	/*
	type int
	fu_id int
	f_cat string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nFollow api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "Follow"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid userid or role"), utils.ErrGeneric, "warn", email
			}
		} else {
			followData := struct{
				Type int `json:"type"`
				FUid int `json:"fu_id"`
				FCat string `json:"f_cat"`
			}{}
			err := json.Unmarshal(xFollowData, &followData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				if followData.Type == 0 && followData.FCat == "" {
					return errors.New("Invalid data"), utils.ErrGeneric, "warn", nil
				}
				if followData.Type == 1 && followData.FUid < 1 {
					return errors.New("Invalid data"), utils.ErrGeneric, "warn", nil
				}
				if followData.Type == 1 && followData.FUid == uid {
					return errors.New("You cannot follow yourself"), "You cannot follow yourself.", "warn", nil
				}
				var catId int
				if followData.Type == 0 {
					if catId, ok = CategoryList[followData.FCat]; !ok {
						return errors.New(utils.ErrInvalidCategory), utils.ErrInvalidCategory, "warn", followData.FCat
					}
				}

				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					if followData.Type == 0 {
						var t_fid int
						err := tx.QueryRowx("SELECT `f_id` FROM `follow` WHERE `u_id`=? AND `type`=? AND `fcat_id`=?", uid, followData.Type, catId).Scan(&t_fid)
						if err != nil {
							if err == sql.ErrNoRows {
								_, err := tx.Exec("INSERT INTO `follow`(`u_id`, `type`, `fcat_id`) VALUES(?,?,?)", uid, followData.Type, catId)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									tx.Commit()
									return nil, "Successfully following this category", "info", nil
								}
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							tx.Rollback()
							return errors.New("You are already following this category"),"You are already following this category.", "warn", nil
						}
					} else if followData.Type == 1 {
						var t_fid int
						err := tx.QueryRowx("SELECT `f_id` FROM `follow` WHERE `u_id`=? AND `type`=? AND `fu_id`=?", uid, followData.Type, followData.FUid).Scan(&t_fid)
						if err != nil {
							if err == sql.ErrNoRows {
								_, err := tx.Exec("INSERT INTO `follow`(`u_id`, `type`, `fu_id`) VALUES(?,?,?)", uid, followData.Type, followData.FUid)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									tx.Commit()
									go func() {
										notifyUser(NotificationData{
											UId: followData.FUid,
											NotificationType:14,
											QACId:uid,
											OriginUId:uid,
										})
									}()
									return nil, "Successfully following this user", "info", nil
								}
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							tx.Rollback()
							return errors.New("You are already following this user"), "You are already following this user", "warn", nil
						}
					} else {
						tx.Rollback()
						return errors.New("Invalid type"), utils.ErrGeneric, "warn", nil
					}
				}
			}
		}
	}

	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "Follow"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "Follow"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func Unfollow(xFollowData []byte, sessionId string) (string) {
	/*
	type int
	fu_id int
	f_cat string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nUnfollow api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "Unfollow"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid userid or role"), utils.ErrGeneric, "warn", email
			}
		} else {
			followData := struct {
				Type int    `json:"type"`
				FUid int    `json:"fu_id"`
				FCat string `json:"f_cat"`
			}{}
			err := json.Unmarshal(xFollowData, &followData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				if followData.Type == 0 && followData.FCat == "" {
					return errors.New("Invalid data"), utils.ErrGeneric, "warn", nil
				}
				if followData.Type == 1 && followData.FUid < 1 {
					return errors.New("Invalid data"), utils.ErrGeneric, "warn", nil
				}
				var catId int
				if followData.Type == 0 {
					if catId, ok = CategoryList[followData.FCat]; !ok {
						return errors.New(utils.ErrInvalidCategory), utils.ErrInvalidCategory, "warn", followData.FCat
					}
				}

				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					if followData.Type == 0 {
						var t_fid int
						err := tx.QueryRowx("SELECT `f_id` FROM `follow` WHERE `u_id`=? AND `type`=? AND `fcat_id`=?", uid, followData.Type, catId).Scan(&t_fid)
						if err != nil {
							if err == sql.ErrNoRows {
								tx.Rollback()
								return errors.New("You are not already following this category"), "You are not already following this category.", "warn", nil
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							_, err := tx.Exec("DELETE FROM `follow` WHERE `u_id`=? AND `type`=? AND `fcat_id`=?", uid, followData.Type, catId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								tx.Commit()
								return nil, "Successfully unfollowed this category", "info", nil
							}
						}
					} else if followData.Type == 1 {
						var t_fid int
						err := tx.QueryRowx("SELECT `f_id` FROM `follow` WHERE `u_id`=? AND `type`=? AND `fu_id`=?", uid, followData.Type, followData.FUid).Scan(&t_fid)
						if err != nil {
							if err == sql.ErrNoRows {
								tx.Rollback()
								return errors.New("You are not already following this user"), "You are not already following this user", "warn", nil
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							_, err := tx.Exec("DELETE FROM `follow` WHERE `u_id`=? AND `type`=? AND `fu_id`=?", uid, followData.Type, followData.FUid)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								tx.Commit()
								return nil, "Successfully unfollowed this user", "info", nil
							}
						}
					} else {
						tx.Rollback()
						return errors.New("Invalid type"), utils.ErrGeneric, "warn", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "Unfollow"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "UnFollow"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetUserFeed(sessionId string) (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetUserFeed api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetUserFeed"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok	 {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid role or userid"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				type userDataFormat struct{
					Type int `json:"type" db:"type"`
					FUId null.Int `json:"fu_id" db:"fu_id"`
					FCatId null.Int `json:"fcat_id" db:"fcat_id"`
				}
				var followUids []int
				var followCatids []int
				rows, err := tx.Queryx("SELECT `type`, `fu_id`, `fcat_id` FROM `follow` WHERE `u_id`=?", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					t_userFollowData := userDataFormat{}
					var numFollowEntries int
					for rows.Next() {
						numFollowEntries++
						err := rows.StructScan(&t_userFollowData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							if t_userFollowData.Type == 0 && t_userFollowData.FCatId.Valid{
								followCatids = append(followCatids, int(t_userFollowData.FCatId.Int64))
							} else if t_userFollowData.Type == 1 && t_userFollowData.FUId.Valid {
								followUids = append(followUids, int(t_userFollowData.FUId.Int64))
							}
						}
					}
					var feedQids []int
					type finalQ struct {
						FName string `json:"fname" db:"fname"`
						LName null.String `json:"lname" db:"lname"`
						UId int `json:"u_id" db:"u_id"`
						DPUrl null.String `json:"dp_url" db:"dp_url"`
						Title string `json:"title" db:"title"`
						Answer string `json:"answer" db:"answer"`
						QId int `json:"q_id" db:"q_id"`
						Created time.Time `json:"created" db:"created"`
						AId int `json:"a_id" db:"a_id"`
					}
					var finalQList []finalQ
					if numFollowEntries < 1 {
						tx.Rollback()
						return errors.New("You haven't followed any users or categories"), "You haven't followed any users or categories", "warn", nil
					} else {
						if len(followUids) > 0 {
							stmt, vars, err := sqlx.In("SELECT u.`fname`, u.`lname`, u.`u_id`, q.`title`, a.`answer`, q.q_id, a.`created`, u.`dp_url`, a.`a_id` FROM user u, question q, answer a WHERE a.u_id IN(?) AND q.q_id=a.q_id AND u.u_id=a.u_id AND `a`.`created` > ? ORDER BY `a`.`created`  DESC LIMIT 50", followUids, time.Now().UTC().Add(-2*24*time.Hour))
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								rows, err := tx.Queryx(stmt, vars...)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									t_finalStruct := finalQ{}
									for rows.Next() {
										err := rows.Scan(&t_finalStruct.FName, &t_finalStruct.LName, &t_finalStruct.UId, &t_finalStruct.Title, &t_finalStruct.Answer, &t_finalStruct.QId, &t_finalStruct.Created, &t_finalStruct.DPUrl, &t_finalStruct.AId)
										if err != nil {
											rows.Close()
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											finalQList = append(finalQList, t_finalStruct)
										}
									}
								}
							}
						}
						if len(followCatids) > 0 {
							stringFollowCatIds := []string{}
							for _, val := range followCatids {
								if catval, ok := ReverseCategoryList[val]; ok {
									stringFollowCatIds = append(stringFollowCatIds, catval)
								}
							}
							type trendingDataFormat struct{
								QId int `json:"q_id"`
								Title string `json:"title"`
								Activity int `json:"activity"`
								Category string `json:"category"`
								Created time.Time `json:"created"`
							}

							type trendingFormat struct{
								Success bool `json:"success"`
								Msg string `json:"msg"`
								Data []trendingDataFormat `json:"data"`
							}
							var trendingResponseStruct trendingFormat
							for _, val := range stringFollowCatIds {
								t_json, err := json.Marshal(struct{Category string `json:"category"`}{Category:val})
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
								trendingResponse := GetTrendingPosts(t_json)
								err = json.Unmarshal([]byte(trendingResponse), &trendingResponseStruct)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
								for _, q := range trendingResponseStruct.Data {
									feedQids = append(feedQids, q.QId)
								}
							}
							if len(feedQids) > 0 {
								stmt, vars, err := sqlx.In("SELECT * FROM (SELECT u.`fname`, u.`lname`, u.`u_id`, q.`title`, a.`answer`, q.q_id, a.`created`, u.`dp_url`, a.`a_id`, (CAST(a.`up_votes` AS SIGNED) - CAST(a.`down_votes` AS SIGNED)) AS score FROM user u, question q, answer a WHERE a.q_id IN(?) AND q.q_id=a.q_id AND u.u_id=a.u_id ORDER BY score DESC LIMIT 1000) s1 GROUP BY s1.q_id LIMIT 50", feedQids)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									rows, err := tx.Queryx(stmt, vars...)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										t_finalStruct := finalQ{}
										for rows.Next() {
											var discard int
											err := rows.Scan(&t_finalStruct.FName, &t_finalStruct.LName, &t_finalStruct.UId, &t_finalStruct.Title, &t_finalStruct.Answer, &t_finalStruct.QId, &t_finalStruct.Created, &t_finalStruct.DPUrl, &t_finalStruct.AId, &discard)
											if err != nil {
												rows.Close()
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												finalQList = append(finalQList, t_finalStruct)
											}
										}
									}
								}
							}
						}
						tx.Commit()
						return nil, "Successfully fetched feed", "Successfully fetched feed", finalQList
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetUserFeed"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetUserFeed"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetFollowingCategories(sessionId string) (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetFollowingCategories api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetFollowingCategories"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid role or userid"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var catList []string
				type userDataFormat struct {
					Type   int      `json:"type" db:"type"`
					FUId   null.Int `json:"fu_id" db:"fu_id"`
					FCatId null.Int `json:"fcat_id" db:"fcat_id"`
				}
				var followCatids []int
				rows, err := tx.Queryx("SELECT `type`, `fu_id`, `fcat_id` FROM `follow` WHERE `u_id`=? AND `type`=0", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					t_userFollowData := userDataFormat{}
					var numFollowEntries int
					for rows.Next() {
						numFollowEntries++
						err := rows.StructScan(&t_userFollowData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							if t_userFollowData.Type == 0 && t_userFollowData.FCatId.Valid {
								followCatids = append(followCatids, int(t_userFollowData.FCatId.Int64))
							}
						}
					}
					for _, val := range followCatids {
						if cat, ok := ReverseCategoryList[val]; ok {
							catList = append(catList, cat)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched follow categories", "info", catList
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetFollowingCategories"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetFollowingCategories"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetFollowingUsers(sessionId string) (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetFollowingUsers api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetFollowingUsers"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid role or userid"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				type userDataFormat struct {
					FName  string `json:"fname" db:"fname"`
					LName null.String `json:"lname" db:"lname"`
					UId int `json:"u_id" db:"u_id"`
					DPUrl null.String `json:"dp_url" db:"dp_url"`
				}
				userDatas := []userDataFormat{}
				rows, err := tx.Queryx("SELECT `u_id`, `fname`, `lname`, `dp_url` FROM `user` WHERE `u_id` IN(SELECT `fu_id` FROM `follow` WHERE `u_id`=? AND `type`=1 AND `fu_id` IS NOT NULL)", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					t_userFollowData := userDataFormat{}
					var numFollowEntries int
					for rows.Next() {
						numFollowEntries++
						err := rows.StructScan(&t_userFollowData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							userDatas = append(userDatas, t_userFollowData)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched following users", "info", userDatas
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetFollowingUsers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetFollowingUsers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetAllUserFollowers(sessionId string) (string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllUserFollowers api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllUserFollowers"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid role or userid"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				type userDataFormat struct {
					FName  string `json:"fname" db:"fname"`
					LName null.String `json:"lname" db:"lname"`
					UId int `json:"u_id" db:"u_id"`
					DPUrl null.String `json:"dp_url" db:"dp_url"`
				}
				userDatas := []userDataFormat{}
				rows, err := tx.Queryx("SELECT `u_id`, `fname`, `lname`, `dp_url` FROM `user` WHERE `u_id` IN(SELECT `u_id` FROM `follow` WHERE `fu_id`=? AND `type`=1)", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					t_userFollowData := userDataFormat{}
					var numFollowEntries int
					for rows.Next() {
						numFollowEntries++
						err := rows.StructScan(&t_userFollowData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							userDatas = append(userDatas, t_userFollowData)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched followers", "info", userDatas
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllUserFollowers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllUserFollowers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

/*Internal*/
func getLoggedInUserEmail(sessionId string) (string, bool) {
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "getLoggedInUserEmail"})
		}
	}()
	if sessionId == "" {
		return "", false
	}
	var t_email []byte
	err := UDB.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(sessionId))
		if err != nil {
			return err
		} else {
			t_email, err = item.ValueCopy(nil)
			if err != nil {
				return err
			} else {
				return nil
			}
		}
	})
	email := string(t_email)
	if email == "" {
		return "", false
	}
	if err != nil {
		if err == badger.ErrKeyNotFound || err == badger.ErrEmptyKey || err == badger.ErrInvalidKey {

		} else {
			utils.Logger(utils.MsgPrint{Api: "getLoggedInUserEmail", Level: "error", Error: err.Error()})
		}
		return "", false
	} else {
		return email, true
	}
}

func getUserIdFromEmail(email string) (int, bool) {
	if email == "" {
		return -1, false
	} else {
		var UId int
		err := DB.QueryRowx("SELECT `u_id` FROM `user` WHERE `email`=?", email).Scan(&UId)
		if err != nil {
			utils.Logger(utils.MsgPrint{Level: "error", Api: "getUserIdFromEmail", Error: err.Error()})
			return -1, false
		} else if UId < 1 {
			return -1, false
		} else {
			return UId, true
		}

	}
}

func hasRightToPost(email string) (int, bool) {
	if email == "" {
		return -99, false
	}
	var role int
	err := DB.QueryRowx("SELECT `role` FROM `user` WHERE `email`=?", email).Scan(&role)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Api: "hasRightToPost", Level: "error"})
		return -99, false
	} else if role < 0 {
		return role, false
	} else {
		return role, true
	}

}

func getUserIdAndRole(email string) (UId, Role int, ok bool) {
	if email == "" {
		return -1, -99, false
	} else {
		err := DB.QueryRowx("SELECT `u_id`, `role` FROM `user` WHERE `email`=?", email).Scan(&UId, &Role)
		if err != nil {
			utils.Logger(utils.MsgPrint{Level: "error", Api: "getUserIdAndRole", Error: err.Error()})
			return -1, -99, false
		} else if UId < 1 {
			return -1, Role, false
		} else if Role < 0 {
			return UId, Role, false
		} else {
			return UId, Role, true
		}

	}
}

func getUserIdRolePoints(email string) (UId, Role, Points int, ok bool) {
	if email == "" {
		return -1, -99, 0, false
	} else {
		err := DB.QueryRowx("SELECT `u_id`, `role`, `points` FROM `user` WHERE `email`=?", email).Scan(&UId, &Role, &Points)
		if err != nil {
			utils.Logger(utils.MsgPrint{Level: "error", Api: "getUserIdAndRole", Error: err.Error()})
			return -1, -99, 0, false
		} else if UId < 1 {
			return -1, Role, Points, false
		} else if Role < 0 {
			return UId, Role, Points, false
		} else {
			return UId, Role, Points, true
		}

	}
}

func addUserPoints(tx *sqlx.Tx, uid, points int) bool {
	if uid < 1 {
		return false
	} else if points == 0 {
		return false
	} else {
		var uPoints int
		var pro bool
		err := tx.QueryRowx("SELECT `points`, `pro` FROM `user` WHERE `u_id`=?", uid).Scan(&uPoints, &pro)
		if pro && points < 0 {
			switch {
			case uPoints >= 1000000:
				if uPoints+points < 1000000 {
					points = -(uPoints - 1000000)
				}
			case uPoints >= 100000:
				if uPoints+points < 100000 {
					points = -(uPoints - 100000)
				}
			case uPoints >= 10000:
				if uPoints+points < 10000 {
					points = -(uPoints - 10000)
				}
			case uPoints >= 1000:
				if uPoints+points < 1000 {
					points = -(uPoints - 1000)
				}
			case uPoints >= 100:
				if uPoints+points < 100 {
					points = -(uPoints - 100)
				}
			}
			if points == 0 {
				return true
			}
		}
		pointFragment := "GREATEST(`points`+" + fmt.Sprint(points) + ",0)"
		_, err = tx.Exec("UPDATE `user` SET `points`="+pointFragment+" WHERE `u_id`=?", uid)
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "addUserPoints"})
			return false
		} else {
			return true
		}
	}
}

func checkForSuspension(tx *sqlx.Tx, uid int) bool {
	if uid < 1 {
		return false
	}
	var experience, deprication int
	err := tx.QueryRowx("SELECT `experience`, `deprication` FROM `user` WHERE `u_id`=?", uid).Scan(&experience, &deprication)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkForSuspension"})
		return false
	} else if deprication < 1 {
		return true
	} else if experience < 10 {
		return true
	} else if float32(deprication)*100/float32(experience) > 20 {
		var suspensionCount int
		tx.QueryRowx("SELECT COUNT(*) FROM `suspension` WHERE `u_id`=?", uid).Scan(&suspensionCount)
		if suspensionCount > 1 {
			_, err := tx.Exec("UPDATE `user` SET `deprication`=FLOOR(`deprication`/3), `role`=-2 WHERE `u_id`=?", uid)
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkForSuspension"})
				return false
			}
			_, err = tx.Exec("INSERT INTO `suspension`(`u_id`)VALUES(?)", uid)
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkForSuspension"})
				return false
			}
		} else {
			_, err := tx.Exec("UPDATE `user` SET `deprication`=FLOOR(`deprication`/3), `role`=-1 WHERE `u_id`=?", uid)
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkForSuspension"})
				return false
			}
			_, err = tx.Exec("INSERT INTO `suspension`(`u_id`)VALUES(?)", uid)
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkForSuspension"})
				return false
			}
		}
		invalidateAllUserSessionsWithUId(tx, uid)
		return true
	} else {
		return true
	}
}

func checkForSuspensionReversal(tx *sqlx.Tx, uid int) bool {
	var suspendedDate time.Time
	err := tx.QueryRowx("SELECT `created` FROM `suspension` WHERE `u_id`=? ORDER BY `created` DESC LIMIT 1", uid).Scan(&suspendedDate)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Msg: "Error in query when finding the suspension time", Api: "checkForSuspensionReversal"})
		return false
	}
	if suspendedDate.UTC().Before(time.Now().UTC().Add(-7 * 24 * time.Hour)) {
		_, err := tx.Exec("UPDATE `user` SET `role`=0 WHERE `u_id`=?", uid)
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Msg: "Error in query when relieving suspension", Api: "checkForSuspensionReversal"})
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

func checkPassword(email string, password string) bool {
	if email == "" {
		return false
	}
	tx, err := DB.Beginx()
	var uPassword string
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkPassword"})
		return false
	} else {
		err = tx.QueryRowx("SELECT `password` FROM `user` WHERE `email`=?", email).Scan(&uPassword)
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "checkPassword"})
			return false
		}
	}
	tx.Commit()
	shaHex := encryptPassword(password, email)
	if shaHex == uPassword {
		return true
	} else {
		return false
	}
}

func encryptPassword(password, email string) string {
	h := hmac.New(sha3.New384, []byte(os.Getenv("PWD_KEY")))
	//salt: first char of email append to password, last char of email prepend to password
	password = string(append([]rune(password), rune([]rune(email)[0])))
	password = string(append([]rune{[]rune(email)[len(email)-1]}, []rune(password)...))
	h.Write([]byte(password))
	sha := h.Sum(nil)
	shaHex := hex.EncodeToString(sha)
	return shaHex
}

func CRON_runUDBGarbageCollection() {
	apiStart := time.Now()
	defer func() { fmt.Println("\nCRON_runUDBGarbageCollection api execution time-> ", time.Since(apiStart)) }()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_runUDBGarbageCollection started"})
	UDB.RunValueLogGC(0.1)
}

func CRON_setLeaders() {
	apiStart := time.Now()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_setLeaders"})
		}
	}()
	defer func() { fmt.Println("\nCRON_setLeaders api execution time-> ", time.Since(apiStart)) }()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_setLeaders started"})
	tx, err := DB.Beginx()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLeaders"})
		return
	}
	type leader struct {
		UId      int       `json:"u_id" db:"u_id"`
		FName    string    `json:"fname" db:"fname"`
		LName null.String `json:"lname" db:"lname"`
		Points int    `json:"points"`
		DPUrl null.String `json:"dp_url" db:"dp_url"`
	}
	leaders := []leader{}
	rows, err := tx.Queryx("SELECT `u_id`, `fname`, `lname`, `points`, `dp_url` FROM `user` WHERE `role`>-1 ORDER BY `points` DESC LIMIT 10")
	if err != nil {
		tx.Rollback()
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLeaders"})
	} else {
		t_leader := leader{}
		for rows.Next() {
			err := rows.StructScan(&t_leader)
			if err != nil {
				rows.Close()
				tx.Rollback()
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLeaders"})
			} else {
				leaders = append(leaders, t_leader)
			}
		}

		responseJson, err := json.Marshal(leaders)
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLeaders"})
			return
		}
		tx.Commit()
		utils.RDB.HSet("leaders", "1", responseJson)

	}
}

func invalidateAllUserSessions(tx *sqlx.Tx, email string) (ok bool) {
	{
		rows, err := tx.Queryx("SELECT `session_id` FROM `user_session` WHERE `email`=?", email)
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "invalidateAllUserSessions"})
			return false
		} else {
			var xSessionId [][]byte
			for rows.Next() {
				var t_sid string
				err = rows.Scan(&t_sid)
				if err != nil {
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "invalidateAllUserSessions"})
					return false
				} else {
					xSessionId = append(xSessionId, []byte(t_sid))
				}
			}
			wb := UDB.NewWriteBatch()
			defer wb.Cancel()
			for _, v := range xSessionId {
				wb.Delete(v)
			}
			err := wb.Flush()
			if err != nil {
				wb.Cancel()
				return false
			} else {
				return true
			}
		}
	}
}

func invalidateAllUserSessionsWithUId(tx *sqlx.Tx, uid int) (ok bool) {
	{
		var email string
		err := tx.QueryRowx("SELECT `email` FROM `user` WHERE `u_id`=?", uid).Scan(&email)
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "invalidateAllUserSessions"})
			return false
		}
		rows, err := tx.Queryx("SELECT `session_id` FROM `user_session` WHERE `email`=?", email)
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "invalidateAllUserSessions"})
			return false
		} else {
			var xSessionId [][]byte
			for rows.Next() {
				var t_sid string
				err = rows.Scan(&t_sid)
				if err != nil {
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "invalidateAllUserSessions"})
					return false
				} else {
					xSessionId = append(xSessionId, []byte(t_sid))
				}
			}
			wb := UDB.NewWriteBatch()
			defer wb.Cancel()
			for _, v := range xSessionId {
				wb.Delete(v)
			}
			err := wb.Flush()
			if err != nil {
				wb.Cancel()
				return false
			} else {
				return true
			}
		}
	}
}

func CRON_deleteOldVerifyEmailEntries() {
	apiStart := time.Now()
	defer func() {fmt.Println("\nCRON_deleteOldVerifyEmailEntries api execution time-> ", time.Since(apiStart))} ()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_deleteOldVerifyEmailEntries"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level:"info", Msg:"CRON_deleteOldVerifyEmailEntries started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"error", Msg:"Unable to begin transaction in CRON_deleteOldVerifyEmailEntries", Error:err.Error()})
	} else {
		_, err := tx.Exec("DELETE from `email_verify` WHERE `created`<?", time.Now().UTC().Add(-3*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in exec in CRON_deleteOldVerifyEmailEntries", Error:err.Error()})
		} else {
			tx.Commit()
		}
	}
}

func CRON_deleteUnverifiedUserAccounts() {
	apiStart := time.Now()
	defer func() {fmt.Println("\nCRON_deleteUnverifiedUserAccounts api execution time-> ", time.Since(apiStart))} ()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_deleteUnverifiedUserAccounts"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level:"info", Msg:"CRON_deleteUnverifiedUserAccounts started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"error", Msg:"Unable to begin transaction in CRON_deleteUnverifiedUserAccounts", Error:err.Error()})
	} else {
		_, err := tx.Exec("DELETE from `user` WHERE `created`<? AND `verified`=0", time.Now().UTC().Add(-2*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in exec in CRON_deleteUnverifiedUserAccounts", Error:err.Error()})
		} else {
			tx.Commit()
		}
	}
}

func CRON_deleteOldEmailSessionIdLinkage() {
	apiStart := time.Now()
	defer func() {fmt.Println("\nCRON_deleteOldEmailSessionIdLinkage api execution time-> ", time.Since(apiStart))} ()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_deleteOldEmailSessionIdLinkage"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level:"info", Msg:"CRON_deleteOldEmailSessionIdLinkage started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"error", Msg:"Unable to begin transaction in CRON_deleteOldEmailSessionIdLinkage", Error:err.Error()})
	} else {
		_, err := tx.Exec("DELETE from `user_session` WHERE `created`<?", time.Now().UTC().Add(-45*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in exec in CRON_deleteOldEmailSessionIdLinkage", Error:err.Error()})
		} else {
			tx.Commit()
		}
	}
}

func archiveUser(tx *sqlx.Tx, u_id int) bool {
	if u_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_user` SELECT a.*, UTC_TIMESTAMP() FROM `user` `a` WHERE `u_id`=?", u_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveUser"})
		return false
	}
	return true
}

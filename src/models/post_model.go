package models

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/jmoiron/sqlx"
	"os"
	"strings"
	"time"
	"utils"
)
var S3Session *session.Session

func init() {
	var err error
	S3Session, err= session.NewSession(&aws.Config{Region: aws.String(os.Getenv("S3_BUCKET_REGION"))})
	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"fatal", Error:err.Error(), Msg:"COULD NOT CREATE S3 SESSION"})
	}
}

func UploadImage(xImageData []byte, sessionId string) (response string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nUploadImage api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UploadImage"})
		}
	}()
	var email string
	var ok bool
	if email, ok = getLoggedInUserEmail(sessionId); !ok {
		utils.Logger(utils.MsgPrint{Error: utils.ErrNotLoggedIn, Level: "warn", Api: "UploadImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = utils.ErrNotLoggedIn
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if role, ok := hasRightToPost(email); !ok {
		switch role {
		case -1:
			utils.Logger(utils.MsgPrint{Error: utils.ErrSuspension, Level: "warn", Api: "UploadImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrSuspension
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		case -2:
			utils.Logger(utils.MsgPrint{Error: utils.ErrTerminated, Level: "warn", Api: "UploadImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrTerminated
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		default:
			utils.Logger(utils.MsgPrint{Error: ("invalid role/userid"), Level: "warn", Api: "UploadImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = "Something went wrong while uploading image."
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		}
	}
	// fmt.Println(string(xImageData))
	//fp, err := os.OpenFile("test.jpg", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0777)
	//if err != nil {
	//	return ""
	//} else
	//dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(string(xImageData)))

	res, err := base64.StdEncoding.DecodeString(string(xImageData))
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if (len(res) / 1024) > 300 {
		utils.Logger(utils.MsgPrint{Error: ("Image too large."), Level: "error", Api: "UploadImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Image too large."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	url, err := utils.AddFileToS3(res, 1, "")
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		var id int64
		tx, err := DB.Beginx()
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = "Something went wrong while uploading image."
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		} else {
			if url != "" {
				res, err := tx.Exec("INSERT INTO `image_track`(`url`, `type`) VALUES(?,?)",url, 0)
				if err != nil {
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadImage"})
					var responseBody utils.ResponseBodyJSON
					responseBody.Success = false
					responseBody.Msg = "Something went wrong while uploading image."
					responseBody.Data = nil
					xResponseJson, _ := json.Marshal(responseBody)
					return string(xResponseJson)
				} else {
					id, err = res.LastInsertId()
					if err != nil {
						tx.Rollback()
						utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadImage"})
						var responseBody utils.ResponseBodyJSON
						responseBody.Success = false
						responseBody.Msg = "Something went wrong while uploading image."
						responseBody.Data = nil
						xResponseJson, _ := json.Marshal(responseBody)
						return string(xResponseJson)
					} else {
					}
				}
			}
			tx.Commit()
			utils.Logger(utils.MsgPrint{Level: "info", Api: "UploadImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = true
			responseBody.Msg = "Successfully uploaded image."
			responseBody.Data = struct{
				URL string `json:"url"`
				TrackId int `json:"track_id"`
			}{
				URL: url,
				TrackId:int(id),
			}
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		}
	}
}

func GetImageUrlFromTrackIds(xTrackData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetImageUrlFromTrackIds api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetImageUrlFromTrackIds"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		trackData := struct{
			Ids []int `json:"ids"`
			Type int `json:"type"`
		}{}
		err := json.Unmarshal(xTrackData, &trackData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if(trackData.Type < 0) || (trackData.Type > 1) {
			return errors.New("Invalid image track type"), utils.ErrGeneric, "warn", nil
		} else if len(trackData.Ids) < 1 {
			return errors.New("No images uploaded"), "You need to add at least one image to the content.", "warn", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				stmt, vars, err := sqlx.In("SELECT `url` FROM `image_track` WHERE `i_id` IN(?)", trackData.Ids)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					rows, err := tx.Queryx(stmt, vars...)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {

						var urls []string
						var url string
						for rows.Next() {
							err := rows.Scan(&url)
							if err != nil {
								rows.Close()
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								urls = append(urls, url)
							}
						}
						tx.Commit()
						return nil, "Successfully fetched urls", "info", urls
					}
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetImageUrlFromTrackIds"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetImageUrlFromTrackIds"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func CRON_deleteUnlinkedImages() {
	apiStart := time.Now()

	defer func() {fmt.Println("\nCRON_deleteUnlinkedImages api execution time-> ", time.Since(apiStart))} ()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_deleteUnlinkedImages"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level:"info", Msg:"CRON_deleteUnlinkedImages started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"error", Msg:"Unable to begin transaction in CRON_deleteUnlinkedImages", Error:err.Error()})
		return
	} else {
		rows, err := tx.Queryx("SELECT `i_id`, `url` FROM `image_track` WHERE `a_id` IS NULL AND `created`<?", time.Now().UTC().Add(-2*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in query in CRON_deleteUnlinkedImages", Error:err.Error()})
		} else {
			var idList []int
			var objectList []*s3.ObjectIdentifier
			var id int
			var url string
			for rows.Next() {
				err := rows.Scan(&id, &url)
				if err != nil {
					rows.Close()
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in rows.scan in CRON_deleteUnlinkedImages", Error:err.Error()})
					return
				} else {
					if id > 0 {
						idList = append(idList, id)
					}
					if url != "" {
						elements := strings.Split(url, "/")
						if len(elements) == 5 && elements[4] != "" {
							objectList = append(objectList, &s3.ObjectIdentifier{Key:aws.String(elements[4])})
						}
					}
				}
			}
			if len(objectList) > 0 {
				input := &s3.DeleteObjectsInput{
					Bucket: aws.String(os.Getenv("S3_BUCKET_DP")),
					Delete: &s3.Delete{
						Objects:objectList,
						Quiet: aws.Bool(false),
					},
				}
				result, err := s3.New(S3Session).DeleteObjects(input)
				if err != nil {
					utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in DeleteObjects in CRON_deleteUnlinkedImages", Error:err.Error()})
					tx.Rollback()
					return
				} else {
					stmt, vars, err := sqlx.In("DELETE FROM `image_track` WHERE `i_id` IN(?)", idList)
					if err != nil {
						tx.Rollback()
						utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in sqlx.in in CRON_deleteUnlinkedImages", Error:err.Error()})
						return
					} else {
						_, err := tx.Exec(stmt, vars...)
						if err != nil {
							tx.Rollback()
							utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in exec in CRON_deleteUnlinkedImages", Error:err.Error()})
							return
						}
					}
					tx.Commit()
					utils.Logger(utils.MsgPrint{Level:"info", Msg:"Objects deleted successfully in DeleteObjects in CRON_deleteUnlinkedImages", Data:result})
				}
			} else {
				tx.Commit()
				utils.Logger(utils.MsgPrint{Level:"info", Msg:"nothing to delete in CRON_deleteUnlinkedImages"})
			}
		}
	}
}
package models

import (
	"encoding/json"
	"time"
	"utils"
)

/*Background theme model*/

type BackgroundThemeModel struct {
	BgId    int       `db:"bg_id" json:"bg_id"`
	BgUrl   string    `db:"bg_url" json:"bg_url"`
	Created time.Time `db:"created" json:"created"`
	Updated time.Time `db:"updated" json:"updated"`
}

func GetAllBackgroundUrls() string {
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllBackgroundUrls"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		tx, err := DB.Beginx()
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			resultX := []struct {
				BGId  int    `json:"bg_id" db:"bg_id"`
				BGUrl string `json:"bg_url" db:"bg_url"`
			}{}
			rows, err := tx.Queryx("SELECT `bg_id`, `bg_url` FROM `background_theme`")
			if err != nil {
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			} else {
				var id int
				var url string
				for rows.Next() {
					err := rows.Scan(&id, &url)
					resultX = append(resultX, struct {
						BGId  int    `json:"bg_id" db:"bg_id"`
						BGUrl string `json:"bg_url" db:"bg_url"`
					}{
						BGId: id, BGUrl: url,
					})
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					}
				}
				tx.Commit()
				return nil, "Successfully fetched all backgrounds", "info", resultX
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllBackgroundUrls"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllBackgroundUrls"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

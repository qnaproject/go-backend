package models

/*Category model*/

type CategoryModel struct {
	CId  int    `db:"c_id" json:"c_id"`
	Name string `db:"name" json:"name"`
}

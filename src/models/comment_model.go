package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"gopkg.in/guregu/null.v3"
	"time"
	"utils"
)

type CommentModel struct {
	CId     int       `json:"c_id" db:"c_id"`
	AId     int       `json:"a_id" db:"a_id"`
	UId     null.Int  `json:"u_id" db:"u_id"`
	Comment string    `json:"comment" db:"comment"`
	Created time.Time `json:"created" db:"created"`
	Updated time.Time `json:"updated" db:"updated"`
}

func AddComment(xCommentData []byte, sessionId string) string {
	/*
		a_id int
		comment string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddComment api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddComment"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		commentData := struct {
			AId     int    `json:"a_id" db:"a_id"`
			UId     int    `json:"u_id" db:"u_id"`
			Comment string `json:"comment" db:"comment"`
		}{}
		err := json.Unmarshal(xCommentData, &commentData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if commentData.AId < 1 {
			return errors.New("Invalid Answer id"), utils.ErrGeneric, "warn", nil
		} else if commentData.Comment == "" {
			return errors.New(utils.ErrCannotPostEmptyComment), utils.ErrCannotPostEmptyComment, "warn", nil
		} else if len(commentData.Comment) < 5 {
			return errors.New("Comment too short"), utils.ErrCommentTooShort, "warn", nil
		} else if len(commentData.Comment) > 100 {
			return errors.New("Comment too long"), utils.ErrCommentTooLong, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/uid"), utils.ErrGeneric, "error", nil
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_aid int
				var t_uid null.Int
				var t_qid int
				err := tx.QueryRowx("SELECT `a_id`, `u_id`, `q_id` FROM `answer` WHERE `a_id`=?", commentData.AId).Scan(&t_aid, &t_uid, &t_qid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrAnswerDoesntExist), utils.ErrAnswerDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					commentData.UId = uid
					bluePolicy := bluemonday.StrictPolicy()
					commentData.Comment = bluePolicy.Sanitize(commentData.Comment)
					res, err := tx.NamedExec("INSERT INTO `comment` (`a_id`, `u_id`, `comment`) VALUES (:a_id, :u_id, :comment)", commentData)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						cId, _ := res.LastInsertId()
						utils.RDB.HDel(fmt.Sprint(t_qid/1000), fmt.Sprint(t_qid%1000))
						tx.Commit()
						if t_uid.Valid && t_uid.Int64 > 0 && (int(t_uid.Int64) != commentData.UId) {
							var aidNullSafe null.Int
							aidNullSafe.Valid = true
							aidNullSafe.Int64 = int64(t_aid)
							go func(){
								var dots, commentPreview string
								if len(commentData.Comment) > 40 {
									commentPreview = commentData.Comment[:40]
									dots= "..."
								} else {
									dots = ""
									commentPreview = commentData.Comment
								}
								notifyUser(NotificationData{
									UId:              int(t_uid.Int64),
									NotificationType: 3,
									OriginUId:        uid,
									QACId:            t_qid,
									AId:              aidNullSafe,
									Preview: 		  commentPreview+dots,
								})
							}()
						}
						return nil, utils.SuccessPostedComment, "info", cId
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteComment(xCommentData []byte, sessionId string) string {
	/*
		c_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteComment api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteComment"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		commentData := struct {
			CId int `json:"c_id"`
		}{}

		err := json.Unmarshal(xCommentData, &commentData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if commentData.CId < 1 {
			return errors.New("Invalid commentid"), utils.ErrGeneric, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/userid"), utils.ErrGeneric, "error", nil
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if role == 1 || role == 2 {
				var t_uid int
				var t_nComment string
				err := tx.QueryRowx("SELECT `u_id`, `comment` FROM `comment` WHERE `c_id`=?", commentData.CId).Scan(&t_uid, &t_nComment)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrCommentDoesntExist), utils.ErrCommentDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					if archiveComment(tx, commentData.CId) {
						var t_qid int
						err := tx.QueryRowx("SELECT a.`q_id` FROM `answer` a, `comment` c WHERE c.`a_id`=a.`a_id` AND c.`c_id`=?", commentData.CId).Scan(&t_qid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						utils.RDB.HDel(fmt.Sprint(t_qid/1000), fmt.Sprint(t_qid%1000))
						_, err = tx.Exec("DELETE FROM `comment` WHERE `c_id`=?", commentData.CId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else if uid == t_uid {
							tx.Commit()
							return nil, utils.SuccessDeletedComment, "info", nil
						} else {
							if addUserPoints(tx, t_uid, utils.PointModDeleteComment) {
								tx.Commit()
								go func(){
									var dots, commentPreview string
									if len(t_nComment) > 40 {
										commentPreview = t_nComment[:40]
										dots= "..."
									} else {
										dots = ""
										commentPreview = t_nComment
									}
									notifyUser(NotificationData{
										UId:              t_uid,
										NotificationType: 10,
										Preview:commentPreview+dots,
									})
								}()
								return nil, utils.SuccessDeletedComment, "info", nil
							} else {
								tx.Rollback()
								return errors.New("Could not deduct points from original user"), utils.ErrGeneric, "error", nil
							}
						}
					} else {
						tx.Rollback()
						return errors.New("Could not archive comment"), utils.ErrGeneric, "error", nil
					}
				}
			} else {
				var t_uid int
				var created time.Time
				err := tx.QueryRowx("SELECT `u_id`, `created` FROM `comment` WHERE `c_id`=?", commentData.CId).Scan(&t_uid, &created)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrCommentDoesntExist), utils.ErrCommentDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if t_uid != uid {
					tx.Rollback()
					return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", nil
				} else if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
					tx.Rollback()
					return errors.New(utils.ErrCannotDeleteCommentAfterGrace), utils.ErrCannotDeleteCommentAfterGrace, "warn", nil
				} else {
					if archiveComment(tx, commentData.CId) {
						var t_qid int
						err := tx.QueryRowx("SELECT a.`q_id` FROM `answer` a, `comment` c WHERE c.`a_id`=a.`a_id` AND c.`c_id`=?", commentData.CId).Scan(&t_qid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						utils.RDB.HDel(fmt.Sprint(t_qid/1000), fmt.Sprint(t_qid%1000))
						_, err = tx.Exec("DELETE FROM `comment` WHERE `c_id`=?", commentData.CId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessDeletedComment, "info", nil
						}
					} else {
						tx.Rollback()
						return errors.New("Could not archive comment"), utils.ErrGeneric, "error", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "DeleteComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "DeleteComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

type dbComment struct {
	CId     int    `json:"c_id" db:"c_id"`
	Comment string `json:"comment" db:"comment"`
}

func getCommentsFromIdsForReport(xCIds []int) []dbComment {
	/*
		CIds []int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetCommentsFromIds api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetCommentsFromIds"})
		}
	}()

	checkData := func() (error, string, string, []dbComment) {
		commentData := struct {
			CIds []int `json:"c_ids" db:"c_ids"`
		}{}
		commentData.CIds = xCIds
		if len(commentData.CIds) < 1 {
			return nil, "Successfully fetched comments", "info", []dbComment{}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				inStmt, inVars, err := sqlx.In("SELECT `c_id`, `comment` FROM `comment` WHERE `c_id` IN(?)", commentData.CIds)
				inStmt = tx.Rebind(inStmt)
				rows, err := tx.Queryx(inStmt, inVars...)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					xDbCommentData := []dbComment{}
					dbCommentData := dbComment{}
					dbCommentIds := []int{}
					for rows.Next() {
						err := rows.StructScan(&dbCommentData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						xDbCommentData = append(xDbCommentData, dbCommentData)
						dbCommentIds = append(dbCommentIds, dbCommentData.CId)
					}
					//delete non-existent comments from report db
					cidsToClose := func(a, b []int) (res []int) {
						m := make(map[int]bool)
						for _, v := range b {
							m[v] = true
						}

						for _, v := range a {
							if _, ok := m[v]; !ok {
								res = append(res, v)
							}
						}
						return res
					}(commentData.CIds, dbCommentIds)
					if len(cidsToClose) > 0 {
						inStmt, inVars, err := sqlx.In("UPDATE `report` SET `closed`=1 WHERE `r_type`=3 AND `qac_id` IN(?)", cidsToClose)
						inStmt = tx.Rebind(inStmt)
						_, err = tx.Exec(inStmt, inVars...)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
					}
					tx.Commit()
					return nil, "", "info", xDbCommentData

				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getCommentsFromIdsForReport"})
		return []dbComment{}
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getCommentsFromIdsForReport"})
		return data
	}
}

func archiveComment(tx *sqlx.Tx, c_id int) bool {
	if c_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_comment` SELECT c.*, UTC_TIMESTAMP() FROM `comment` `c` WHERE `c_id`=?", c_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveComment"})
		return false
	}
	return true
}

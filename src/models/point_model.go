package models

import "time"

/*Point model*/

type PointModel struct {
	UId     int       `db:"u_id" json:"u_id"`
	AId     int       `db:"a_id" json:"a_id"`
	PType   int       `db:"p_type" json:"p_type"`
	Created time.Time `db:"created" json:"created"`
	Updated time.Time `db:"updated" json:"updated"`
}

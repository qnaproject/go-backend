package models

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/guregu/null.v3"
	"gopkg.in/redis.v5"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
	"utils"
)

type ArticleModel struct {
	AId      int       `db:"a_id" json:"a_id"`
	UId      null.Int  `db:"u_id" json:"u_id"`
	Title    string    `db:"title" json:"title"`
	Content  string    `db:"content" json:"content"`
	Category int       `json:"category" db:"category"`
	Keywords null.String    `db:"keywords" json:"keywords"`
	Points null.Int `json:"points" db:"points"`
	PreviewUrl null.String `json:"preview_url" db:"preview_url"`
	Created  time.Time `db:"created" json:"created"`
	Updated  time.Time `db:"updated" json:"updated"`
}

func AddArticle(xArticleData []byte, sessionId string) string {
	/*
		//anon bool
		title string
		category string
		content string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddArticle api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddArticle"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		aData := struct {
			Title    string `json:"title"`
			Category string `json:"category"`
			Content  string `json:"content"`
			TrackIds []int `json:"track_ids"`
			PreviewId int `json:"preview_id"`
		}{}
		aStoreData := ArticleModel{}
		err := json.Unmarshal(xArticleData, &aData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			/*UId validation*/
			email, ok := getLoggedInUserEmail(sessionId)
			if !ok {
				return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
			} else {
				UId, role, ok := getUserIdAndRole(email)
				if !ok {
					if role == -1 {
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
					} else if role == -2 {
						return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", email
					} else {
						return errors.New("Invalid role or userid"), utils.ErrGeneric, "warn", email
					}
				} else {

					aStoreData.UId.Valid = true
					aStoreData.UId.Int64 = int64(UId)
					/*Use this to enable question anonymity*/
					//if !qData.Anon {
					//	qStoreData.UId.Valid = true
					//	qStoreData.UId.Int64 = int64(UId)
					//} else {
					//	qStoreData.UId.Valid = false
					//}
					/*Title validation*/

					aData.Title = strings.TrimSpace(aData.Title)
					if len(aData.Title) < 10 {
						return errors.New("The title is too short"), "The title is too short", "warn", aData.Title
					} else if len(aData.Title) > 100 {
						return errors.New("The title is long"), "The title is too long", "warn", aData.Title
					} else {
						aData.Title = strings.ToLower(aData.Title)
						match, err := regexp.MatchString(`^[a-z0-9\\?\\.\\'\\-\\, ]{10,100}$`, aData.Title)
						if err != nil {
							return err, utils.ErrGeneric, "error", nil
						} else if !match {
							return errors.New("Invalid characters in title"), "Invalid characters in title.", "warn", nil
						} else {
							aData.Title = func() string {
								for i, v := range aData.Title {
									return string(unicode.ToUpper(v)) + aData.Title[i+1:]
								}
								return ""
							}()
							/*Description validation*/
							if aData.Content == "" {
								return errors.New("Please provide article content."), "Please provide article content", "warn", nil
							}
							if len(aData.Content) < 20 {
								return errors.New("Content too short"), "Content too short", "warn", nil
							} else if len(aData.Content) > 5000 {
								return errors.New("Content too long"), "Content too long", "warn", nil
							} else {
								bluePolicy := bluemonday.UGCPolicy()
								bluePolicy.AddTargetBlankToFullyQualifiedLinks(true)
								bluePolicy.AllowAttrs("class").Matching(bluemonday.Paragraph).OnElements("pre")
								bluePolicy.AllowAttrs("spellcheck").Matching(bluemonday.Paragraph).OnElements("pre")
								bluePolicy.AllowAttrs("class").Matching(bluemonday.Paragraph).OnElements("span")
								bluePolicy.AllowElements("u")
								aData.Content = bluePolicy.Sanitize(aData.Content)

								/*Category validation*/
								if _, ok := CategoryList[aData.Category]; !ok {
									return errors.New(utils.ErrInvalidCategory), utils.ErrInvalidCategory, "warn", aData.Category
								} else {
									aStoreData.Category = CategoryList[aData.Category]
									aStoreData.Title = aData.Title
									aStoreData.Content = aData.Content

									tx, err := DB.Beginx()
									if err != nil {
										return err, utils.ErrGeneric, "error", nil
									} else {
										keywords := utils.GetKeywordsFromTitle(aData.Title)
										if keywords != "" {
											aStoreData.Keywords.Valid = true
											aStoreData.Keywords.String = keywords
										}
										if aData.PreviewId > 0 {
											var t_previewImageUrl string
											err := tx.QueryRowx("SELECT `url` FROM `image_track` WHERE `i_id`=?", aData.PreviewId).Scan(&t_previewImageUrl)
											if err != nil {
												if err != sql.ErrNoRows {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
											} else {
												if t_previewImageUrl != "" {
													aStoreData.PreviewUrl.String, aStoreData.PreviewUrl.Valid = t_previewImageUrl, true
												}
											}
										}
										row, err := tx.NamedExec("INSERT INTO `article` (`u_id`, `title`, `content`, `category`, `keywords`, `preview_url`) VALUES (:u_id, :title, :content, :category, :keywords, :preview_url)", &aStoreData)
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											aId, err := row.LastInsertId()
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												if len(aData.TrackIds) > 0 {
													var trackIdList []int
													for _, v := range aData.TrackIds {
														trackIdList = append(trackIdList, v)
													}
													stmt, vars, err := sqlx.In("UPDATE `image_track` SET `a_id`=? WHERE `i_id` IN(?) AND `type`=1 AND `a_id` IS NULL", aId, trackIdList)
													if err != nil {
														tx.Rollback()
														return err, utils.ErrGeneric, "error", nil
													} else {
														_,err := tx.Exec(stmt, vars...)
														if err != nil {
															tx.Rollback()
															return err, utils.ErrGeneric, "error", nil
														}
													}
												}

												_, err := tx.Exec("UPDATE `user` SET `experience`=`experience`+1 WHERE `u_id`=?", aStoreData.UId)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
												tx.Commit()
												return nil, "Successfully posted article", "info", aId
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteArticle(xArticleData []byte, sessionId string) string {
	/*
		a_id
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteArticle api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteArticle"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		aData := struct {
			AId int64 `json:"a_id"`
		}{}
		err := json.Unmarshal(xArticleData, &aData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			/*Check logged in*/
			email, ok := getLoggedInUserEmail(sessionId)
			if !ok {
				return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
			} else {
				if aData.AId < 1 {
					return errors.New("Invalid AId"), utils.ErrGeneric, "warn", nil
				} else {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					}
					uid, role, ok := getUserIdAndRole(email)
					if !ok {
						tx.Rollback()
						if role == -1 {
							return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
						} else if role == -2 {
							return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", email
						} else {
							return errors.New("Invalid role or userid"), utils.ErrGeneric, "error", email
						}
					} else if role == 1 || role == 2 {
						/*MOD or ADMIN can delete any question*/
						/*TODO: send email to user notifying of deletion*/
						var originalUId null.Int
						var t_nArticle string
						err := tx.QueryRowx("SELECT `u_id`, `title` FROM `article` WHERE `a_id`=?", aData.AId).Scan(&originalUId, &t_nArticle)
						if err != nil {
							tx.Rollback()
							if err == sql.ErrNoRows {
								return errors.New("Article Doesn't exist"), "This article doesn't exist", "warn", nil
							} else {
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							if archiveArticle(tx, int(aData.AId)) {
								res, err := tx.Exec("DELETE FROM `article` WHERE `a_id`=?", aData.AId)
								utils.RDB.HDel("a"+fmt.Sprint(aData.AId/1000), fmt.Sprint(aData.AId%1000))
								if err != nil {
									tx.Rollback()
									if err == sql.ErrNoRows {
										return errors.New("Article Doesn't exist"), "This article doesn't exist.", "warn", nil
									} else {
										return err, utils.ErrGeneric, "error", nil
									}
								} else if t_num, _ := res.RowsAffected(); t_num < 1 {
									tx.Rollback()
									return errors.New("This article doesn't exist."), "This article doesn't exist.", "warn", nil
								} else {
									_, err := tx.Exec("UPDATE `image_track` SET `a_id`=NULL WHERE `a_id`=? AND `type`=?", aData.AId, 1)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									}
									/*deduct 100 points from user since mod/admin deleted post for violation*/
									if uid == int(originalUId.Int64) {
										tx.Commit()
										return nil, "Successfully deleted article", "info", nil
									}
									if originalUId.Valid && originalUId.Int64 > 0 {
										if addUserPoints(tx, int(originalUId.Int64), utils.PointModDeleteQuestion) {
											_, err := tx.Exec("UPDATE `user` SET `deprication`=`deprication`+1 WHERE `u_id`=?", originalUId.Int64)
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											}
											if checkForSuspension(tx, int(originalUId.Int64)) {
												tx.Commit()
												go func(){
													notifyUser(NotificationData{
														UId:              int(originalUId.Int64),
														NotificationType: 11,
														Preview:t_nArticle,
													})
												}()
												return nil, "Successfully deleted article", "info", nil
											} else {
												tx.Rollback()
												return errors.New("Could not check for suspension possibility"), utils.ErrGeneric, "error", nil
											}
										} else {
											tx.Rollback()
											return errors.New("Could not deduct original user points on mod deleting article"), utils.ErrGeneric, "error", nil
										}
									} else {
										tx.Commit()
										return nil, "Successfully deleted article", "info", nil
									}
								}
							} else {
								tx.Rollback()
								return errors.New("Could not archive article"), utils.ErrGeneric, "error", nil
							}
						}
					} else {
						var originalUId null.Int
						var created time.Time
						err := tx.QueryRowx("SELECT `u_id`, `created` FROM `article` WHERE `a_id`=?", aData.AId).Scan(&originalUId, &created)
						if err != nil {
							tx.Rollback()
							if err == sql.ErrNoRows {
								return errors.New("This article doesn't exist"), "This article doesn't exist", "warn", nil
							} else {
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							if !originalUId.Valid || originalUId.Int64 < 1 {
								tx.Rollback()
								return errors.New("You are not authorized to delete this article"), "You are not authorized to delete this article", "warn", nil
							} else if originalUId.Int64 != int64(uid) {
								tx.Rollback()
								return errors.New("You are not authorized to delete this article"), "You are not authorized to delete this article", "warn", nil
							} else {
								if archiveArticle(tx, int(aData.AId)) {
									_, err := tx.Exec("DELETE FROM `article` WHERE `a_id`=?", aData.AId)
									utils.RDB.HDel("a"+fmt.Sprint(aData.AId/1000), fmt.Sprint(aData.AId%1000))
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										_, err := tx.Exec("UPDATE `image_track` SET `a_id`=NULL WHERE `a_id`=? AND `type`=?", aData.AId, 1)
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										}
										tx.Commit()
										return nil, "Successfully deleted article", "info", nil
									}
								} else {
									tx.Rollback()
									return errors.New("Could not archive question"), utils.ErrGeneric, "error", nil
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "DeleteArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "DeleteArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetArticle(xArticleData []byte) string {
	/*
		a_id
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetArticle api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetArticle"})
		}
	}()

	aData := struct {
		AId int `json:"a_id"`
	}{}
	var aidForRedis int
	err := json.Unmarshal(xArticleData, &aData)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = utils.ErrGeneric
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		if aData.AId < 1 {
			utils.Logger(utils.MsgPrint{Error: errors.New("Invalid article id").Error(), Msg: utils.ErrGeneric, Level: "warn", Api: "GetArticle"})
			responseData := utils.ResponseBodyJSON{}
			responseData.Msg = utils.ErrGeneric
			responseData.Success = false
			xResponseJson, _ := json.Marshal(responseData)
			return string(xResponseJson)
		} else {
			aidForRedis = aData.AId
			rPost, err := utils.RDB.HGet("a"+fmt.Sprint(aData.AId/1000), fmt.Sprint(aData.AId%1000)).Result()
			if err != nil || len(rPost) == 0 {
				if err != redis.Nil && len(rPost) > 0 {
					utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetArticle"})
					responseData := utils.ResponseBodyJSON{}
					responseData.Msg = utils.ErrGeneric
					responseData.Success = false
					xResponseJson, _ := json.Marshal(responseData)
					return string(xResponseJson)
				}
			} else {
				//decompress
				postDataFromRedis, err := utils.GzipDecompress(rPost)
				if err != nil {
					utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetArticle"})
					responseData := utils.ResponseBodyJSON{}
					responseData.Msg = utils.ErrGeneric
					responseData.Success = false
					xResponseJson, _ := json.Marshal(responseData)
					return string(xResponseJson)
				}
				fmt.Println("REDIS")
				return postDataFromRedis
			}

			checkData := func() (error, string, string, interface{}) {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					type aComments struct {
						CId     int         `json:"c_id" db:"c_id"`
						Comment string      `json:"comment" db:"comment"`
						Name    null.String `json:"name" db:"comment"`
						UId     null.Int    `json:"u_id" db:"u_id"`
						Created time.Time   `json:"created" db:"created"`
					}
					aResult := struct {
						AId      int         `json:"a_id" db:"a_id"`
						UId      null.Int    `json:"u_id" db:"u_id"`
						Name     null.String `json:"name" db:"name"`
						Title    string      `json:"title" db:"title"`
						Content  string      `json:"content" db:"content"`
						Points   int         `json:"points" db:"points"`
						Category int         `json:"category" db:"category"`
						Created  time.Time   `json:"created" db:"created"`
						Comments []aComments `json:"comments" db:"comments"`
					}{}
					err := tx.QueryRowx("SELECT `a_id`, `u_id`, `title`, `content`, `points`, `category`, `created` FROM `article` WHERE `a_id`=?", aData.AId).StructScan(&aResult)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New("Article doesn't exist"), "This article doesn't exist", "warn", nil
						} else {
							return err, utils.ErrGeneric, "error", nil
						}
					} else {
						rows, err := tx.Queryx("SELECT `c_id`, `comment`, `u_id`, `created` FROM `article_comment` WHERE `a_id`=?", aData.AId)
						if (err != nil) && (err != sql.ErrNoRows) {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							dbComment := struct {
								CId     int       `db:"c_id"`
								Comment string    `db:"comment"`
								UId     null.Int  `db:"u_id"`
								Created time.Time `db:"created"`
							}{}
							aComment := aComments{}
							xComments := []aComments{}
							uidNameMap := map[int]string{}
							xUids := []int{}
							for rows.Next() {
								err := rows.StructScan(&dbComment)
								if err != nil {
									rows.Close()
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									if dbComment.UId.Valid && dbComment.UId.Int64 > 0 {
										xUids = append(xUids, int(dbComment.UId.Int64))
									}
									aComment.UId = dbComment.UId
									aComment.CId = dbComment.CId
									aComment.Created = dbComment.Created
									aComment.Comment = dbComment.Comment
									xComments = append(xComments, aComment)
								}
							}
							if len(xUids) > 0 {
								stmt, vars, err := sqlx.In("SELECT `u_id`,`fname`,`lname` FROM `user` WHERE `u_id` IN(?)", xUids)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									rows, err := tx.Queryx(stmt, vars...)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										var t_uid int
										var t_fname string
										var t_lname null.String
										for rows.Next() {
											err := rows.Scan(&t_uid, &t_fname, &t_lname)
											if err != nil {
												rows.Close()
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												var t_fullname string
												t_fullname = t_fname
												if t_lname.Valid && (len(t_lname.String) > 0) {
													t_fullname += " " + t_lname.String
												}
												uidNameMap[t_uid] = t_fullname
											}
										}
										for i, val := range xComments {
											if name, ok := uidNameMap[int(val.UId.Int64)]; ok {
												xComments[i].Name.Valid, xComments[i].Name.String = true, name
											}
										}
										aResult.Comments = xComments
									}
								}
							}
						}

						var fname string
						var lname null.String
						if aResult.UId.Valid && aResult.UId.Int64 > 0 {
							err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", aResult.UId.Int64).Scan(&fname, &lname)
							if err != nil {
								if err == sql.ErrNoRows {
									aResult.UId.Valid = false
									aResult.UId.Int64 = 0
									aResult.Name.Valid = false
									tx.Commit()
									return nil, "Successfully fetched article", "info", aResult
								} else {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
							}
							aResult.Name.Valid = true
							aResult.Name.String = fname
							if lname.Valid && lname.String != "" && len(lname.String) > 0 {
								aResult.Name.String += " " + lname.String
							}
							tx.Commit()
							return nil, "Successfully fetched article", "info", aResult
						} else {
							tx.Commit()
							aResult.UId.Valid = false
							aResult.UId.Int64 = 0
							aResult.Name.Valid = false
							return nil, "successfully fetched article", "info", aResult
						}
					}
				}
			}

			err, msg, level, data := checkData()
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getArticle"})
				responseData := utils.ResponseBodyJSON{}
				responseData.Msg = msg
				responseData.Success = false
				xResponseJson, _ := json.Marshal(responseData)
				return string(xResponseJson)
			} else {
				utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getArticle"})
				responseData := utils.ResponseBodyJSON{}
				responseData.Msg = msg
				responseData.Success = true
				responseData.Data = data
				xResponseJson, _ := json.Marshal(responseData)
				compressed := utils.GzipCompress((xResponseJson))
				fmt.Println("MySQL")
				if len(compressed) < 2500 && aidForRedis > 0 {
					go func() {
						utils.RDB.HSet("a"+fmt.Sprint(aidForRedis/1000), fmt.Sprint(aidForRedis%1000), compressed)
					}()
				}
				return string(xResponseJson)
			}
		}
	}
}

func archiveArticle(tx *sqlx.Tx, a_id int) bool {
	if a_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_article` SELECT a.*, UTC_TIMESTAMP() FROM `article` `a` WHERE `a_id`=?", a_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveArticle"})
		return false
	}
	return true
}

func GetAllUserArticles(xUserData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllUserArticles api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllUserArticles"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			UId int `json:"u_id"`
		}{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if userData.UId < 1 {
			return errors.New("Invalid userid"), utils.ErrGeneric, "error", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `title`, `content`, `created`, `a_id` FROM `article` WHERE `u_id`=?", userData.UId)
				if rows.Err() != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "info", []int{}
				}
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return nil, "You have not posted any articles", "info", []int{}
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					type aDataStruct struct {
						Title   string    `json:"title" db:"title"`
						Created time.Time `json:"created" db:"created"`
						Content string    `json:"content" db:"content"`
						AId     int       `json:"a_id" db:"a_id"`
					}
					xAData := []aDataStruct{}
					for rows.Next() {
						t_adata := aDataStruct{}
						err := rows.StructScan(&t_adata)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							xAData = append(xAData, t_adata)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched articles.", "info", xAData
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllUserArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllUserArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetLatestArticles(xCategoryData []byte) string {
	/*
		category string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetLatestArticles api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetLatestArticles"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		reqData := struct {
			Category string `json:"category"`
		}{}
		err := json.Unmarshal(xCategoryData, &reqData)
		if err != nil {
			return err, utils.ErrGeneric, "error", ""
		} else if reqData.Category == "" || reqData.Category == "General" {
			reqData.Category = "general"
		} else if _, ok := CategoryList[reqData.Category]; !ok && (reqData.Category != "general") {
			return errors.New("Invalid category"), "Invalid category", "warn", reqData.Category
		}
		result, err := utils.RDB.HGet("latest:a"+strings.ToLower(reqData.Category), "page1").Result()
		if err != nil {
			if err == redis.Nil || len(result) < 1 {
				go CRON_setLatestArticles()
				return errors.New("empty latest articles"), "No articles.", "error", ""
			} else {
				return err, utils.ErrGeneric, "error", ""
			}
		}

		resultString := `{"success":"true", "msg":"Successfully fetched latest articles", "data":[`
		decompressedString, err := utils.GzipDecompress(result)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		}
		resultString = resultString + decompressedString
		resultString += "]}"
		return nil, "Successfully fetched latest articles", "info", resultString
	}
	err, msg, level, data1 := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetLatestArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetLatestArticles"})
		return data1.(string)
	}
}

func UpvoteArticle(xArticleData []byte, sessionId string) string {
	/*
		a_id int
		session_id string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nUpvoteArticle api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UpvoteArticle"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		aData := struct {
			AId int `json:"a_id"`
		}{}

		err := json.Unmarshal(xArticleData, &aData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if aData.AId < 1 {
			return errors.New("Invalid aid"), utils.ErrGeneric, "warn", nil
		} else {
			if email, ok := getLoggedInUserEmail(sessionId); !ok {
				return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
			} else if uid, role, ok := getUserIdAndRole(email); !ok {
				switch role {
				case -1:
					return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
				case -2:
					return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
				default:
					return errors.New("invalid userid or role"), utils.ErrGeneric, "warn", email
				}
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					var t_aid int
					var t_aUid null.Int
					var t_nArticle string
					err := tx.QueryRowx("SELECT `a_id`, `u_id`, `title` FROM `article` WHERE `a_id`=?", aData.AId).Scan(&t_aid, &t_aUid, &t_nArticle)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New("This article doesn't exist"), "This article doesn't exist.", "warn", nil
						} else {
							return err, utils.ErrGeneric, "error", nil
						}
					}
					if t_aUid.Valid && t_aUid.Int64 > 0 && (int(t_aUid.Int64) == uid) {
						tx.Rollback()
						return nil, "You cannot like your own article.", "warn", nil
					}
					var t_uid int
					err = tx.QueryRowx("SELECT `u_id` FROM `article_vote` WHERE `u_id`=? AND `a_id`=?", uid, aData.AId).Scan(&t_uid)
					if err != nil {
						if err == sql.ErrNoRows {
							//can vote
							_, err = tx.Exec("UPDATE `article` SET `points`=`points`+1 WHERE `a_id`=?", aData.AId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								_, err := tx.Exec("INSERT INTO `article_vote`(`a_id`, `u_id`) VALUES(?, ?)", aData.AId, uid)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									if t_aUid.Valid && t_aUid.Int64 > 0 {
										addUserPoints(tx, int(t_aUid.Int64), 1)
									}
									tx.Commit()
									utils.RDB.HDel("a"+fmt.Sprint(aData.AId/1000), fmt.Sprint(aData.AId%1000))
									if t_aUid.Valid && t_aUid.Int64 > 0 {
										go func(){
											notifyUser(NotificationData{
												UId:              int(t_aUid.Int64),
												OriginUId:        uid,
												QACId:            aData.AId,
												NotificationType: 12,
												Message:          "",
												Preview: t_nArticle,
											})
										}()
									}
									return nil, "Successfully upvoted article", "info", nil
								}
							}
						} else {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
					} else {
						tx.Commit()
						return nil, "You have already liked.", "warn", nil
					}
				}
			}

		}
	}

	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "UpvoteArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "UpvoteArticle"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetOtherArticles(xArticleData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetOtherArticles api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetOtherArticles"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		aData := struct {
			AId      int    `json:"a_id"`
			Category string `json:"category"`
		}{}

		err := json.Unmarshal(xArticleData, &aData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if aData.AId < 1 {
			return errors.New("Invalid AId"), utils.ErrGeneric, "warn", nil
		} else {
			var catId int
			if len(aData.Category) < 1 {
				return errors.New("Invalid category"), "Invalid Category", "warn", nil
			} else {
				if catIdtemp, ok := CategoryList[aData.Category]; !ok {
					return errors.New("Invalid category"), "Invalid Category", "warn", nil
				} else {
					catId = catIdtemp
				}
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			}

			var t_aTitle string
			err = tx.QueryRowx("SELECT `title` FROM `article` WHERE `a_id`=?", aData.AId).Scan(&t_aTitle)
			if err != nil {
				tx.Rollback()
				if err == sql.ErrNoRows {
					return errors.New("This article doesn't exist"), "This article doesn't exist", "warn", nil
				}
				return err, utils.ErrGeneric, "error", nil
			}
			otherArticles := []struct {
				AId      int    `json:"a_id"`
				Title    string `json:"title"`
				Points int    `json:"points"`
			}{}
			keywords := utils.GetKeywordsFromTitle(t_aTitle)
			fmt.Println(t_aTitle)
			var depletedAids []int
			if keywords != "" {
				fmt.Println(keywords)
				rows, err := tx.Queryx("SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND `a_id` <> ? AND MATCH(`keywords`) AGAINST(?) LIMIT 12", catId, aData.AId, keywords)
				if err != nil {
					if err != sql.ErrNoRows {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					var oAid, oPoints int
					var oTitle string
					for rows.Next() {
						err := rows.Scan(&oAid, &oTitle, &oPoints)
						depletedAids = append(depletedAids, oAid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							otherArticles = append(otherArticles, struct {
								AId      int    `json:"a_id"`
								Title    string `json:"title"`
								Points int    `json:"points"`
							}{
								AId:      oAid,
								Title:    oTitle,
								Points: oPoints,
							})
						}
					}
					fmt.Println(otherArticles)
				}
			}

			if len(otherArticles) < 12 {
				fmt.Println("YES")
				var limit1, limit2 int
				if len(otherArticles)%2 == 0 {
					limit1, limit2 = (12-len(otherArticles))/2, (12-len(otherArticles))/2
				} else {
					limit1, limit2 = (12-len(otherArticles))/2, (12-len(otherArticles)+1)/2
				}
				var stmt string
				var vars []interface{}
				if len(depletedAids) > 0 {
					stmt, vars, err = sqlx.In("(SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND `a_id` NOT IN (?) AND `a_id` < ? ORDER BY `a_id` DESC LIMIT ?) UNION (SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND `a_id` NOT IN (?) AND `a_id` > ? ORDER BY `a_id` LIMIT ?)", catId, depletedAids, aData.AId, limit1, catId, depletedAids, aData.AId, limit2)
				} else {
					stmt, vars, err = sqlx.In("(SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND `a_id` < ? ORDER BY `a_id` DESC LIMIT ?) UNION (SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND `a_id` > ? ORDER BY `a_id` LIMIT ?)", catId, aData.AId, limit1, catId, aData.AId, limit2)
				}

				rows, err := tx.Queryx(stmt, vars...)

			if err != nil {
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			} else {

				var oAid, oPoints int
				var oTitle string
				for rows.Next() {
					err := rows.Scan(&oAid, &oTitle, &oPoints)
					if err != nil {
						rows.Close()
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						otherArticles = append(otherArticles, struct {
							AId    int    `json:"a_id"`
							Title  string `json:"title"`
							Points int    `json:"points"`
						}{
							AId:    oAid,
							Title:  oTitle,
							Points: oPoints,
						})
					}
				}
			}
			}
			tx.Commit()
			fmt.Println(otherArticles)
			return nil, "Successfully fetched other articles", "info", otherArticles
		}

	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getOtherArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getOtherArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetOtherUserArticles(xUserData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetOtherUserArticles api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetOtherUserArticles"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		uData := struct {
			UId int `json:"u_id"`
		}{}

		err := json.Unmarshal(xUserData, &uData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if uData.UId < 1 {
			return errors.New("Invalid UId"), utils.ErrGeneric, "warn", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			}
			rows, err := tx.Queryx("SELECT `a_id`, `title`, `points` FROM `article` WHERE `u_id`=? ORDER BY RAND() LIMIT 8", uData.UId)
			if err != nil {
				tx.Rollback()
				return err, utils.ErrGeneric, "error", nil
			} else {
				otherArticles := []struct {
					AId    int    `json:"a_id"`
					Title  string `json:"title"`
					Points int    `json:"points"`
				}{}
				var oAid, oPoints int
				var oTitle string
				for rows.Next() {
					err := rows.Scan(&oAid, &oTitle, &oPoints)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						otherArticles = append(otherArticles, struct {
							AId    int    `json:"a_id"`
							Title  string `json:"title"`
							Points int    `json:"points"`
						}{
							AId:    oAid,
							Title:  oTitle,
							Points: oPoints,
						})
					}
				}
				tx.Commit()
				return nil, "Successfully fetched other user articles", "info", otherArticles
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getOtherUserArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getOtherUserArticles"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetOtherArticlesForQuestion(xArticleData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetOtherArticlesForQuestion api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetOtherArticlesForQuestion"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		aData := struct {
			QId int `json:"q_id"`
			Category string `json:"category"`
		}{}

		err := json.Unmarshal(xArticleData, &aData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			var catId int
			if len(aData.Category) < 1 {
				return errors.New("Invalid category"), "Invalid Category", "warn", nil
			} else {
				if catIdtemp, ok := CategoryList[aData.Category]; !ok {
					return errors.New("Invalid category"), "Invalid Category", "warn", nil
				} else {
					catId = catIdtemp
				}
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			}

			var t_qTitle string
			err = tx.QueryRowx("SELECT `title` FROM `question` WHERE `q_id`=?", aData.QId).Scan(&t_qTitle)
			if err != nil {
				tx.Rollback()
				if err == sql.ErrNoRows {
					return errors.New("This question doesn't exist"), "This question doesn't exist", "warn", nil
				}
				return err, utils.ErrGeneric, "error", nil
			}
			otherArticles := []struct {
				AId      int    `json:"a_id"`
				Title    string `json:"title"`
				Points int    `json:"points"`
			}{}
			keywords := utils.GetKeywordsFromTitle(t_qTitle)
			var depletedAids []int
			if keywords != "" {
				rows, err := tx.Queryx("SELECT `a_id`, `title`, `points` FROM `article` WHERE `category`=? AND MATCH(`keywords`) AGAINST(?) LIMIT 12", catId, keywords)
				if err != nil {
					if err != sql.ErrNoRows {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					var oAid, oPoints int
					var oTitle string
					for rows.Next() {
						err := rows.Scan(&oAid, &oTitle, &oPoints)
						depletedAids = append(depletedAids, oAid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							otherArticles = append(otherArticles, struct {
								AId      int    `json:"a_id"`
								Title    string `json:"title"`
								Points int    `json:"points"`
							}{
								AId:      oAid,
								Title:    oTitle,
								Points: oPoints,
							})
						}
					}
				}
			}

			if len(otherArticles) < 12 {
				var limit int
				if len(otherArticles)%2 == 0 {
					limit = (12-len(otherArticles))/2
				} else {
					limit = (12-len(otherArticles)+1)/2
				}
				var stmt string
				var vars []interface{}
				if len(depletedAids) > 0 {
					stmt, vars, err = sqlx.In("SELECT `a_id`, `title`, `points` FROM `article` WHERE `a_id` NOT IN(?) AND `a_id` > (FLOOR(RAND() * (SELECT `a_id` FROM `article` WHERE `category`=? ORDER BY `a_id` DESC LIMIT 1))) AND `category`=? LIMIT ?", depletedAids, catId, catId, limit)
				} else {
					stmt, vars, err = sqlx.In("SELECT `a_id`, `title`, `points` FROM `article` WHERE `a_id` > (FLOOR(RAND() * (SELECT `a_id` FROM `article` WHERE `category`=? ORDER BY `a_id` DESC LIMIT 1))) AND `category`=? LIMIT ?", catId, catId, limit)
				}

				rows, err := tx.Queryx(stmt, vars...)

				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {

					var oAid, oPoints int
					var oTitle string
					for rows.Next() {
						err := rows.Scan(&oAid, &oTitle, &oPoints)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							otherArticles = append(otherArticles, struct {
								AId    int    `json:"a_id"`
								Title  string `json:"title"`
								Points int    `json:"points"`
							}{
								AId:    oAid,
								Title:  oTitle,
								Points: oPoints,
							})
						}
					}
				}
			}
			tx.Commit()
			return nil, "Successfully fetched other articles", "info", otherArticles
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetOtherArticlesForQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetOtherArticlesForQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func AddArticleComment(xCommentData []byte, sessionId string) string {
	/*
		a_id int
		comment string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddArticleComment api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddArticleComment"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		commentData := struct {
			AId     int    `json:"a_id" db:"a_id"`
			UId     int    `json:"u_id" db:"u_id"`
			Comment string `json:"comment" db:"comment"`
		}{}
		err := json.Unmarshal(xCommentData, &commentData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if commentData.AId < 1 {
			return errors.New("Invalid article id"), utils.ErrGeneric, "warn", nil
		} else if commentData.Comment == "" {
			return errors.New(utils.ErrCannotPostEmptyComment), utils.ErrCannotPostEmptyComment, "warn", nil
		} else if len(commentData.Comment) < 5 {
			return errors.New("Comment too short"), utils.ErrCommentTooShort, "warn", nil
		} else if len(commentData.Comment) > 100 {
			return errors.New("Comment too long"), utils.ErrCommentTooLong, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/uid"), utils.ErrGeneric, "error", nil
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_aid int
				var t_uid null.Int
				err := tx.QueryRowx("SELECT `a_id`, `u_id` FROM `article` WHERE `a_id`=?", commentData.AId).Scan(&t_aid, &t_uid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("This article does not exist."), "This article does not exist.", "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					commentData.UId = uid
					bluePolicy := bluemonday.StrictPolicy()
					commentData.Comment = bluePolicy.Sanitize(commentData.Comment)
					res, err := tx.NamedExec("INSERT INTO `article_comment` (`a_id`, `u_id`, `comment`) VALUES (:a_id, :u_id, :comment)", commentData)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						cId, _ := res.LastInsertId()
						utils.RDB.HDel("a"+fmt.Sprint(t_aid/1000), fmt.Sprint(t_aid%1000))
						tx.Commit()
						if t_uid.Valid && t_uid.Int64 > 0 && (int(t_uid.Int64) != commentData.UId) {
							go func(){
								var dots, commentPreview string
								if len(commentData.Comment) > 40 {
									commentPreview = commentData.Comment[:40]
									dots= "..."
								} else {
									dots = ""
									commentPreview = commentData.Comment
								}
								notifyUser(NotificationData{
									UId:              int(t_uid.Int64),
									NotificationType: 13,
									OriginUId:        uid,
									QACId:            t_aid,
									Preview:commentPreview+dots,
								})
							}()
						}
						return nil, utils.SuccessPostedComment, "info", cId
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddArticleComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddArticleComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteArticleComment(xCommentData []byte, sessionId string) string {
	/*
		c_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteArticleComment api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteArticleComment"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		commentData := struct {
			CId int `json:"c_id"`
		}{}

		err := json.Unmarshal(xCommentData, &commentData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if commentData.CId < 1 {
			return errors.New("Invalid commentid"), utils.ErrGeneric, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/userid"), utils.ErrGeneric, "error", nil
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if role == 1 || role == 2 {
				var t_uid int
				err := tx.QueryRowx("SELECT `u_id` FROM `article_comment` WHERE `c_id`=?", commentData.CId).Scan(&t_uid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrCommentDoesntExist), utils.ErrCommentDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					if archiveComment(tx, commentData.CId) {
						var t_aid int
						err := tx.QueryRowx("SELECT `a_id` FROM `article_comment` WHERE `c_id`=?", commentData.CId).Scan(&t_aid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						utils.RDB.HDel("a"+fmt.Sprint(t_aid/1000), fmt.Sprint(t_aid%1000))
						_, err = tx.Exec("DELETE FROM `article_comment` WHERE `c_id`=?", commentData.CId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else if uid == t_uid {
							tx.Commit()
							return nil, utils.SuccessDeletedComment, "info", nil
						} else {
							if addUserPoints(tx, t_uid, utils.PointModDeleteComment) {
								tx.Commit()
								go notifyUser(NotificationData{
									UId:              t_uid,
									NotificationType: 10,
								})
								return nil, utils.SuccessDeletedComment, "info", nil
							} else {
								tx.Rollback()
								return errors.New("Could not deduct points from original user"), utils.ErrGeneric, "error", nil
							}
						}
					} else {
						tx.Rollback()
						return errors.New("Could not archive comment"), utils.ErrGeneric, "error", nil
					}
				}
			} else {
				var t_uid int
				var created time.Time
				err := tx.QueryRowx("SELECT `u_id`, `created` FROM `article_comment` WHERE `c_id`=?", commentData.CId).Scan(&t_uid, &created)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrCommentDoesntExist), utils.ErrCommentDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if t_uid != uid {
					tx.Rollback()
					return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", nil
				} else if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
					tx.Rollback()
					return errors.New(utils.ErrCannotDeleteCommentAfterGrace), utils.ErrCannotDeleteCommentAfterGrace, "warn", nil
				} else {
					if archiveArticleComment(tx, commentData.CId) {
						var t_aid int
						err := tx.QueryRowx("SELECT `a_id` FROM `article_comment` WHERE `c_id`=?", commentData.CId).Scan(&t_aid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						utils.RDB.HDel("a"+fmt.Sprint(t_aid/1000), fmt.Sprint(t_aid%1000))
						_, err = tx.Exec("DELETE FROM `article_comment` WHERE `c_id`=?", commentData.CId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessDeletedComment, "info", nil
						}
					} else {
						tx.Rollback()
						return errors.New("Could not archive comment"), utils.ErrGeneric, "error", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "DeleteArticleComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "DeleteArticleComment"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func UploadArticleImage(xImageData []byte, sessionId string) (response string) {
	start := time.Now()
	defer func() {
		fmt.Println("\nUploadArticleImage api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UploadArticleImage"})
		}
	}()
	var email string
	var ok bool
	if email, ok = getLoggedInUserEmail(sessionId); !ok {
		utils.Logger(utils.MsgPrint{Error: utils.ErrNotLoggedIn, Level: "warn", Api: "UploadArticleImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = utils.ErrNotLoggedIn
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if role, ok := hasRightToPost(email); !ok {
		switch role {
		case -1:
			utils.Logger(utils.MsgPrint{Error: utils.ErrSuspension, Level: "warn", Api: "UploadArticleImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrSuspension
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		case -2:
			utils.Logger(utils.MsgPrint{Error: utils.ErrTerminated, Level: "warn", Api: "UploadArticleImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = utils.ErrTerminated
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		default:
			utils.Logger(utils.MsgPrint{Error: ("invalid role/userid"), Level: "warn", Api: "UploadArticleImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = "Something went wrong while uploading image."
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		}
	}
	// fmt.Println(string(xImageData))
	//fp, err := os.OpenFile("test.jpg", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0777)
	//if err != nil {
	//	return ""
	//} else
	//dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(string(xImageData)))

	res, err := base64.StdEncoding.DecodeString(string(xImageData))
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadArticleImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	if (len(res) / 1024) > 300 {
		utils.Logger(utils.MsgPrint{Error: ("Image too large."), Level: "error", Api: "UploadArticleImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Image too large."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	}

	url, err := utils.AddFileToS3(res, 1, "")
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadArticleImage"})
		var responseBody utils.ResponseBodyJSON
		responseBody.Success = false
		responseBody.Msg = "Something went wrong while uploading image."
		responseBody.Data = nil
		xResponseJson, _ := json.Marshal(responseBody)
		return string(xResponseJson)
	} else {
		var id int64
		tx, err := DB.Beginx()
		if err != nil {
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadArticleImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = false
			responseBody.Msg = "Something went wrong while uploading image."
			responseBody.Data = nil
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		} else {
			if url != "" {
				res, err := tx.Exec("INSERT INTO `image_track`(`url`, `type`) VALUES(?,?)",url, 1)
				if err != nil {
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadArticleImage"})
					var responseBody utils.ResponseBodyJSON
					responseBody.Success = false
					responseBody.Msg = "Something went wrong while uploading image."
					responseBody.Data = nil
					xResponseJson, _ := json.Marshal(responseBody)
					return string(xResponseJson)
				} else {
					id, err = res.LastInsertId()
					if err != nil {
						tx.Rollback()
						utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "UploadArticleImage"})
						var responseBody utils.ResponseBodyJSON
						responseBody.Success = false
						responseBody.Msg = "Something went wrong while uploading image."
						responseBody.Data = nil
						xResponseJson, _ := json.Marshal(responseBody)
						return string(xResponseJson)
					} else {
					}
				}
			}
			tx.Commit()
			utils.Logger(utils.MsgPrint{Level: "info", Api: "UploadArticleImage"})
			var responseBody utils.ResponseBodyJSON
			responseBody.Success = true
			responseBody.Msg = "Successfully uploaded image."
			responseBody.Data = struct{
				URL string `json:"url"`
				TrackId int `json:"track_id"`
			}{
				URL: url,
				TrackId:int(id),
			}
			xResponseJson, _ := json.Marshal(responseBody)
			return string(xResponseJson)
		}
	}
}


func CRON_setLatestArticles() {
	apiStart := time.Now()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_setLatestArticles"})
		}
	}()
	defer func() { fmt.Println("\nCRON_setLatestArticles api execution time-> ", time.Since(apiStart)) }()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_setLatestArticles started"})
	tx, err := DB.Beginx()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestArticles"})
		return
	}
	type latestPost struct {
		AId      int       `json:"a_id" db:"a_id"`
		Title    string    `json:"title" db:"title"`
		Category string    `json:"category"`
		Created  time.Time `json:"created" db:"created"`
		PreviewUrl null.String `json:"preview_url" db:"preview_url"`
		FName null.String	`json:"fname" db:"fname"`
		LName null.String `json:"lname" db:"lname"`
	}
	//latestCategoryList := CategoryList
	latestCategoryList := map[string]int{}
	for key, val := range CategoryList {
		latestCategoryList[key] = val
	}
	latestCategoryList["General"] = -1

	for key, val := range latestCategoryList {
		latestPosts := []latestPost{}

		var query string
		if key == "General" {
			query = "SELECT a.`a_id`, a.`title`, a.`category`, a.`created`, a.`preview_url`, u.`fname`, u.`lname` FROM `article` a LEFT JOIN `user` u ON a.`u_id`=u.`u_id`  ORDER BY a.`created` DESC LIMIT 100"
		} else {
			query = "SELECT a.`a_id`, a.`title`, a.`category`, a.`created`, a.`preview_url`, u.`fname`, u.`lname` FROM `article` a LEFT JOIN `user` u ON a.`u_id`=u.`u_id` WHERE `category`=" + strconv.Itoa(val) + " ORDER BY a.`created` DESC LIMIT 100"
		}
		rows, err := tx.Queryx(query)
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestArticles"})
			return
		} else {
			t_latestPost := latestPost{}
			for rows.Next() {
				var aid, category int
				var title string
				var created time.Time
				var preview null.String
				var fname, lname null.String
				err := rows.Scan(&aid, &title, &category, &created, &preview, &fname, &lname)
				if err != nil {
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestArticles"})
					rows.Close()
					return
				} else {
					if _, ok := ReverseCategoryList[category]; !ok {
						tx.Rollback()
						utils.Logger(utils.MsgPrint{Error: errors.New("Invalid category").Error(), Level: "error", Api: "CRON_setLatestArticles", Data: "aid->" + strconv.Itoa(aid)})
						return
					}
					t_latestPost.Category = ReverseCategoryList[category]
					t_latestPost.Title = title
					t_latestPost.AId = aid
					t_latestPost.Created = created
					t_latestPost.PreviewUrl = preview
					t_latestPost.FName = fname
					t_latestPost.LName = lname
					latestPosts = append(latestPosts, t_latestPost)
				}
			}
		}
		{
			responseJson, err := json.Marshal(latestPosts)
			if err != nil {
				tx.Rollback()
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestArticles"})
				return
			}
			compressedString := utils.GzipCompress(responseJson)
			utils.RDB.HSet("latest:a"+strings.ToLower(key), "page1", compressedString)
		}
	}
	tx.Commit()
}

func archiveArticleComment(tx *sqlx.Tx, c_id int) bool {
	if c_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_article_comment` SELECT c.*, UTC_TIMESTAMP() FROM `article_comment` `c` WHERE `c_id`=?", c_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveArticleComment"})
		return false
	}
	return true
}

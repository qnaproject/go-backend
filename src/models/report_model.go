package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"strings"
	"time"
	"utils"
)

var reportCategory = map[string]int{
	"Hate Speech":                   1,
	"Political Propaganda":          2,
	"Harmful Intent":                3,
	"Sexually Explicit Content":     4,
	"Advertising/Spam":              5,
	"Misleading":                    6,
	"Content unrelated to question": 7,
	"Wrong Category":                8,
}

var reverseReportCategory = map[int]string{
	1: "Hate Speech",
	2: "Political Propaganda",
	3: "Harmful Intent",
	4: "Sexually Explicit Content",
	5: "Advertising/Spam",
	6: "Misleading",
	7: "Content unrelated to question",
	9: "Wrong Category",
}

type ReportModel struct {
	RId     int       `db:"r_id" json:"r_id"`
	QACId   int       `db:"qac_id" json:"qac_id"`
	Type    int       `db:"type" json:"type"`
	Created time.Time `db:"created" json:"created"`
	Updated time.Time `db:"updated" json:"updated"`
}

func ReportPost(xReportData []byte, sessionId string) string {
	/*
		qac_id int
		r_type int
		report_category string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nReportPost api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ReportPost"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		reportData := struct {
			QACid          int    `json:"qac_id"`
			Type           int    `json:"r_type"`
			ReportCategory string `json:"report_category"`
		}{}
		err := json.Unmarshal(xReportData, &reportData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if reportData.QACid < 1 {
			return errors.New("Invalid QACId"), utils.ErrGeneric, "warn", reportData
		} else if reportData.Type < 1 {
			return errors.New("Invalid Type"), utils.ErrGeneric, "warn", reportData
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if reportCategoryCode, ok := reportCategory[reportData.ReportCategory]; !ok {
			return errors.New(utils.ErrInvalidReportCategory), utils.ErrInvalidReportCategory, "warn", nil
		} else {
			tableName := ``
			columnName := ``
			switch reportData.Type {
			case 1:
				tableName = `question`
				columnName = `q_id`
			case 2:
				tableName = `answer`
				columnName = `a_id`
			case 3:
				tableName = `comment`
				columnName = `c_id`
			case 4:
				tableName = `article`
				columnName = `a_id`
			case 5:
				tableName = `article_comment`
				columnName = `c_id`
			default:
				return errors.New("Invalid Type"), utils.ErrGeneric, "warn", nil
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_qacid int
				err := tx.QueryRowx("SELECT "+columnName+" FROM "+tableName+" WHERE "+columnName+"=?", reportData.QACid).Scan(&t_qacid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(tableName + " doesn't exist"), strings.Title(tableName) + " doesn't exist", "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					if email, ok := getLoggedInUserEmail(sessionId); !ok {
						tx.Rollback()
						return errors.New("Could not get email from sessionId"), utils.ErrNotLoggedIn, "warn", nil
					} else if UId, ok := getUserIdFromEmail(email); !ok {
						tx.Rollback()
						return errors.New("Could not get userid from email"), utils.ErrGeneric, "warn", nil
					} else {
						reportDataToDb := struct {
							QACid          int `db:"qac_id"`
							RType          int `db:"r_type"`
							UId            int `db:"u_id"`
							ReportCategory int `db:"report_category"`
						}{
							QACid:          reportData.QACid,
							RType:          reportData.Type,
							ReportCategory: reportCategoryCode,
							UId:            UId,
						}
						_, err := tx.NamedExec("INSERT INTO `report`(`qac_id`, `r_type`, `u_id`, `report_category`) VALUES(:qac_id, :r_type, :u_id, :report_category)", &reportDataToDb)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessReported, "info", nil
						}
					}
				}

			}

		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "ReportPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "ReportPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

/*MOD/ADMIN only*/
func GetReportsForModeration(sessionId string) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetReportsForModeration api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetReportsForModeration"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if role, ok := hasRightToPost(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid Role"), utils.ErrGeneric, "error", email + " " + string(role)
			}
		} else if role != 2 {
			return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", email
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `qac_id`, `r_type`, `report_category`, `created` FROM `report`")
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					type qReportFormat struct {
						QId      int       `json:"q_id"`
						Category string    `json:"category"`
						Created  time.Time `json:"created"`
					}
					type aReportFormat struct {
						AId      int       `json:"q_id"`
						Category string    `json:"category"`
						Created  time.Time `json:"created"`
					}
					type cReportFormat struct {
						CId      int       `json:"q_id"`
						Category string    `json:"category"`
						Created  time.Time `json:"created"`
					}
					qReportIds := []int{}
					aReportIds := []int{}
					cReportIds := []int{}
					qReportData := []qReportFormat{}
					aReportData := []aReportFormat{}
					cReportData := []cReportFormat{}
					for rows.Next() {
						t_report := struct {
							QACId    int       `db:"qac_id"`
							Category int       `db:"report_category"`
							Created  time.Time `db:"created"`
							RType    int       `db:"r_type"`
						}{}
						err := rows.StructScan(&t_report)
						if err != nil {
							tx.Rollback()
							rows.Close()
							return err, utils.ErrGeneric, "error", nil
						} else {
							switch t_report.RType {
							case 1:
								qReportIds = append(qReportIds, t_report.QACId)
								qReportData = append(qReportData, qReportFormat{
									QId:      t_report.QACId,
									Category: reverseReportCategory[t_report.Category],
									Created:  t_report.Created,
								})
							case 2:
								aReportIds = append(aReportIds, t_report.QACId)
								aReportData = append(aReportData, aReportFormat{
									AId:      t_report.QACId,
									Category: reverseReportCategory[t_report.Category],
									Created:  t_report.Created,
								})
							case 3:
								cReportIds = append(cReportIds, t_report.QACId)
								cReportData = append(cReportData, cReportFormat{
									CId:      t_report.QACId,
									Category: reverseReportCategory[t_report.Category],
									Created:  t_report.Created,
								})
							default:
								break
							}
						}
					}

					qDetails := getQuestionsFromIdsForReport(qReportIds)
					//remove
					fmt.Println(qDetails)

					//question operation starts here
					qMap := map[int]struct {
						QId        int      `json:"q_id"`
						Title      string   `json:"title"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}

					for _, v := range qDetails {
						qMap[v.QId] = struct {
							QId        int      `json:"q_id"`
							Title      string   `json:"title"`
							Categories []string `json:"categories"`
							Count      int      `json:"count"`
						}{
							QId:   v.QId,
							Title: v.Title,
						}
					}

					for _, v := range qReportData {
						if _, ok := qMap[v.QId]; ok {
							t_qMap := qMap[v.QId]
							t_qMap.Count++
							t_qMap.Categories = append(t_qMap.Categories, v.Category)
							qMap[v.QId] = t_qMap
						}
					}
					var qSlice = []struct {
						QId        int      `json:"q_id"`
						Title      string   `json:"title"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}
					for _, v := range qMap {
						qSlice = append(qSlice, v)
					}
					//question operation ends here

					//answer operation starts here
					aDetails := getAnswersFromIdsForReport(aReportIds)
					aMap := map[int]struct {
						AId        int      `json:"a_id"`
						Answer     string   `json:"answer"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}

					for _, v := range aDetails {
						aMap[v.AId] = struct {
							AId        int      `json:"a_id"`
							Answer     string   `json:"answer"`
							Categories []string `json:"categories"`
							Count      int      `json:"count"`
						}{
							AId:    v.AId,
							Answer: v.Answer,
						}
					}

					for _, v := range aReportData {
						if _, ok := aMap[v.AId]; ok {
							t_aMap := aMap[v.AId]
							t_aMap.Count++
							t_aMap.Categories = append(t_aMap.Categories, v.Category)
							aMap[v.AId] = t_aMap
						}
					}
					var aSlice = []struct {
						AId        int      `json:"a_id"`
						Answer     string   `json:"answer"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}
					for _, v := range aMap {
						aSlice = append(aSlice, v)
					}
					//question operation ends here

					//comment operation starts here
					cDetails := getCommentsFromIdsForReport(cReportIds)
					cMap := map[int]struct {
						CId        int      `json:"c_id"`
						Comment    string   `json:"comment"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}

					for _, v := range cDetails {
						cMap[v.CId] = struct {
							CId        int      `json:"c_id"`
							Comment    string   `json:"comment"`
							Categories []string `json:"categories"`
							Count      int      `json:"count"`
						}{
							CId:     v.CId,
							Comment: v.Comment,
						}
					}

					for _, v := range cReportData {
						if _, ok := cMap[v.CId]; ok {
							t_cMap := cMap[v.CId]
							t_cMap.Count++
							t_cMap.Categories = append(t_cMap.Categories, v.Category)
							cMap[v.CId] = t_cMap
						}
					}
					var cSlice = []struct {
						CId        int      `json:"c_id"`
						Comment    string   `json:"comment"`
						Categories []string `json:"categories"`
						Count      int      `json:"count"`
					}{}
					for _, v := range cMap {
						cSlice = append(cSlice, v)
					}
					//comment operation ends here

					rFinal := map[string]interface{}{}
					rFinal["questions"] = qSlice
					rFinal["answers"] = aSlice
					rFinal["comments"] = cSlice
					tx.Commit()
					return nil, "", "", rFinal
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "ReportPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "ReportPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func CRON_deleteOldClosedReportEntries() {
	apiStart := time.Now()
	defer func() {fmt.Println("\nCRON_deleteOldClosedReportEntries api execution time-> ", time.Since(apiStart))} ()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_deleteOldClosedReportEntries"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level:"info", Msg:"CRON_deleteOldClosedReportEntries started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level:"error", Msg:"Unable to begin transaction in CRON_deleteOldClosedReportEntries", Error:err.Error()})
	} else {
		_, err := tx.Exec("DELETE from `report` WHERE `closed`=1 AND `created`<?", time.Now().UTC().Add(-15*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level:"error", Msg:"Error in exec in CRON_deleteOldClosedReportEntries", Error:err.Error()})
		} else {
			tx.Commit()
		}
	}
}

package models

import "time"

/*
Suspension model
*/

type SuspensionModel struct {
	SId      int       `db:"s_id" json:"s_id"`
	UId      int       `db:"u_id" json:"u_id"`
	Duration int       `db:"duration" json:"duration"`
	Created  time.Time `db:"created" json:"created"`
	Updated  time.Time `db:"updated" json:"updated"`
}

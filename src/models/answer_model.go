package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"gopkg.in/guregu/null.v3"
	"math"
	"regexp"
	"strconv"
	"time"
	"unicode"
	"utils"
)

/*Answer model*/

type AnswerModel struct {
	AId           int         `db:"a_id" json:"a_id"`
	QId           int         `db:"q_id" json:"q_id"`
	UId           null.Int    `db:"u_id" json:"u_id"`
	Answer        string      `db:"answer" json:"answer"`
	Qualification null.String `db:"qualification" json:"qualification"`
	UpVotes       int         `db:"up_votes" json:"up_votes"`
	DownVotes     int         `db:"down_votes" json:"down_votes"`
	Created       time.Time   `db:"created" json:"created"`
	Updated       time.Time   `db:"updated" json:"updated"`
}

func AddAnswer(xAnswerData []byte, sessionId string) string {
	/*
		q_id int
		qualification string
		answer string
	track_ids []int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddAnswer api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddAnswer"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		}
		answerData := struct {
			QId           int    `json:"q_id"`
			Answer        string `json:"answer"`
			Qualification string `json:"qualification"`
			TrackIds []int `json:"track_ids"`
		}{}
		err := json.Unmarshal(xAnswerData, &answerData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if answerData.QId < 1 {
			return errors.New("Invalid question id"), utils.ErrGeneric, "warn", nil
		} else if answerData.Answer == "" {
			return errors.New(utils.ErrEmptyAnswer), utils.ErrEmptyAnswer, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, points, ok := getUserIdRolePoints(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("invalid role/userid"), utils.ErrGeneric, "errors", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_closed bool
				var t_uid null.Int
				var t_qTitle string
				err := tx.QueryRowx("SELECT `u_id`, `closed`, `title` FROM `question` WHERE `q_id`=?", answerData.QId).Scan(&t_uid, &t_closed, &t_qTitle)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if t_closed {
					tx.Rollback()
					return errors.New(utils.ErrThisQuestionIsClosed), utils.ErrThisQuestionIsClosed, "warn", nil
				} else if t_uid.Valid && t_uid.Int64 == int64(uid) {
					tx.Rollback()
					return errors.New(utils.ErrCannotAnswerOwnQuestion), utils.ErrCannotAnswerOwnQuestion, "warn", nil
				} else {
					dbAnswerData := AnswerModel{}
					dbAnswerData.UId.Valid, dbAnswerData.UId.Int64 = true, int64(uid)
					dbAnswerData.QId = answerData.QId
					if answerData.Qualification != "" {
						match, err := regexp.MatchString(`^[a-zA-Z0-9\\. ]{1,100}$`, answerData.Qualification)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", answerData.Qualification
						} else if !match {
							tx.Rollback()
							return errors.New("Invalid Characters in qualification"), utils.ErrInvalidCharactersInQualification, "warn", nil
						} else {
							answerData.Qualification = func() string {
								for i, v := range answerData.Qualification {
									return string(unicode.ToUpper(v)) + answerData.Qualification[i+1:]
								}
								return ""
							}()
							dbAnswerData.Qualification.Valid, dbAnswerData.Qualification.String = true, answerData.Qualification
						}
					}
					bluePolicy := bluemonday.UGCPolicy()
					bluePolicy.AddTargetBlankToFullyQualifiedLinks(true)
					bluePolicy.AllowAttrs("class").Matching(bluemonday.Paragraph).OnElements("pre")
					bluePolicy.AllowAttrs("spellcheck").Matching(bluemonday.Paragraph).OnElements("pre")
					bluePolicy.AllowAttrs("class").Matching(bluemonday.Paragraph).OnElements("span")
					bluePolicy.AllowElements("u")
					answerData.Answer = bluePolicy.Sanitize(answerData.Answer)
					if len(answerData.Answer) < 10 {
						tx.Rollback()
						return errors.New(utils.ErrAnswerTooShort), utils.ErrAnswerTooShort, "warn", nil
					} else if len(answerData.Answer) > 5000 {
						tx.Rollback()
						return errors.New(utils.ErrAnswerTooLong), utils.ErrAnswerTooLong, "warn", nil
					} else {
						dbAnswerData.Answer = answerData.Answer
						res, err := tx.NamedExec("INSERT INTO `answer`(`q_id`, `u_id`, `qualification`, `answer`) VALUES(:q_id, :u_id, :qualification, :answer)", &dbAnswerData)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							/*POINTS*/
							activityIncrement := utils.PointAnswerMinimum * utils.GetPointsMultiplier(points)
							if t_uid.Valid && t_uid.Int64 > 0 {
								strActivityIncrement := strconv.Itoa(activityIncrement)
								query := "UPDATE user u,question q SET u.points=u.points+" + strActivityIncrement + ", q.activity=q.activity+" + strActivityIncrement + " WHERE u.u_id=q.u_id AND q.q_id=" + strconv.Itoa(dbAnswerData.QId)
								_, err := tx.Exec(query)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									_, err := tx.Exec("UPDATE `user` SET `experience`=`experience`+1 WHERE `u_id`=?", uid)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									}
									utils.RDB.HDel(fmt.Sprint(answerData.QId/1000), fmt.Sprint(answerData.QId%1000))
									aId, _ := res.LastInsertId()

									if aId > 0 {
										if len(answerData.TrackIds) > 0 {
											var trackIdList []int
											for _, v := range answerData.TrackIds {
												trackIdList = append(trackIdList, v)
											}
											stmt, vars, err := sqlx.In("UPDATE `image_track` SET `a_id`=? WHERE `i_id` IN(?) AND `type`=0 AND `a_id` IS NULL", aId, trackIdList)
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												_,err := tx.Exec(stmt, vars...)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
											}
										}

										var aidNullSafe null.Int
										aidNullSafe.Valid = true
										aidNullSafe.Int64 = int64(aId)
										go func() {
											bluePolicy := bluemonday.StrictPolicy()
											t_nAnswer := bluePolicy.Sanitize(answerData.Answer)
											var answerPreview string
											var dots string
											if len(t_nAnswer) > 40 {
												answerPreview = t_nAnswer[:40]
												dots= "..."
											} else {
												dots = ""
												answerPreview = t_nAnswer
											}
											notifyUser(NotificationData{
												UId:              int(t_uid.Int64),
												OriginUId:        uid,
												QACId:            answerData.QId,
												NotificationType: 2,
												Message:          "",
												AId:              aidNullSafe,
												Preview: 		  t_qTitle+"\n"+answerPreview+dots,
											})
										}()
									}
									tx.Commit()
									return nil, utils.SuccessPostedAnswer, "info", aId
								}
							} else {
								_, err := tx.Exec("UPDATE `question` SET `activity`=`activity`+"+strconv.Itoa(activityIncrement)+" WHERE `q_id`=?", dbAnswerData.QId)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									utils.RDB.HDel(fmt.Sprint(answerData.QId/1000), fmt.Sprint(answerData.QId%1000))
									tx.Commit()
									aId, _ := res.LastInsertId()
									return nil, utils.SuccessPostedAnswer, "info", aId
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetAllUserAnswers(xUserData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllUserAnswers api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllUserAnswers"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		{
			userData := struct {
				UId int `json:"u_id"`
			}{}
			err := json.Unmarshal(xUserData, &userData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if userData.UId < 1 {
				return errors.New("Invalid userid"), utils.ErrGeneric, "warn", nil
			}
			uid := userData.UId

			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT a.`a_id`, a.`q_id`, a.`answer`, q.`title`, a.`created` FROM `answer` a, `question` q WHERE a.`q_id`=q.`q_id` AND a.`u_id`=?", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					aDbData := []struct {
						AId     int       `json:"a_id" db:"a_id"`
						QId     int       `json:"q_id" db:"q_id"`
						Answer  string    `json:"answer" db:"answer"`
						Title   string    `json:"title" db:"title"`
						Created time.Time `json:"created" db:"created"`
					}{}
					for rows.Next() {
						t_answer := struct {
							AId     int       `json:"a_id" db:"a_id"`
							QId     int       `json:"q_id" db:"q_id"`
							Answer  string    `json:"answer" db:"answer"`
							Title   string    `json:"title" db:"title"`
							Created time.Time `json:"created" db:"created"`
						}{}
						err := rows.StructScan(&t_answer)
						if err != nil {
							tx.Rollback()
							rows.Close()
							return err, utils.ErrGeneric, "error", nil
						} else {
							aDbData = append(aDbData, t_answer)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched answers", "info", aDbData
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllUserAnswers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllUserAnswers"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteAnswer(xAnswerData []byte, sessionId string) string {
	/*
		a_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteAnswer api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteAnswer"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		answerData := struct {
			AId int `json:"a_id"`
		}{}
		err := json.Unmarshal(xAnswerData, &answerData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if sessionId == "" {
			return errors.New("Invalid Session id"), utils.ErrNotLoggedIn, "warn", nil
		} else if answerData.AId < 1 {
			return errors.New("Invalid aid"), utils.ErrGeneric, "warn", answerData.AId
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/uid"), utils.ErrGeneric, "error", email + " " + string(role)
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var dbUid null.Int
				var created time.Time
				var t_nAnswer string
				err := tx.QueryRowx("SELECT `u_id`, `created`, `answer` FROM `answer` WHERE `a_id`=?", answerData.AId).Scan(&dbUid, &created, &t_nAnswer)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New("answer doesnt exist"), utils.ErrAnswerDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else if role == 1 || role == 2 {
					if archiveAnswer(tx, answerData.AId) {
						var t_qid int
						err := tx.QueryRowx("SELECT `q_id` FROM `answer` WHERE `a_id`=?", answerData.AId).Scan(&t_qid)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						utils.RDB.HDel(fmt.Sprint(t_qid/1000), fmt.Sprint(t_qid%1000))
						_, err = tx.Exec("DELETE FROM `answer` WHERE `a_id`=?", answerData.AId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							_, err := tx.Exec("UPDATE `image_track` SET `a_id`=NULL WHERE `a_id`=? AND `type`=?", answerData.AId, 0)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
							if uid == int(dbUid.Int64) || (!dbUid.Valid || dbUid.Int64 == 0) {
								tx.Commit()
								return nil, utils.SuccessDeletedAnswer, "info", nil
							}
							/*delete 100 points from original user for violation of policy as mod/admin deleted it.*/
							if addUserPoints(tx, int(dbUid.Int64), utils.PointModDeleteAnswer) {
								_, err := tx.Exec("UPDATE `user` SET `deprication`=`deprication`+1 WHERE `u_id`=?", dbUid.Int64)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
								if checkForSuspension(tx, int(dbUid.Int64)) {
									tx.Commit()
									go func(){
										bluePolicy := bluemonday.StrictPolicy()
										t_nAnswer := bluePolicy.Sanitize(t_nAnswer)
										var answerPreview string
										var dots string
										if len(t_nAnswer) > 40 {
											answerPreview = t_nAnswer[:40]
											dots= "..."
										} else {
											dots = ""
											answerPreview = t_nAnswer
										}
										notifyUser(NotificationData{
											UId:              int(dbUid.Int64),
											NotificationType: 9,
											Preview: answerPreview+dots,
										})
									}()
									return nil, utils.SuccessDeletedAnswer, "info", nil
								} else {
									tx.Rollback()
									return errors.New("Could not check for suspension."), utils.ErrGeneric, "error", nil
								}
							} else {
								tx.Rollback()
								return errors.New("Could not deduct points from original user"), utils.ErrGeneric, "error", nil
							}
						}
					} else {
						tx.Rollback()
						return errors.New("Could not archive answer"), utils.ErrGeneric, "error", nil
					}
				} else {
					if uid != int(dbUid.Int64) {
						tx.Rollback()
						return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", nil
					} else if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
						tx.Rollback()
						return errors.New(utils.ErrCannotDeleteAnswerAfterGrace), utils.ErrCannotDeleteAnswerAfterGrace, "wanr", nil
					} else {
						if archiveAnswer(tx, answerData.AId) {
							var t_qid int
							err := tx.QueryRowx("SELECT `q_id` FROM `answer` WHERE `a_id`=?", answerData.AId).Scan(&t_qid)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
							utils.RDB.HDel(fmt.Sprint(t_qid/1000), fmt.Sprint(t_qid%1000))
							_, err = tx.Exec("DELETE FROM `answer` WHERE `a_id`=?", answerData.AId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								_, err := tx.Exec("UPDATE `image_track` SET `a_id`=NULL WHERE `a_id`=? AND `type`=?", answerData.AId, 0)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
								tx.Commit()
								return nil, utils.SuccessDeletedAnswer, "info", nil
							}
						} else {
							tx.Rollback()
							return errors.New("Could not archive answer"), utils.ErrGeneric, "error", nil
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func VoteAnswer(xAnswerData []byte, sessionId string) string {
	/*
		a_id int
		vote_type int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nVoteAnswer api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "VoteAnswer"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid uid or role"), utils.ErrGeneric, "error", nil
			}
		} else {
			answerData := struct {
				AId      int `json:"a_id"`
				VoteType int `json:"vote_type"`
			}{}
			err := json.Unmarshal(xAnswerData, &answerData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if answerData.AId < 1 {
				return errors.New("Invalid answer id"), utils.ErrGeneric, "error", nil
			} else if answerData.VoteType == 0 {
				return errors.New("Invalid vote_type"), utils.ErrGeneric, "error", nil
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					/*Check if the answer exists*/
					var t_aid, qid int
					var answerUid null.Int
					var t_nAnswer string
					err := tx.QueryRowx("SELECT `a_id`, `u_id`, `q_id`, `answer` FROM `answer` WHERE `a_id`=?", answerData.AId).Scan(&t_aid, &answerUid, &qid, &t_nAnswer)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New(utils.ErrAnswerDoesntExist), utils.ErrAnswerDoesntExist, "warn", nil
						} else {
							return err, utils.ErrGeneric, "error", nil
						}
					} else {
						var voteType = 1
						var voteTypeString = "up_votes"
						var voteTypeOppositeString = "down_votes"
						if answerData.VoteType < 0 {
							voteType = -1
							voteTypeString = "down_votes"
							voteTypeOppositeString = "up_votes"
						}
						if answerUid.Valid && int(answerUid.Int64) == uid {
							tx.Rollback()
							return errors.New(utils.ErrCannotVoteOwnAnswer), utils.ErrCannotVoteOwnAnswer, "warn", nil
						}
						/*Check for previous vote from same user*/
						var dbVoteType int
						err := tx.QueryRowx("SELECT `vote_type` FROM `vote` WHERE `u_id`=? AND `a_id`=?", uid, answerData.AId).Scan(&dbVoteType)
						if err != nil {
							if err == sql.ErrNoRows {
								/*Fresh vote*/
								_, err := tx.Exec("INSERT INTO `vote`(`u_id`, `a_id`, `vote_type`) VALUES(?, ?, ?)", uid, answerData.AId, voteType)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									_, err := tx.Exec("UPDATE `answer` `a`, `question` `q` SET a."+voteTypeString+"=a."+voteTypeString+"+"+strconv.Itoa((int(math.Abs(float64(voteType)))))+", q.activity=q.activity+1 WHERE a.q_id=q.q_id AND a.a_id=?", answerData.AId)
									if err != nil {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										if (!answerUid.Valid) || addUserPoints(tx, int(answerUid.Int64), voteType) {
											utils.RDB.HDel(fmt.Sprint(qid/1000), fmt.Sprint(qid%1000))
											tx.Commit()
											var notifType int
											if voteType == 1 {
												notifType = 6
											} else {
												notifType = 7
											}
											var aidNullSafe null.Int
											aidNullSafe.Valid = true
											aidNullSafe.Int64 = int64(answerData.AId)
											if answerUid.Valid && answerUid.Int64 > 0 {
												go func(){
													var dots, answerPreview string
													bluePolicy := bluemonday.StrictPolicy()
													t_nAnswer := bluePolicy.Sanitize(t_nAnswer)
													if len(t_nAnswer) > 40 {
														answerPreview = t_nAnswer[:40]
														dots= "..."
													} else {
														dots = ""
														answerPreview = t_nAnswer
													}
													notifyUser(NotificationData{
														UId:              int(answerUid.Int64),
														OriginUId:        uid,
														NotificationType: notifType,
														QACId:            qid,
														AId:              aidNullSafe,
														Preview: answerPreview+dots,
													})
												}()

											}
											return nil, "Successfully Voted.", "info", nil
										} else {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										}
									}
								}
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							/*Previous vote exists*/
							var voteNormalize int

							if dbVoteType == voteType {
								tx.Rollback()
								if dbVoteType == -1 {
									return errors.New(utils.ErrAlreadyDownVoted), utils.ErrAlreadyDownVoted, "warn", nil
								} else {
									return errors.New(utils.ErrAlreadyUpVoted), utils.ErrAlreadyUpVoted, "warn", nil
								}
							} else if dbVoteType == 0 {
								tx.Rollback()
								return errors.New("Invalid vote_type in vote"), utils.ErrGeneric, "error", nil
							}

							switch {
							case voteType < 0:
								voteNormalize = -2
							case voteType > 0:
								voteNormalize = +2
							default:
								tx.Rollback()
								return errors.New("Invalid vote_type"), utils.ErrGeneric, "error", nil
							}

							_, err := tx.Exec("UPDATE `vote` SET `vote_type`="+strconv.Itoa(voteType)+" WHERE `u_id`=? AND `a_id`=?", uid, answerData.AId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								_, err := tx.Exec("UPDATE `answer` SET "+voteTypeString+"="+voteTypeString+"+"+strconv.Itoa(int(math.Abs(float64(voteType))))+", "+voteTypeOppositeString+"="+voteTypeOppositeString+"+"+strconv.Itoa(int(-math.Abs(float64(voteType))))+" WHERE `a_id`=?", answerData.AId)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									if !answerUid.Valid || addUserPoints(tx, int(answerUid.Int64), voteNormalize) {
										utils.RDB.HDel(fmt.Sprint(qid/1000), fmt.Sprint(qid%1000))
										tx.Commit()
										var notifType int
										if voteType == 1 {
											notifType = 6
										} else {
											notifType = 7
										}
										var aidNullSafe null.Int
										aidNullSafe.Valid = true
										aidNullSafe.Int64 = int64(answerData.AId)
										if(answerUid.Valid && answerUid.Int64 > 0) {
											go func(){
												var dots, answerPreview string
												bluePolicy := bluemonday.StrictPolicy()
												t_nAnswer := bluePolicy.Sanitize(t_nAnswer)
												if len(t_nAnswer) > 40 {
													answerPreview = t_nAnswer[:40]
													dots= "..."
												} else {
													dots = ""
													answerPreview = t_nAnswer
												}
												notifyUser(NotificationData{
													UId:              int(answerUid.Int64),
													OriginUId:        uid,
													NotificationType: notifType,
													QACId:            qid,
													AId:              aidNullSafe,
													Preview:answerPreview+dots,
												})
											}()

										}
										return nil, "Successfully Voted.", "info", nil
									} else {
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "VoteAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "VoteAnswer"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

/*MOD/ADMIN only*/

type dbAnswer struct {
	AId    int    `json:"a_id" db:"a_id"`
	Answer string `json:"answer" db:"answer"`
}

func getAnswersFromIdsForReport(xAIds []int) []dbAnswer {
	/*
		QIds []int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAnswersFromIds api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAnswersFromIds"})
		}
	}()

	checkData := func() (error, string, string, []dbAnswer) {
		answerData := struct {
			AIds []int `json:"a_ids" db:"a_ids"`
		}{}
		answerData.AIds = xAIds
		if len(answerData.AIds) < 1 {
			return nil, "Successfully fetched answers", "info", []dbAnswer{}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				inStmt, inVars, err := sqlx.In("SELECT `a_id`, `answer` FROM `answer` WHERE `a_id` IN(?)", answerData.AIds)
				inStmt = tx.Rebind(inStmt)
				rows, err := tx.Queryx(inStmt, inVars...)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					xDbAnswerData := []dbAnswer{}
					dbAnswerData := dbAnswer{}
					dbAnswerIds := []int{}
					for rows.Next() {
						err := rows.StructScan(&dbAnswerData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						xDbAnswerData = append(xDbAnswerData, dbAnswerData)
						dbAnswerIds = append(dbAnswerIds, dbAnswerData.AId)
					}
					//delete non-existent questions from report db
					aidsToClose := func(a, b []int) (res []int) {
						m := make(map[int]bool)
						for _, v := range b {
							m[v] = true
						}

						for _, v := range a {
							if _, ok := m[v]; !ok {
								res = append(res, v)
							}
						}
						return res
					}(answerData.AIds, dbAnswerIds)
					if len(aidsToClose) > 0 {
						inStmt, inVars, err := sqlx.In("UPDATE `report` SET `closed`=1 WHERE `r_type`=2 AND `qac_id` IN(?)", aidsToClose)
						inStmt = tx.Rebind(inStmt)
						_, err = tx.Exec(inStmt, inVars...)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
					}
					tx.Commit()
					return nil, "", "info", xDbAnswerData

				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getAnswersFromIdsForReport"})
		return []dbAnswer{}
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getAnswersFromIdsForReport"})
		return data
	}
}

func archiveAnswer(tx *sqlx.Tx, a_id int) bool {
	if a_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_answer` SELECT a.*, UTC_TIMESTAMP() FROM `answer` `a` WHERE `a_id`=?", a_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveAnswer"})
		return false
	}
	return true
}

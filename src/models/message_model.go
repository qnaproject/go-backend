package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"gopkg.in/guregu/null.v3"
	"html"
	"time"
	"utils"
)

type MessageModel struct {
	MId        int       `json:"m_id" db:"m_id"`
	FromUserId int       `json:"from_user_id" db:"from_user_id"`
	ToUserId   int       `json:"to_user_id" db:"to_user_id"`
	Message    string    `json:"message" db:"message"`
	MsgRead    bool      `json:"msg_read" db:"msg_read"`
	Created    time.Time `json:"created" db:"created"`
}

type MessageModelWithNull struct {
	MId        int       `json:"m_id" db:"m_id"`
	FromUserId null.Int  `json:"from_user_id" db:"from_user_id"`
	Name       string    `json:"name"`
	ToUserId   null.Int  `json:"to_user_id" db:"to_user_id"`
	Message    string    `json:"message" db:"message"`
	MsgRead    bool      `json:"msg_read" db:"msg_read"`
	Created    time.Time `json:"created" db:"created"`
}

func AddMessage(xMessageData []byte, sessionId string) string {
	/*
		to_user_id id
		message string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddMessage api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddMessage"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "error", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid role/uid"), utils.ErrGeneric, "error", email
			}
		} else {
			messageData := MessageModel{}
			err := json.Unmarshal(xMessageData, &messageData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if messageData.Message == "" {
				return errors.New("Empty message"), utils.ErrCannotSendEmptyMessage, "warn", nil
			} else if len(messageData.Message) > 1000 {
				return errors.New(utils.ErrMessageTooLong), utils.ErrMessageTooLong, "warn", nil
			} else {
				messageData.FromUserId = uid
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					if messageData.ToUserId == 0 {
						tx.Rollback()
						return errors.New("Invalid to_user_id"), utils.ErrGeneric, "warn", nil
					} else if messageData.ToUserId == messageData.FromUserId {
						tx.Rollback()
						return errors.New(utils.ErrCannotMessageYourself), utils.ErrCannotMessageYourself, "warn", nil
					}
					toRole := 0
					err := tx.QueryRowx("SELECT `role` FROM `user` WHERE `u_id`=?", messageData.ToUserId).Scan(&toRole)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New("User doesn't exist"), utils.ErrRecipientForMessageDoesntExist, "warn", nil
						}
						return err, utils.ErrGeneric, "error", nil
					} else if toRole == -2 {
						tx.Rollback()
						return errors.New("cant message - recipients account has been terminated"), utils.ErrCannotSendMessageToRecipient, "warn", messageData.ToUserId
					} else {
						var t_blockuid int
						err := tx.QueryRowx("SELECT `u_id` FROM `message_blocklist` WHERE `u_id`=? AND `blocked_by_id`=?", uid, messageData.ToUserId).Scan(&t_blockuid)
						if err != nil && err != sql.ErrNoRows {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else if err == nil {
							tx.Rollback()
							return errors.New(utils.ErrCannotSendMessageToRecipient), utils.ErrCannotSendMessageToRecipient, "warn", nil
						}
						bluePolicy := bluemonday.StrictPolicy()
						messageData.Message = bluePolicy.Sanitize(messageData.Message)
						messageData.Message = html.UnescapeString(messageData.Message)
						if messageData.Message == "" {
							tx.Rollback()
							return errors.New("Empty message"), "You cannot send this message.", "warn", nil
						}
						result, err := tx.Exec("INSERT INTO `message`(`from_user_id`, `to_user_id`, `message`) VALUES(?,?,?)", messageData.FromUserId, messageData.ToUserId, messageData.Message)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							insertId, err := result.LastInsertId()
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								tx.Commit()
								return nil, utils.SuccessMessageSent, "info", insertId
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddMessage"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddMessage"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetConversation(xMessageData []byte, sessionId string) string {
	/*
		from_user_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetMessage api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetMessage"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "error", nil
		} else {
			if uid, role, ok := getUserIdAndRole(email); !ok {
				switch role {
				case -1:
					return errors.New(utils.ErrSuspension), utils.ErrSuspension, "error", nil
				case -2:
					return errors.New(utils.ErrTerminated), utils.ErrTerminated, "error", nil
				default:
					return errors.New("Invalid uid/role"), utils.ErrGeneric, "error", email
				}
			} else {
				requestData := struct {
					FromUserId int `json:"from_user_id"`
				}{}
				err := json.Unmarshal(xMessageData, &requestData)
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else if requestData.FromUserId < 1 {
					return errors.New("Invalid fromuserId"), utils.ErrGeneric, "warn", nil
				} else if requestData.FromUserId == uid {
					return errors.New("Trying to view self messages"), utils.ErrGeneric, "warn", email
				} else {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						rows, err := tx.Queryx("SELECT * FROM `message` WHERE `to_user_id` IN(?,?) AND `from_user_id` IN(?,?) ORDER BY `created` DESC LIMIT 100", uid, requestData.FromUserId, uid, requestData.FromUserId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							msg := MessageModelWithNull{}
							msgs := []MessageModelWithNull{}
							msgIds := []int{}
							for rows.Next() {
								err := rows.StructScan(&msg)
								if err != nil {
									rows.Close()
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									msgs = append(msgs, msg)
									msgIds = append(msgIds, msg.MId)
								}
							}
							if len(msgIds) > 0 {
								stmt, vars, err := sqlx.In("UPDATE `message` SET `msg_read`=1 WHERE `m_id` IN(?)", msgIds)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
								_, err = tx.Exec(stmt, vars...)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								}
							}
							var t_fname string
							var t_lname null.String
							err := tx.QueryRowx("SELECT `fname`, `lname` FROM `user` WHERE `u_id`=?", requestData.FromUserId).Scan(&t_fname, &t_lname)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								var name string
								if t_lname.Valid && t_lname.String != "" {
									name = t_fname + " " + t_lname.String
								} else {
									name = t_fname
								}
								for i, v := range msgs {
									if v.FromUserId.Valid && int(v.FromUserId.Int64) == uid {
										msgs[i].Name = "You"
									} else {
										msgs[i].Name = name
									}
								}
							}
							tx.Commit()
							return nil, "Successfully fetched messages", "info", msgs
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetMessage"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetMessage"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetAllMessages(sessionId string) string {
	/*
	 */
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllMessages api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllMessages"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid uid/role"), utils.ErrGeneric, "error", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `msg_read`, `from_user_id`, `message`, `created`  FROM `message` WHERE `m_id` IN (SELECT MAX(`m_id`) from `message` WHERE `to_user_id`=? AND `from_user_id` IS NOT NULL GROUP BY(`from_user_id`)) ORDER BY `m_id` DESC LIMIT 200", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					type msgFormat struct {
						Name       string    `json:"name"`
						DPUrl null.String `json:"dp_url" db:"dp_url"`
						Message    string    `json:"message" db:"message"`
						FromUserId int       `json:"from_user_id" db:"from_user_id"`
						MsgRead    bool      `json:"msg_read" db:"msg_read"`
						Created    time.Time `json:"created" db:"created"`
					}

					msg := msgFormat{}
					msgs := []msgFormat{}
					var uids []int
					for rows.Next() {
						err := rows.StructScan(&msg)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							msgs = append(msgs, msg)
							uids = append(uids, msg.FromUserId)
						}
					}
					uidName := map[int]string{}
					uidDp := map[int]null.String{}
					if len(uids) > 0 {
						stmt, vars, err := sqlx.In("SELECT `fname`,`lname`,`u_id`, `dp_url` FROM `user` WHERE `u_id` IN(?)", uids)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						rows, err := tx.Queryx(stmt, vars...)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							var t_fname string
							var t_lname, t_dpurl null.String
							var t_uid int
							for rows.Next() {
								err := rows.Scan(&t_fname, &t_lname, &t_uid, &t_dpurl)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									if t_lname.Valid && t_lname.String != "" {
										uidName[t_uid] = t_fname + " " + t_lname.String
									} else {
										uidName[t_uid] = t_fname
									}
									if t_dpurl.Valid && t_dpurl.String != "" {
										uidDp[t_uid] = t_dpurl
									}
								}
							}
							for key, val := range msgs {
								if name, ok := uidName[val.FromUserId]; ok {
									msgs[key].Name = name
								}
								if dp, ok := uidDp[val.FromUserId]; ok {
									msgs[key].DPUrl = dp
								}
							}
						}
					}
					tx.Commit()
					return nil, "Successfully fetched message list", "info", msgs
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllMessages"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllMessages"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func MarkAllMessagesAsRead(sessionId string) string {
	/*
	 */
	start := time.Now()
	defer func() {
		fmt.Println("\nMarkAllMessagesAsRead api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "MarkAllMessagesAsRead"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			default:
				return errors.New("Invalid userid or role"), utils.ErrGeneric, "warn", email
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				_, err := tx.Exec("UPDATE `message` SET `msg_read`=1 WHERE `to_user_id`=?", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					tx.Commit()
					return nil, "Successfully marked all messages as read", "info", nil
				}
			}
		}
	}

	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "MarkAllMessagesAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "MarkAllMessagesAsRead"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func BlockMessagesFromUser(xUserData []byte, sessionId string) string {
	/*
		u_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nBlockMessagesFromUser api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "BlockMessagesFromUser"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("invalid uid/role"), utils.ErrGeneric, "error", email
			}
		} else {
			userData := struct {
				UId int `json:"u_id"`
			}{}
			err := json.Unmarshal(xUserData, &userData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if userData.UId < 1 {
				return errors.New("Invalid userid"), utils.ErrGeneric, "warn", nil
			} else if userData.UId == uid {
				return errors.New("Cannot block self"), utils.ErrGeneric, "warn", nil
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					var t_uid int
					err := tx.QueryRowx("SELECT `u_id` FROM `user` WHERE `u_id`=?", userData.UId).Scan(&t_uid)
					if err != nil {
						tx.Rollback()
						if err == sql.ErrNoRows {
							return errors.New("User doesnt exist"), utils.ErrAccountNonExistant, "warn", nil
						} else {
							return err, utils.ErrGeneric, "error", nil
						}
					} else {
						var t_uid int
						err := tx.QueryRowx("SELECT `u_id` FROM `message_blocklist` WHERE `blocked_by_id`=? AND `u_id`=?", uid, userData.UId).Scan(&t_uid)
						if err != nil {
							if err == sql.ErrNoRows {
								_, err := tx.Exec("INSERT INTO `message_blocklist`(`u_id`,`blocked_by_id`) VALUES(?,?)", userData.UId, uid)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									tx.Commit()
									return nil, utils.SuccessBlockedMessages, "info", nil
								}
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							tx.Rollback()
							return errors.New("user is already blocked"), utils.ErrUserAlreadyBlocked, "warn", nil
						}

					}
				}
			}
		}
	}

	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "BlockMessagesFromUser"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "BlockMessagesFromUser"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func UnblockUser(xUserData []byte, sessionId string) string {
	/*
		u_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nUnblockUser api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "UnblockUser"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, role, ok := getUserIdAndRole(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("Invalid uid/role"), utils.ErrGeneric, "error", email
			}
		} else {
			userData := struct {
				UId int `json:"u_id"`
			}{}
			err := json.Unmarshal(xUserData, &userData)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else if userData.UId < 1 {
				return errors.New("Invalid uid to unblock"), utils.ErrGeneric, "warn", nil
			} else {
				tx, err := DB.Beginx()
				if err != nil {
					return err, utils.ErrGeneric, "error", nil
				} else {
					result, err := tx.Exec("DELETE FROM `message_blocklist` WHERE `u_id`=? AND `blocked_by_id`=?", userData.UId, uid)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						if rows, _ := result.RowsAffected(); rows < 1 {
							tx.Rollback()
							return errors.New("This user was not previously blocked."), utils.ErrUserNotBlocked, "warn", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessUnblockedUser, "info", nil
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "UnblockUser"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "UnblockUser"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func CRON_deleteOldMessages() {
	apiStart := time.Now()
	defer func() { fmt.Println("\nCRON_deleteOldMessages api execution time-> ", time.Since(apiStart)) }()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_deleteOldMessages started"})
	tx, err := DB.Beginx()

	if err != nil {
		utils.Logger(utils.MsgPrint{Level: "error", Msg: "Unable to begin transaction in CRON_deleteOldMessages", Error: err.Error()})
		return
	} else {
		tx.Exec("INSERT INTO `archive_message` SELECT m.*, UTC_TIMESTAMP() FROM message m WHERE `created` < ?", time.Now().UTC().Add(-15*24*time.Hour))
		_, err := tx.Exec("DELETE FROM `message` WHERE `created` < ?", time.Now().UTC().Add(-15*24*time.Hour))
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Level: "error", Error: err.Error()})
			return
		} else {
			tx.Commit()
			return
		}
	}
}

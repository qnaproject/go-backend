package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"gopkg.in/guregu/null.v3"
	"gopkg.in/redis.v5"
	"html"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
	"utils"
)

func init() {
	pong, err := utils.RDB.Ping().Result()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Api: "Redis ping test", Level: "fatal"})
	} else {
		fmt.Println("\n", pong)
	}
	fmt.Println("hash-max-ziplist-entries->", utils.RDB.ConfigGet("hash-max-ziplist-entries"))
}

/*Question model*/

type QuestionModel struct {
	QId         int       `db:"q_id" json:"q_id"`
	UId         null.Int  `db:"u_id" json:"u_id"`
	Title       string    `db:"title" json:"title"`
	Description string    `db:"description" json:"description"`
	Category    int       `db:"category" json:"category"`
	Keywords null.String  `db:"keywords" json:"keywords"`
	Closed      bool      `db:"closed" json:"closed"`
	Activity    int       `db:"activity" json:"activity"`
	Created     time.Time `db:"created" json:"created"`
	Updated     time.Time `db:"updated" json:"updated"`
}

var CategoryList = map[string]int{
	"Art":         1,
	"Economy":       2,
	"Education":     3,
	"Entertainment": 4,
	"Fiction":     5,
	"Food":    6,
	"Health":        7,
	"Humanity":     8,
	"Nature":      9,
	"News":    10,
	"Pets":        11,
	"Relationships":       12,
	"Science":      13,
	"Software":    14,
	"Sports":      15,
	"Technology":    16,
	"Travel":      17,
	// "Anon":18,
}

var ReverseCategoryList = map[int]string{
	1:  "Art",
	2:  "Economy",
	3:  "Education",
	4:  "Entertainment",
	5:  "Fiction",
	6:  "Food",
	7:  "Health",
	8:  "Humanity",
	9:  "Nature",
	10: "News",
	11: "Pets",
	12: "Relationships",
	13: "Science",
	14: "Software",
	15: "Sports",
	16: "Technology",
	17: "Travel",
	// 18:"Anon",
}

func AddQuestion(xQuestionData []byte, sessionId string) string {
	/*
		//anon bool
		title string
		category string
		description string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddQuestion api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddQuestion"})
		}
	}()
	strictSanitize := bluemonday.StrictPolicy()
	checkData := func() (error, string, string, interface{}) {
		qData := struct {
			Anon        bool   `json:"anon"`
			Title       string `json:"title"`
			Category    string `json:"category"`
			Description string `json:"description"`
		}{}
		qStoreData := QuestionModel{}
		err := json.Unmarshal(xQuestionData, &qData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			/*UId validation*/
			email, ok := getLoggedInUserEmail(sessionId)
			if !ok {
				return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
			} else {
				role, ok := hasRightToPost(email)
				if !ok {
					if role == -1 {
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
					} else if role == -2 {
						return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", email
					} else {
						return errors.New(utils.ErrGeneric), utils.ErrGeneric, "warn", email
					}
				}
				UId, ok := getUserIdFromEmail(email)
				if !ok {
					return errors.New(utils.ErrGeneric), utils.ErrGeneric, "error", nil
				} else {

					qStoreData.UId.Valid = true
					qStoreData.UId.Int64 = int64(UId)
					/*Use this to enable question anonymity*/
					//if !qData.Anon {
					//	qStoreData.UId.Valid = true
					//	qStoreData.UId.Int64 = int64(UId)
					//} else {
					//	qStoreData.UId.Valid = false
					//}
					/*Title validation*/

					qData.Title = strings.TrimSpace(qData.Title)
					if len(qData.Title) < 20 {
						return errors.New(utils.ErrQuestionTooShort), utils.ErrQuestionTooShort, "warn", qData.Title
					} else if len(qData.Title) > 100 {
						return errors.New(utils.ErrQuestionTooLong), utils.ErrQuestionTooLong, "warn", qData.Title
					} else {
						qData.Title = strings.ToLower(qData.Title)
						match, err := regexp.MatchString(`^[a-z0-9\\?\\.\\'\\-\\, ]{10,100}$`, qData.Title)
						if err != nil {
							return err, utils.ErrGeneric, "error", nil
						} else if !match {
							return errors.New("Invalid characters in question"), utils.ErrInvalidCharsInQuestion, "warn", nil
						} else {
							qData.Title = func() string {
								for i, v := range qData.Title {
									return string(unicode.ToUpper(v)) + qData.Title[i+1:]
								}
								return ""
							}()
							/*Description validation*/
							if qData.Description == "" {
								qData.Description = "No description provided"
							}
							if len(qData.Description) < 20 {
								return errors.New(utils.ErrDescriptionTooShort), utils.ErrDescriptionTooShort, "warn", nil
							} else if len(qData.Description) > 1500 {
								return errors.New(utils.ErrDescriptionTooLong), utils.ErrDescriptionTooLong, "warn", nil
							} else {
								qData.Description = strictSanitize.Sanitize(qData.Description)
								qData.Description = html.UnescapeString(qData.Description)

								/*Category validation*/
								if _, ok := CategoryList[qData.Category]; !ok {
									return errors.New(utils.ErrInvalidCategory), utils.ErrInvalidCategory, "warn", qData.Category
								} else {
									qStoreData.Category = CategoryList[qData.Category]
									qStoreData.Title = qData.Title
									qStoreData.Description = qData.Description

									tx, err := DB.Beginx()
									if err != nil {
										return err, utils.ErrGeneric, "error", nil
									} else {
										keywords := utils.GetKeywordsFromTitle(qData.Title)
										if keywords != "" {
											qStoreData.Keywords.Valid = true
											qStoreData.Keywords.String = keywords
										}
										row, err := tx.NamedExec("INSERT INTO `question` (`u_id`, `title`, `description`, `category`, `keywords`) VALUES (:u_id, :title, :description, :category, :keywords)", &qStoreData)
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											qId, err := row.LastInsertId()
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												_, err := tx.Exec("UPDATE `user` SET `experience`=`experience`+1 WHERE `u_id`=?", qStoreData.UId)
												if err != nil {
													tx.Rollback()
													return err, utils.ErrGeneric, "error", nil
												}
												tx.Commit()
												return nil, utils.SuccessPostedQuestion, "info", qId
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteQuestion(xQuestionData []byte, sessionId string) string {
	/*
		q_id
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteQuestion api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteQuestion"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		qData := struct {
			QId int64 `json:"q_id"`
		}{}
		err := json.Unmarshal(xQuestionData, &qData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			/*Check logged in*/
			email, ok := getLoggedInUserEmail(sessionId)
			if !ok {
				return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
			} else {
				if qData.QId < 1 {
					return errors.New("Invalid QId"), utils.ErrGeneric, "warn", nil
				} else {
					loggedInUId, ok := getUserIdFromEmail(email)
					if !ok {
						return errors.New("Could not get UId from email"), utils.ErrGeneric, "error", email
					} else {
						tx, err := DB.Beginx()
						if err != nil {
							return err, utils.ErrGeneric, "error", nil
						} else {
							role, ok := hasRightToPost(email)
							if !ok {
								tx.Rollback()
								if role == -1 {
									return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", email
								} else if role == -2 {
									return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", email
								} else {
									return errors.New(utils.ErrGeneric), utils.ErrGeneric, "error", nil
								}
							} else if role == 1 || role == 2 {
								/*MOD or ADMIN can delete any question*/
								/*TODO: send email to user notifying of deletion*/
								var originalUId null.Int
								var t_nQuestion string
								err := tx.QueryRowx("SELECT `u_id`, `title` FROM `question` WHERE `q_id`=?", qData.QId).Scan(&originalUId, &t_nQuestion)
								if err != nil {
									tx.Rollback()
									if err == sql.ErrNoRows {
										return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
									} else {
										return err, utils.ErrGeneric, "error", nil
									}
								} else {
									if archiveQuestion(tx, int(qData.QId)) {
										res, err := tx.Exec("DELETE FROM `question` WHERE `q_id`=?", qData.QId)
										utils.RDB.HDel(fmt.Sprint(qData.QId/1000), fmt.Sprint(qData.QId%1000))
										if err != nil {
											tx.Rollback()
											if err == sql.ErrNoRows {
												return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
											} else {
												return err, utils.ErrGeneric, "error", nil
											}
										} else if t_num, _ := res.RowsAffected(); t_num < 1 {
											tx.Rollback()
											return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
										} else {
											/*deduct 100 points from user since mod/admin deleted post for violation*/
											if loggedInUId == int(originalUId.Int64) {
												tx.Commit()
												return nil, utils.SuccessDeletedQuestion, "info", nil
											}
											if originalUId.Valid && originalUId.Int64 > 0 {
												if addUserPoints(tx, int(originalUId.Int64), utils.PointModDeleteQuestion) {
													_, err := tx.Exec("UPDATE `user` SET `deprication`=`deprication`+1 WHERE `u_id`=?", originalUId.Int64)
													if err != nil {
														tx.Rollback()
														return err, utils.ErrGeneric, "error", nil
													}
													if checkForSuspension(tx, int(originalUId.Int64)) {
														tx.Commit()
														go func() {
															notifyUser(NotificationData{
																UId:              int(originalUId.Int64),
																NotificationType: 8,
																Preview: t_nQuestion,
															})
														}()
														return nil, utils.SuccessDeletedQuestion, "info", nil
													} else {
														tx.Rollback()
														return errors.New("Could not check for suspension possibility"), utils.ErrGeneric, "error", nil
													}
												} else {
													tx.Rollback()
													return errors.New("Could not deduct original user points on mod deleting question"), utils.ErrGeneric, "error", nil
												}
											} else {
												tx.Commit()
												return nil, utils.SuccessDeletedQuestion, "info", nil
											}
										}
									} else {
										tx.Rollback()
										return errors.New("Could not archive question"), utils.ErrGeneric, "error", nil
									}
								}
							} else {
								var originalUId null.Int
								var created time.Time
								err := tx.QueryRowx("SELECT `u_id`, `created` FROM `question` WHERE `q_id`=?", qData.QId).Scan(&originalUId, &created)
								if err != nil {
									tx.Rollback()
									if err == sql.ErrNoRows {
										return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
									} else {
										return err, utils.ErrGeneric, "error", nil
									}
								} else if created.UTC().Before(time.Now().UTC().Add(-2 * 24 * time.Hour)) {
									tx.Rollback()
									return errors.New(utils.ErrCannotDeleteQuestionAfterGrace), utils.ErrCannotDeleteQuestionAfterGrace, "warn", nil
								} else {
									if !originalUId.Valid || originalUId.Int64 < 1 {
										tx.Rollback()
										return errors.New(utils.ErrNoPermissioToDeleteQuestion), utils.ErrNoPermissioToDeleteQuestion, "warn", nil
									} else if originalUId.Int64 != int64(loggedInUId) {
										tx.Rollback()
										return errors.New(utils.ErrNoPermissioToDeleteQuestion), utils.ErrNoPermissioToDeleteQuestion, "warn", nil
									} else {
										if archiveQuestion(tx, int(qData.QId)) {
											_, err := tx.Exec("DELETE FROM `question` WHERE `q_id`=?", qData.QId)
											utils.RDB.HDel(fmt.Sprint(qData.QId/1000), fmt.Sprint(qData.QId%1000))
											if err != nil {
												tx.Rollback()
												return err, utils.ErrGeneric, "error", nil
											} else {
												tx.Commit()
												return nil, utils.SuccessDeletedQuestion, "info", nil
											}
										} else {
											tx.Rollback()
											return errors.New("Could not archive question"), utils.ErrGeneric, "error", nil
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "DeleteQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "DeleteQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func ChangeQuestionCategory(xQuestionData []byte, sessionId string) string {
	/*
		q_id int
		category string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nChangeQuestionCategory api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "ChangeQuestionCategory"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		categoryData := struct {
			Category string `json:"category"`
			QId      int    `json:"q_id"`
		}{}
		err := json.Unmarshal(xQuestionData, &categoryData)
		if err != nil {
			return err, utils.ErrGeneric, "warn", nil
		} else if sessionId == "" {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if categoryData.QId < 1 {
			return errors.New("Invalid Question Id"), utils.ErrQuestionDoesntExist, "warn", nil
		} else if categoryId, ok := CategoryList[categoryData.Category]; !ok {
			return errors.New("Invalid category"), utils.ErrInvalidCategory, "warn", categoryData.Category
		} else if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if role, ok := hasRightToPost(email); !ok {
			switch role {
			case -1:
				return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
			case -2:
				return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
			default:
				return errors.New("invalid role"), utils.ErrGeneric, "error", email + "->role " + strconv.Itoa(role)
			}
		} else if role == 1 || role == 2 {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var t_qid int
				err := tx.QueryRowx("SELECT `q_id` FROM `question` WHERE `q_id`=?", categoryData.QId).Scan(&t_qid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return err, utils.ErrQuestionDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "warn", nil
					}
				} else {
					res, err := tx.Exec("UPDATE `question` SET `category`=? WHERE `q_id`=?", categoryId, categoryData.QId)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else if rowCount, err := res.RowsAffected(); rowCount < 1 || err != nil {
						tx.Rollback()
						return errors.New(utils.ErrQuestionAlreadyOfCategory), utils.ErrQuestionAlreadyOfCategory, "warn", nil
					} else {
						tx.Commit()
						return nil, utils.SuccessCategoryChanged, "info", nil
					}
				}
			}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				var originalUid int
				err := tx.QueryRowx("SELECT `u_id` FROM `question` WHERE `q_id`=?", categoryData.QId).Scan(&originalUid)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return err, utils.ErrQuestionDoesntExist, "warn", nil
					} else {
						return err, utils.ErrGeneric, "warn", nil
					}
				} else if uid, ok := getUserIdFromEmail(email); !ok || uid != originalUid {
					tx.Rollback()
					if !ok {
						return errors.New("Could not get userid from email"), utils.ErrGeneric, "error", nil
					} else {
						return errors.New(utils.ErrNoPermissionToChangeCategory), utils.ErrNoPermissionToChangeCategory, "warn", nil
					}
				} else {
					res, err := tx.Exec("UPDATE `question` SET `category`="+string(categoryId)+" WHERE `q_id`=?", categoryData.QId)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else if rowCount, err := res.RowsAffected(); rowCount < 1 || err != nil {
						tx.Rollback()
						return errors.New(utils.ErrQuestionAlreadyOfCategory), utils.ErrQuestionAlreadyOfCategory, "warn", nil
					} else {
						tx.Commit()
						return nil, utils.SuccessCategoryChanged, "info", nil
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "ChangeQuestionCategory"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "ChangeQuestionCategory"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetAllUserQuestions(xUserData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllUserQuestions api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllUserQuestions"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		userData := struct {
			UId int `json:"u_id"`
		}{}
		err := json.Unmarshal(xUserData, &userData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if userData.UId < 1 {
			return errors.New("Invalid userid"), utils.ErrGeneric, "error", nil
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `title`, `created`, `q_id` FROM `question` WHERE `u_id`=?", userData.UId)
				if rows.Err() != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "info", []int{}
				}
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return nil, utils.ErrYouHaveNotAskedAnyQuestions, "info", []int{}
					} else {
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
					type qDataStruct struct {
						Title   string    `json:"title" db:"title"`
						Created time.Time `json:"created" db:"created"`
						QId     int       `json:"q_id" db:"q_id"`
					}
					xQData := []qDataStruct{}
					for rows.Next() {
						t_qdata := qDataStruct{}
						err := rows.StructScan(&t_qdata)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							xQData = append(xQData, t_qdata)
						}
					}
					tx.Commit()
					return nil, "Successfully fetched questions.", "info", xQData
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllUserQuestions"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllUserQuestions"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetPost(xQuestionData []byte) string {
	/*
		q_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetPost api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetPost"})
		}
	}()

	var qidForRedis int
	questionData := struct {
		QId int `json:"q_id"`
	}{}

	err := json.Unmarshal(xQuestionData, &questionData)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = utils.ErrGeneric
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else if questionData.QId < 1 {
		utils.Logger(utils.MsgPrint{Error: errors.New("Invalid question id").Error(), Msg: utils.ErrGeneric, Level: "warn", Api: "GetPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = utils.ErrGeneric
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		rPost, err := utils.RDB.HGet(fmt.Sprint(questionData.QId/1000), fmt.Sprint(questionData.QId%1000)).Result()
		if err != nil || len(rPost) == 0 {
			if err != redis.Nil && len(rPost) > 0 {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetPost"})
				responseData := utils.ResponseBodyJSON{}
				responseData.Msg = utils.ErrGeneric
				responseData.Success = false
				xResponseJson, _ := json.Marshal(responseData)
				return string(xResponseJson)
			}
		} else {
			//decompress
			postDataFromRedis, err := utils.GzipDecompress(rPost)
			if err != nil {
				utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: utils.ErrGeneric, Level: "error", Api: "GetPost"})
				responseData := utils.ResponseBodyJSON{}
				responseData.Msg = utils.ErrGeneric
				responseData.Success = false
				xResponseJson, _ := json.Marshal(responseData)
				return string(xResponseJson)
			}
			fmt.Println("REDIS")
			return postDataFromRedis
		}
	}

	checkData := func() (error, string, string, interface{}) {
		{
			type questionWithName struct {
				QuestionModel
				Name string `json:"name"`
			}

			type commentWithName struct {
				CommentModel
				Name string `json:"name"`
			}

			type answerWithName struct {
				AnswerModel
				Name     string            `json:"name"`
				Comments []commentWithName `json:"comments"`
			}
			post := struct {
				Question questionWithName `json:"question"`
				Answers  []answerWithName `json:"answers"`
			}{}

			var questionNoName QuestionModel
			var question questionWithName
			var answers []answerWithName
			userIdToName := map[int64]string{}
			var uids []int64

			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				err := tx.Get(&questionNoName, "SELECT * FROM `question` WHERE `q_id`=?", questionData.QId)
				if err != nil {
					tx.Rollback()
					if err == sql.ErrNoRows {
						return errors.New(utils.ErrQuestionDoesntExist), utils.ErrQuestionDoesntExist, "warn", nil
					}
					return err, utils.ErrGeneric, "error", nil
				} else {
					question.QuestionModel = questionNoName
					if questionNoName.UId.Valid && questionNoName.UId.Int64 > 0 {
						uids = append(uids, questionNoName.UId.Int64)
					}
					rows, err := tx.Queryx("SELECT * FROM `answer` WHERE `q_id`=?", questionData.QId)
					if err != nil {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					} else {
						var answerIds []int
						for rows.Next() {
							t_answer := AnswerModel{}
							err := rows.StructScan(&t_answer)
							if err != nil {
								rows.Close()
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								answerIds = append(answerIds, t_answer.AId)
								t_answerWithName := answerWithName{}
								t_answerWithName.AnswerModel = t_answer
								answers = append(answers, t_answerWithName)
								if t_answer.UId.Valid && t_answer.UId.Int64 > 0 {
									uids = append(uids, t_answer.UId.Int64)
								}
							}
						}
						answerToCommentMap := map[int][]commentWithName{}
						if len(answerIds) > 0 {
							stmt, args, err := sqlx.In("SELECT * FROM `comment` WHERE `a_id` IN(?)", answerIds)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								rows, err := tx.Queryx(stmt, args...)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									for rows.Next() {
										t_comment := CommentModel{}
										err := rows.StructScan(&t_comment)
										if err != nil {
											tx.Rollback()
											return err, utils.ErrGeneric, "error", nil
										} else {
											if t_comment.UId.Valid && t_comment.UId.Int64 > 0 {
												uids = append(uids, t_comment.UId.Int64)
											}
											var t_commentWithName commentWithName
											t_commentWithName.CommentModel = t_comment
											answerToCommentMap[t_comment.AId] = append(answerToCommentMap[t_comment.AId], t_commentWithName)
										}
									}
								}
							}
						}
						if len(uids) > 0 {
						stmt, args, err := sqlx.In("SELECT `u_id`,`fname`,`lname` FROM `user` WHERE `u_id` IN(?)", uids)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							rows, err := tx.Queryx(stmt, args...)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								for rows.Next() {
									var fname string
									var lname null.String
									var uid int64
									err := rows.Scan(&uid, &fname, &lname)
									if err != nil {
										rows.Close()
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										if lname.Valid && lname.String != "" {
											userIdToName[uid] = fname + " " + lname.String
										} else {
											userIdToName[uid] = fname
										}
									}
								}
								if question.UId.Valid && question.UId.Int64 > 0 {
									if val, ok := userIdToName[question.UId.Int64]; ok {
										question.Name = val
									}
								}
								for i, v := range answers {
									if v.UId.Valid && v.UId.Int64 > 0 {
										if val, ok := userIdToName[v.UId.Int64]; ok {
											answers[i].Name = val
										}
									}
								}

								for i, _ := range answerToCommentMap {
									for ii, vv := range answerToCommentMap[i] {
										if answerToCommentMap[i][ii].UId.Valid && answerToCommentMap[i][ii].UId.Int64 > 0 {
											vv.Name = userIdToName[vv.UId.Int64]
											answerToCommentMap[i][ii] = vv
										}
									}
								}

								for i, v := range answers {
									answers[i].Comments = answerToCommentMap[v.AId]
								}

								tx.Commit()
								qidForRedis = questionData.QId
								post.Question = question
								post.Answers = answers
								return nil, "Successfully fetched post", "info", post
							}
						}
					} else {
							tx.Commit()
							qidForRedis = questionData.QId
							post.Question = question
							post.Answers = answers
							return nil, "Successfully fetched post", "info", post
						}
					}
				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetPost"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		compressed := utils.GzipCompress((xResponseJson))
		fmt.Println("MySQL")
		fmt.Println("len->", len(compressed))
		fmt.Println("key->", qidForRedis/1000)
		fmt.Println("field->", qidForRedis%1000)
		if len(compressed) < 2500 && qidForRedis > 0 {
			go func() {
				utils.RDB.HSet(fmt.Sprint(qidForRedis/1000), fmt.Sprint(qidForRedis%1000), compressed)
			}()
		}
		return string(xResponseJson)
	}
}

func GetTrendingPosts(xCategoryData []byte) string {
	/*
		category string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetTrendingPosts api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetTrendingPosts"})
		}
	}()

	checkData := func() (error, string, string, string) {
		categoryData := struct {
			Category string `json:"category"`
		}{}

		err := json.Unmarshal(xCategoryData, &categoryData)
		if err != nil {
			return err, utils.ErrGeneric, "error", ""
		} else {
			searchKey := ""
			if categoryData.Category == "" || categoryData.Category == "General" {
				searchKey = "general"
			} else {
				if _, ok := CategoryList[categoryData.Category]; ok {
					searchKey = strings.ToLower(categoryData.Category)
				}
			}
			if searchKey == "" {
				return errors.New("Invalid category"), utils.ErrInvalidCategory, "warn", categoryData.Category
			}
			result, err := utils.RDB.HGet("trending:", searchKey).Result()
			if err != nil {
				if err == redis.Nil || len(result) < 1 {
					go CRON_setTrendingPosts()
					return errors.New("empty latest posts"), "No posts.", "error", ""
				} else {
					return err, utils.ErrGeneric, "error", ""
				}
			}
			if len(result) == 0 {
				return errors.New("Empty data in get trending post"), "No posts.", "error", ""
			} else {
				decompressedString, err := utils.GzipDecompress(result)
				if err != nil {
					return err, utils.ErrGeneric, "error", ""
				}
				return nil, "Successfully fetched trending posts", "info", decompressedString
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetTrendingPosts"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetTrendingPosts"})
		return string(data)
	}
}

func GetLatestPosts(xCategoryData []byte) string {
	/*
		category string
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetLatestPosts api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetLatestPosts"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		reqData := struct {
			Category string `json:"category"`
		}{}
		err := json.Unmarshal(xCategoryData, &reqData)
		if err != nil {
			return err, utils.ErrGeneric, "error", ""
		} else if reqData.Category == "" || reqData.Category == "General" {
			reqData.Category = "general"
		} else if _, ok := CategoryList[reqData.Category]; !ok && (reqData.Category != "general") {
			return errors.New("Invalid category"), "Invalid category", "warn", reqData.Category
		}
		result, err := utils.RDB.HGetAll("latest:" + strings.ToLower(reqData.Category)).Result()
		if err != nil {
			if err == redis.Nil || len(result) < 1 {
				go CRON_setLatestPosts()
				return errors.New("empty latest posts"), "No posts.", "error", ""
			} else {
				return err, utils.ErrGeneric, "error", ""
			}
		}

		resultString := `{"success":"true", "msg":"Successfully fetched latest posts", "data":[`
		page1 := ""
		page2 := ""
		page3 := ""
		for k, val := range result {
			decompressedString, err := utils.GzipDecompress(val)
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			}
			switch k {
			case "page1":
				page1 = decompressedString
			case "page2":
				page2 = decompressedString
			case "page3":
				page3 = decompressedString
			}
		}

		resultString =resultString
		if page1 !="" {
			resultString+= page1
		}
		if page2 !="" {
			resultString+= ","+page2
		}
		if page3 != "" {
			resultString+= ","+page3
		}
		resultString += "]}"

		return nil, "Successfully fetched latest posts", "info", resultString
	}
	err, msg, level, data1 := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetLatestPosts"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetLatestPosts"})
		return data1.(string)
	}
}

func GetOtherQuestions(xQuestionData []byte) string {
	start := time.Now()
	defer func() {
		fmt.Println("\ngetOtherQuestions api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "getOtherQuestions"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		qData := struct {
			QId      int    `json:"q_id"`
			Category string `json:"category"`
		}{}

		err := json.Unmarshal(xQuestionData, &qData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if qData.QId < 1 {
			return errors.New("Invalid QId"), utils.ErrGeneric, "warn", nil
		} else {
			var catId int
			if len(qData.Category) < 1 {
				return errors.New("Invalid category"), "Invalid Category", "warn", nil
			} else {
				if catIdtemp, ok := CategoryList[qData.Category]; !ok {
					return errors.New("Invalid category"), "Invalid Category", "warn", nil
				} else {
					catId = catIdtemp
				}
			}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			}
			var t_qTitle string
			err = tx.QueryRowx("SELECT `title` FROM `question` WHERE `q_id`=?", qData.QId).Scan(&t_qTitle)
			if err != nil {
				tx.Rollback()
				if err == sql.ErrNoRows {
					return errors.New("This question doesn't exist"), "This question doesn't exist", "warn", nil
				}
				return err, utils.ErrGeneric, "error", nil
			}
			otherQuestions := []struct {
				QId      int    `json:"q_id"`
				Title    string `json:"title"`
				Activity int    `json:"activity"`
			}{}
			keywords := utils.GetKeywordsFromTitle(t_qTitle)
			var depletedQids []int
			if keywords != "" {
				rows, err := tx.Queryx("SELECT `q_id`, `title`, `activity` FROM `question` WHERE `category`=? AND `q_id` <> ? AND MATCH(`keywords`) AGAINST(?) LIMIT 12", catId, qData.QId, keywords)
				if err != nil {
					if err != sql.ErrNoRows {
						tx.Rollback()
						return err, utils.ErrGeneric, "error", nil
					}
				} else {
						var oQid, oActivity int
						var oTitle string
						for rows.Next() {
							err := rows.Scan(&oQid, &oTitle, &oActivity)
							depletedQids = append(depletedQids, oQid)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								otherQuestions = append(otherQuestions, struct {
									QId      int    `json:"q_id"`
									Title    string `json:"title"`
									Activity int    `json:"activity"`
								}{
									QId:      oQid,
									Title:    oTitle,
									Activity: oActivity,
								})
							}
						}
				}
			}

			if len(otherQuestions) < 12 {
				var limit1, limit2 int
				if len(otherQuestions) % 2 == 0 {
					limit1, limit2 = (12 - len(otherQuestions))/2, (12 - len(otherQuestions))/2
				} else {
					limit1, limit2 = (12 - len(otherQuestions))/2, (12 - len(otherQuestions) + 1)/2
				}
				var stmt string
				var vars []interface{}
				if len(depletedQids) > 0 {
					stmt, vars, err = sqlx.In("(SELECT `q_id`, `title`, `activity` FROM `question` WHERE `category`=? AND `q_id` NOT IN (?) AND `q_id` < ? ORDER BY `q_id` DESC LIMIT ?) UNION (SELECT `q_id`, `title`, `activity` FROM `question` WHERE `category`=? AND `q_id` NOT IN (?) AND `q_id` > ? ORDER BY `q_id` LIMIT ?)", catId, depletedQids, qData.QId, limit1, catId, depletedQids, qData.QId, limit2)
				} else {
					stmt, vars, err = sqlx.In("(SELECT `q_id`, `title`, `activity` FROM `question` WHERE `category`=? AND `q_id` < ? ORDER BY `q_id` DESC LIMIT ?) UNION (SELECT `q_id`, `title`, `activity` FROM `question` WHERE `category`=? AND `q_id` > ? ORDER BY `q_id` LIMIT ?)", catId, qData.QId, limit1, catId, qData.QId, limit2)
				}

				rows, err := tx.Queryx(stmt, vars...)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					var oQid, oActivity int
					var oTitle string
					for rows.Next() {
						err := rows.Scan(&oQid, &oTitle, &oActivity)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							otherQuestions = append(otherQuestions, struct {
								QId      int    `json:"q_id"`
								Title    string `json:"title"`
								Activity int    `json:"activity"`
							}{
								QId:      oQid,
								Title:    oTitle,
								Activity: oActivity,
							})
						}
					}
				}
			}
			tx.Commit()
			return nil, "Successfully fetched other questions", "info", otherQuestions
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getOtherQuestions"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getOtherQuestions"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

/*MOD/ADMIN/INTERNAL only*/
func CloseQuestion(xQuestionData []byte, sessionId string) string {
	/*
		q_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nCloseQuestion api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CloseQuestion"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		qData := struct {
			QId int64 `json:"q_id"`
		}{}

		err := json.Unmarshal(xQuestionData, &qData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if qData.QId < 1 {
			return errors.New("Invalid QId"), utils.ErrGeneric, "warn", qData.QId
		} else {
			if email, ok := getLoggedInUserEmail(sessionId); !ok {
				return errors.New("Could not get logged in user email"), utils.ErrNotLoggedIn, "warn", nil
			} else {
				if role, ok := hasRightToPost(email); !ok {
					if role == -1 {
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
					} else if role == -2 {
						return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
					} else {
						return errors.New("invalid role"), utils.ErrGeneric, "error", email + " role->" + strconv.Itoa(role)
					}
				} else if role == 1 || role == 2 {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						res, err := tx.Exec("UPDATE `question` SET `closed`=1 WHERE `q_id`=?", qData.QId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else if rCount, _ := res.RowsAffected(); rCount < 1 {
							tx.Rollback()
							return errors.New(utils.ErrQuestionAlreadyClosedOrDeleted), utils.ErrQuestionAlreadyClosedOrDeleted, "warn", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessClosedQuestion, "info", nil
						}
					}
				} else {
					return errors.New(utils.ErrNoPermissioToCloseQuestion), utils.ErrNoPermissioToCloseQuestion, "warn", nil
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "CloseQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "CloseQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func OpenQuestion(xQuestionData []byte, sessionId string) string {
	/*
		q_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nOpenQuestion api execution time-> ", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CloseQuestion"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		qData := struct {
			QId int64 `json:"q_id"`
		}{}

		err := json.Unmarshal(xQuestionData, &qData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else if qData.QId < 1 {
			return errors.New("Invalid QId"), utils.ErrGeneric, "warn", qData.QId
		} else {
			if email, ok := getLoggedInUserEmail(sessionId); !ok {
				return errors.New("Could not get logged in user email"), utils.ErrNotLoggedIn, "warn", nil
			} else {
				if role, ok := hasRightToPost(email); !ok {
					if role == -1 {
						return errors.New(utils.ErrSuspension), utils.ErrSuspension, "warn", nil
					} else if role == -2 {
						return errors.New(utils.ErrTerminated), utils.ErrTerminated, "warn", nil
					} else {
						return errors.New("invalid role"), utils.ErrGeneric, "error", email + " role->" + strconv.Itoa(role)
					}
				} else if role == 1 || role == 2 {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						res, err := tx.Exec("UPDATE `question` SET `closed`=0 WHERE `q_id`=?", qData.QId)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else if rCount, _ := res.RowsAffected(); rCount < 1 {
							tx.Rollback()
							return errors.New(utils.ErrQuestionAlreadyOpenedOrDeleted), utils.ErrQuestionAlreadyOpenedOrDeleted, "warn", nil
						} else {
							tx.Commit()
							return nil, utils.SuccessOpenedQuestion, "info", nil
						}
					}
				} else {
					return errors.New(utils.ErrNoPermissioToOpenQuestion), utils.ErrNoPermissioToOpenQuestion, "warn", nil
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "OpenQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "OpenQuestion"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = true
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

type dbQuestion struct {
	QId   int    `json:"q_id" db:"q_id"`
	Title string `json:"title" db:"title"`
}

func getQuestionsFromIdsForReport(xQIds []int) []dbQuestion {
	/*
		QIds []int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetQuestionsFromIds api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetQuestionsFromIds"})
		}
	}()

	checkData := func() (error, string, string, []dbQuestion) {
		questionData := struct {
			QIds []int `json:"q_ids" db:"q_ids"`
		}{}
		questionData.QIds = xQIds
		if len(questionData.QIds) < 1 {
			return nil, "Successfully fetched questions", "info", []dbQuestion{}
		} else {
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				inStmt, inVars, err := sqlx.In("SELECT `q_id`, `title` FROM `question` WHERE `q_id` IN(?)", questionData.QIds)
				inStmt = tx.Rebind(inStmt)
				rows, err := tx.Queryx(inStmt, inVars...)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					xDbQuestionData := []dbQuestion{}
					dbQuestionData := dbQuestion{}
					dbQuestionIds := []int{}
					for rows.Next() {
						err := rows.StructScan(&dbQuestionData)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
						xDbQuestionData = append(xDbQuestionData, dbQuestionData)
						dbQuestionIds = append(dbQuestionIds, dbQuestionData.QId)
					}
					//delete non-existent questions from report db
					qidsToClose := func(a, b []int) (res []int) {
						m := make(map[int]bool)
						for _, v := range b {
							m[v] = true
						}

						for _, v := range a {
							if _, ok := m[v]; !ok {
								res = append(res, v)
							}
						}
						return res
					}(questionData.QIds, dbQuestionIds)
					if len(qidsToClose) > 0 {
						inStmt, inVars, err := sqlx.In("UPDATE `report` SET `closed`=1 WHERE `r_type`=1 AND `qac_id` IN(?)", qidsToClose)
						inStmt = tx.Rebind(inStmt)
						_, err = tx.Exec(inStmt, inVars...)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						}
					}
					tx.Commit()
					return nil, "", "info", xDbQuestionData

				}
			}
		}
	}
	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "getQuestionsFromIdsForReport"})
		return []dbQuestion{}
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "getQuestionsFromIdsForReport"})
		return data
	}
}

func archiveQuestion(tx *sqlx.Tx, q_id int) bool {
	if q_id < 1 {
		return false
	}
	_, err := tx.Exec("INSERT INTO `archive_question` SELECT q.*, UTC_TIMESTAMP() FROM `question` `q` WHERE `q_id`=?", q_id)
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "archiveQuestion"})
		return false
	}
	return true
}

func CRON_setTrendingPosts() {
	apiStart := time.Now()
	defer func() { fmt.Println("\nCRON_setTrendingPosts api execution time-> ", time.Since(apiStart)) }()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_setTrendingPosts"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_setTrendingPosts started"})
	tx, err := DB.Beginx()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setTrendingPosts"})
		return
	}
	type trendingPost struct {
		QId      int       `json:"q_id" db:"q_id"`
		Title    string    `json:"title" db:"title"`
		Activity int       `json:"activity" db:"activity"`
		Category string    `json:"category"`
		Created  time.Time `json:"created" db:"created"`
	}
	//categoryList := CategoryList
	categoryList := map[string]int{}
	for key, val := range CategoryList {
		categoryList[key] = val
	}
	categoryList["General"] = -1
	for key, val := range categoryList {

		trendingPostOfCategory := []trendingPost{}
		var rows *sqlx.Rows
		var err error
		if key == "General" {
			rows, err = tx.Queryx("SELECT `q_id`, `title`, `activity`, `category`, `created` FROM `question` WHERE `created` > ? ORDER BY `activity` DESC LIMIT 50", time.Now().UTC().Add(-112*24*time.Hour))
		} else {
			rows, err = tx.Queryx("SELECT `q_id`, `title`, `activity`, `created` FROM `question` WHERE `category`=? AND `created` > ? ORDER BY `activity` DESC LIMIT 50", val, time.Now().UTC().Add(-112*24*time.Hour))
		}
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setTrendingPosts"})
			return
		} else {
			var qid, category, activity int
			var title string
			var created time.Time
			post := trendingPost{}
			for rows.Next() {
				var err error
				if key == "General" {
					err = rows.Scan(&qid, &title, &activity, &category, &created)
				} else {
					err = rows.Scan(&qid, &title, &activity, &created)
				}
				if err != nil {

					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setTrendingPosts"})
					rows.Close()
					return
				} else {
					post.Category = key
					if key == "General" {
						post.Category = ReverseCategoryList[category]
					}
					post.QId = qid
					post.Title = title
					post.Activity = activity
					post.Created = created
					trendingPostOfCategory = append(trendingPostOfCategory, post)
				}
			}
			response := utils.ResponseBodyJSON{}
			response.Data = trendingPostOfCategory
			response.Msg = "Successfully fetched post"
			response.Success = true
			stringPost, err := json.Marshal(&response)
			if err != nil {
				tx.Rollback()
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setTrendingPosts"})
				return
			}
			compressedResult := utils.GzipCompress(stringPost)

			utils.RDB.HSet("trending:", strings.ToLower(key), (compressedResult))
		}
	}
	tx.Commit()
	return
}

func CRON_setLatestPosts() {
	apiStart := time.Now()
	defer func() { fmt.Println("\nCRON_setLatestPosts api execution time-> ", time.Since(apiStart)) }()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "CRON_setLatestPosts"})
		}
	}()
	utils.Logger(utils.MsgPrint{Level: "info", Msg: "CRON_setLatestPosts started"})
	tx, err := DB.Beginx()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestPosts"})
		return
	}
	type latestPost struct {
		QId      int       `json:"q_id" db:"q_id"`
		Title    string    `json:"title" db:"title"`
		Category string    `json:"category"`
		Created  time.Time `json:"created" db:"created"`
	}
	//latestCategoryList := CategoryList
	latestCategoryList := map[string]int{}
	for key, val := range CategoryList {
		latestCategoryList[key] = val
	}
	latestCategoryList["General"] = -1

	for key, val := range latestCategoryList {
		latestPosts := []latestPost{}

		var query string
		if key == "General" {
			query = "SELECT `q_id`, `title`, `category`, `created` FROM `question` ORDER BY `created` DESC LIMIT 100"
		} else {
			query = "SELECT `q_id`, `title`, `category`, `created` FROM `question` WHERE `category`=" + strconv.Itoa(val) + " ORDER BY `created` DESC LIMIT 100"
		}
		rows, err := tx.Queryx(query)
		if err != nil {
			tx.Rollback()
			utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestPosts"})
			return
		} else {
			t_latestPost := latestPost{}
			for rows.Next() {
				var qid, category int
				var title string
				var created time.Time
				err := rows.Scan(&qid, &title, &category, &created)
				if err != nil {
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestPosts"})
					rows.Close()
					return
				} else {
					if _, ok := ReverseCategoryList[category]; !ok {
						tx.Rollback()
						utils.Logger(utils.MsgPrint{Error: errors.New("Invalid category").Error(), Level: "error", Api: "CRON_setLatestPosts", Data: "qid->" + strconv.Itoa(qid)})
						return
					}
					t_latestPost.Category = ReverseCategoryList[category]
					t_latestPost.Title = title
					t_latestPost.QId = qid
					t_latestPost.Created = created
					latestPosts = append(latestPosts, t_latestPost)
				}
			}
		}
		if len(latestPosts) < 100 {
			responseJson, err := json.Marshal(latestPosts)
			if err != nil {
				tx.Rollback()
				utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestPosts"})
				return
			}
			compressedString := utils.GzipCompress(responseJson)
			utils.RDB.HSet("latest:"+strings.ToLower(key), "page"+strconv.Itoa(1), compressedString)
		} else {
			for i := 0; i < len(latestPosts)/100; i++ {
				responseJson, err := json.Marshal(latestPosts[i*100 : (i+1)*100])
				if err != nil {
					tx.Rollback()
					utils.Logger(utils.MsgPrint{Error: err.Error(), Level: "error", Api: "CRON_setLatestPosts"})
					return
				}
				compressedString := utils.GzipCompress(responseJson)
				utils.RDB.HSet("latest:"+strings.ToLower(key), "page"+strconv.Itoa(i+1), compressedString)
			}
		}
	}
	tx.Commit()
}

func invalidateRedisPost(QId int) {

}

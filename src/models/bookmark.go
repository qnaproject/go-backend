package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"time"
	"utils"
)

func AddBookmark(xBookMarkData []byte, sessionId string) (string) {
	/*
	qa_id
	type
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nAddBookmark api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "AddBookmark"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		bookmarkData := struct{
			QAId int `json:"qa_id"`
			Type int `json:"type"`
		}{}
		err := json.Unmarshal(xBookMarkData, &bookmarkData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			if bookmarkData.QAId < 1 || bookmarkData.Type < 1 {
				return errors.New("Invalid type or qaid"), utils.ErrGeneric, "warn", nil
			} else {
				if email, ok := getLoggedInUserEmail(sessionId); !ok {
					return errors.New(utils.ErrNotLoggedIn),utils.ErrNotLoggedIn, "warn", nil
				} else if uid, ok := getUserIdFromEmail(email); !ok {
					return errors.New("Could not get uid from email"), utils.ErrGeneric, "warn", nil
				} else {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						var t_uid int
						err := tx.QueryRowx("SELECT `u_id` FROM `bookmark` WHERE `qa_id`=? AND `u_id`=? AND `type`=?", bookmarkData.QAId, uid, bookmarkData.Type).Scan(&t_uid)
						if err != nil {
							if err == sql.ErrNoRows {
								var id int
								var table, column string
								if bookmarkData.Type == 1 {
									table = "question"
									column = "q_id"
								} else {
									table = "article"
									column = "a_id"
								}
								err := tx.QueryRowx("SELECT `"+column+"` FROM `"+table+"` WHERE `"+column+"`=?", bookmarkData.QAId).Scan(&id)
								if err != nil {
									tx.Rollback()
									if err == sql.ErrNoRows {
										return errors.New("This "+table+" doesn't exist"), "This "+table+" doesn't exist", "warn", nil
									}
									return err, utils.ErrGeneric, "error", nil
								}
								_, err = tx.Exec("INSERT INTO `bookmark`(`qa_id`, `u_id`, `type`) VALUES(?,?,?)", bookmarkData.QAId, uid, bookmarkData.Type)
								if err != nil {
									tx.Rollback()
									return err, utils.ErrGeneric, "error", nil
								} else {
									tx.Commit()
									return nil, "Bookmark added.", "info", nil
								}
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else {
							tx.Rollback()
							return nil, "This bookmark already exists.", "info", nil
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "AddBookmark"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "AddBookmark"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func DeleteBookmark(xBookMarkData []byte, sessionId string) (string) {
	/*
	b_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nDeleteBookmark api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "DeleteBookmark"})
		}
	}()
	checkData := func() (error, string, string, interface{}) {
		bookmarkData := struct{
			BId int `json:"b_id"`
		}{}
		err := json.Unmarshal(xBookMarkData, &bookmarkData)
		if err != nil {
			return err, utils.ErrGeneric, "error", nil
		} else {
			if bookmarkData.BId < 1  {
				return errors.New("Invalid b_id"), utils.ErrGeneric, "warn", nil
			} else {
				if email, ok := getLoggedInUserEmail(sessionId); !ok {
					return errors.New(utils.ErrNotLoggedIn),utils.ErrNotLoggedIn, "warn", nil
				} else if uid, ok := getUserIdFromEmail(email); !ok {
					return errors.New("Could not get uid from email"), utils.ErrGeneric, "warn", nil
				} else {
					tx, err := DB.Beginx()
					if err != nil {
						return err, utils.ErrGeneric, "error", nil
					} else {
						var t_uid int
						err := tx.QueryRowx("SELECT `u_id` FROM `bookmark` WHERE `b_id`=?", bookmarkData.BId).Scan(&t_uid)
						if err != nil {
							if err == sql.ErrNoRows {
								tx.Rollback()
								return errors.New("This bookmark doesn't exist"), "This bookmark doesn't exist", "warn", nil
							} else {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							}
						} else if t_uid != uid {
							tx.Rollback()
							return errors.New(utils.ErrNotAuthorized), utils.ErrNotAuthorized, "warn", nil
						} else {
							_, err := tx.Exec("DELETE FROM `bookmark` WHERE `b_id`=?", bookmarkData.BId)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								tx.Commit()
								return nil, "Bookmark deleted.", "info", nil
							}
						}
					}
				}
			}
		}
	}
	err, msg, level, _ := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "DeleteBookmark"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "DeleteBookmark"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
}

func GetAllUserBookmarks(sessionId string) (string) {
	/*
	b_id int
	*/
	start := time.Now()
	defer func() {
		fmt.Println("\nGetAllUserBookmarks api execution time->", time.Since(start))
	}()
	defer func() {
		if a := recover(); a != nil {
			utils.Logger(utils.MsgPrint{Panic: a, Level: "error", Api: "GetAllUserBookmarks"})
		}
	}()

	checkData := func() (error, string, string, interface{}) {
		if email, ok := getLoggedInUserEmail(sessionId); !ok {
			return errors.New(utils.ErrNotLoggedIn), utils.ErrNotLoggedIn, "warn", nil
		} else if uid, ok := getUserIdFromEmail(email); !ok {
			return errors.New("Could not get user id from email"), utils.ErrGeneric, "error", email
		} else {
			questionTitleMap := map[int]string{}
			articleTitleMap := map[int]string{}
			type bookmarkData struct {
				BId   int    `json:"b_id" db:"b_id"`
				QAId  int    `json:"qa_id" db:"qa_id"`
				Type  int    `json:"type" db:"type"`
				Title string `json:"title" db:"title"`
			}
			xBookmarks := []bookmarkData{}
			qids := []int{}
			aids := []int{}
			tx, err := DB.Beginx()
			if err != nil {
				return err, utils.ErrGeneric, "error", nil
			} else {
				rows, err := tx.Queryx("SELECT `b_id`, `qa_id`, `type` FROM `bookmark` WHERE `u_id`=?", uid)
				if err != nil {
					tx.Rollback()
					return err, utils.ErrGeneric, "error", nil
				} else {
					var dbBookMark bookmarkData
					for rows.Next() {
						err := rows.StructScan(&dbBookMark)
						xBookmarks = append(xBookmarks, dbBookMark)
						if err != nil {
							rows.Close()
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							if dbBookMark.Type == 1 {
								qids = append(qids, dbBookMark.QAId)
							} else {
								aids = append(aids, dbBookMark.QAId)
							}
						}
					}
					if len(qids) > 0 {
						stmt, vars, err := sqlx.In("SELECT `q_id`, `title` FROM `question` WHERE `q_id` IN(?)", qids)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							rows, err := tx.Queryx(stmt, vars...)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								var t_qid int
								var t_title string
								for rows.Next() {
									err := rows.Scan(&t_qid, &t_title)
									if err != nil {
										rows.Close()
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										questionTitleMap[t_qid] = t_title
									}
								}
							}
						}
					}

					if len(aids) > 0 {
						stmt, vars, err := sqlx.In("SELECT `a_id`, `title` FROM `article` WHERE `a_id` IN(?)", aids)
						if err != nil {
							tx.Rollback()
							return err, utils.ErrGeneric, "error", nil
						} else {
							rows, err := tx.Queryx(stmt, vars...)
							if err != nil {
								tx.Rollback()
								return err, utils.ErrGeneric, "error", nil
							} else {
								var t_aid int
								var t_title string
								for rows.Next() {
									err := rows.Scan(&t_aid, &t_title)
									if err != nil {
										rows.Close()
										tx.Rollback()
										return err, utils.ErrGeneric, "error", nil
									} else {
										articleTitleMap[t_aid] = t_title
									}
								}
							}
						}
					}
					tx.Commit()
					for i, v := range xBookmarks {
						if v.Type == 1 {
							if t_title, ok := questionTitleMap[v.QAId]; ok {
								xBookmarks[i].Title = t_title
							} else {
								xBookmarks[i].Title = "Question Deleted"
							}
						} else {
							if t_title, ok := articleTitleMap[v.QAId]; ok {
								xBookmarks[i].Title = t_title
							} else {
								xBookmarks[i].Title = "Article Deleted"
							}
						}
					}
					return nil, "Successfully fetched all user bookmarks", "info", xBookmarks
				}
			}
		}
	}

	err, msg, level, data := checkData()
	if err != nil {
		utils.Logger(utils.MsgPrint{Error: err.Error(), Msg: msg, Level: level, Api: "GetAllUserBookmarks"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Msg = msg
		responseData.Success = false
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	} else {
		utils.Logger(utils.MsgPrint{Msg: msg, Level: level, Api: "GetAllUserBookmarks"})
		responseData := utils.ResponseBodyJSON{}
		responseData.Success = true
		responseData.Msg = msg
		responseData.Data = data
		xResponseJson, _ := json.Marshal(responseData)
		return string(xResponseJson)
	}
	}
